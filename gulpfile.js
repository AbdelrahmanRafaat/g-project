var elixir = require('laravel-elixir');

/**
 * clientProfile , taskerProfile , landingPage , home , taskersListing , pendingListing , createPending , settings , notifications ,
*/
elixir(function(mix) {
	/**
	 * Css
	 * 'libraries/dropzone.css',
	 * 
	 */
	mix.styles([
		'libraries/bootstrap.min.css',
		'libraries/sweetalert.css',
		'landingPage.css'
	] , 'public/css/landingPage.css');

	mix.styles([
		'libraries/bootstrap.min.css',
		'libraries/select2.min.css',
		'libraries/sweetalert.css',
		'navbar.css',
		'createTask.css'
	] , 'public/css/createTask.css');

	mix.styles([
		'libraries/bootstrap.min.css',
		'libraries/sweetalert.css',
		'navbar.css',
		'showTask.css'
	] , 'public/css/showTask.css');


	mix.styles([
		'libraries/bootstrap.min.css',
		'libraries/select2.min.css',
		'libraries/sweetalert.css',
		'libraries/dropzone.css',
		'navbar.css',
		'edit-client.css'
	] , 'public/css/edit-client.css');

	mix.styles([
		'libraries/bootstrap.min.css',
		'libraries/select2.min.css',
		'libraries/sweetalert.css',
		'libraries/dropzone.css',
		'navbar.css',
		'edit-tasker.css'
	] , 'public/css/edit-tasker.css');


	mix.styles([
		'libraries/bootstrap.min.css',
		'libraries/sweetalert.css',
		'navbar.css',
		'clientProfile.css'
	] , 'public/css/clientProfile.css');

	mix.styles([
		'libraries/bootstrap.min.css',
		'libraries/sweetalert.css',
		'navbar.css',
		'taskerProfile.css'
	] , 'public/css/taskerProfile.css');

	mix.styles([
		'libraries/bootstrap.min.css',
		'libraries/sweetalert.css',
		'navbar.css',
		'home.css'
	] , 'public/css/home.css');

	mix.styles([
		'libraries/bootstrap.min.css',
		'libraries/sweetalert.css',
		'navbar.css',
		'taskersListing.css'
	] , 'public/css/taskersListing.css');

	mix.styles([
		'libraries/bootstrap.min.css',
		'libraries/sweetalert.css',
		'navbar.css',
		'pendingListing.css'
	] , 'public/css/pendingListing.css');

	mix.styles([
		'libraries/bootstrap.min.css',
		'libraries/sweetalert.css',
		'navbar.css',
		'createPending.css'
	] , 'public/css/createPending.css');

	mix.styles([
		'libraries/bootstrap.min.css',
		'libraries/sweetalert.css',
		'navbar.css',
		'notifications.css'
	] , 'public/css/notifications.css');

	mix.styles([
		'libraries/bootstrap.min.css',
		'libraries/sweetalert.css',
		'libraries/dropzone.css',
		'libraries/select2.min.css',
		'navbar.css',
		'buildProfile.css'
	] , 'public/css/buildProfile.css');

	mix.styles([
		'libraries/bootstrap.min.css',
		'libraries/sweetalert.css',
		'navbar.css',
		'chatBoard.css'
	],'public/css/chatBoard.css');

	/**
	 * ############################################################################################################
	 * ############################################################################################################
	 * ############################################################################################################
	 */

	/**
	 * JS ,
	 *  taskersListing , pendingListing , createPending  , notifications 
	 */
	mix.scripts([
		'libraries/jquery.min.js',
		'libraries/bootstrap.min.js',
		'libraries/sweetalert.min.js',
		'libraries/dropzone.js',
		'libraries/select2.min.js',
		'helpers.js',
		'edit-client.js',
		'authNavBarNotifications.js',
		'authNavBarMessages.js'
	] , 'public/js/edit-client.js');

	mix.scripts([
		'libraries/jquery.min.js',
		'libraries/bootstrap.min.js',
		'libraries/sweetalert.min.js',
		'helpers.js',
		'showTask.js',
		'authNavBarNotifications.js',
		'authNavBarMessages.js'
	] , 'public/js/showTask.js');


	mix.scripts([
		'libraries/jquery.min.js',
		'libraries/bootstrap.min.js',
		'libraries/sweetalert.min.js',
		'libraries/select2.min.js',
		'helpers.js',
		'createTask.js',
		'authNavBarNotifications.js',
		'authNavBarMessages.js'
	] , 'public/js/createTask.js');

	mix.scripts([
		'libraries/jquery.min.js',
		'libraries/bootstrap.min.js',
		'libraries/sweetalert.min.js',
		'libraries/dropzone.js',
		'libraries/select2.min.js',
		'helpers.js',
		'edit-tasker.js',
		'authNavBarNotifications.js',
		'authNavBarMessages.js'
	] , 'public/js/edit-tasker.js');


	mix.scripts([
		'libraries/jquery.min.js',
		'libraries/bootstrap.min.js',
		'libraries/sweetalert.min.js',
		'landingPage.js',
		'authNavBarNotifications.js',
		'authNavBarMessages.js'
	] , 'public/js/landingPage.js');

	mix.scripts([
		'libraries/jquery.min.js',
		'libraries/bootstrap.min.js',
		'libraries/sweetalert.min.js',
		'helpers.js',
		'clientProfile.js',
		'authNavBarNotifications.js',
		'authNavBarMessages.js'
	] , 'public/js/clientProfile.js');

	mix.scripts([
		'libraries/jquery.min.js',
		'libraries/bootstrap.min.js',
		'libraries/sweetalert.min.js',
		'helpers.js',
		'taskerProfile.js',
		'authNavBarNotifications.js',
		'authNavBarMessages.js'
	] , 'public/js/taskerProfile.js');

	mix.scripts([
		'libraries/jquery.min.js',
		'libraries/bootstrap.min.js',
		'libraries/sweetalert.min.js',
		'home.js',
		'authNavBarNotifications.js',
		'authNavBarMessages.js'
	] , 'public/js/home.js');

	mix.scripts([
		'libraries/jquery.min.js',
		'libraries/bootstrap.min.js',
		'libraries/sweetalert.min.js',
		'taskersListing.js',
		'authNavBarNotifications.js',
		'authNavBarMessages.js'
	] , 'public/js/taskersListing.js');

	mix.scripts([
		'libraries/jquery.min.js',
		'libraries/bootstrap.min.js',
		'libraries/sweetalert.min.js',
		'pendingListing.js',
		'authNavBarNotifications.js',
		'authNavBarMessages.js'
	] , 'public/js/pendingListing.js');

	mix.scripts([
		'libraries/jquery.min.js',
		'libraries/bootstrap.min.js',
		'libraries/sweetalert.min.js',
		'createPending.js',
		'authNavBarNotifications.js',
		'authNavBarMessages.js'
	] , 'public/js/createPending.js');

	mix.scripts([
		'libraries/jquery.min.js',
		'libraries/bootstrap.min.js',
		'libraries/sweetalert.min.js',
		'notifications.js',
		'authNavBarNotifications.js',
		'authNavBarMessages.js'
	] , 'public/js/notifications.js');

	mix.scripts([
		'libraries/jquery.min.js',
		'libraries/bootstrap.min.js',
		'libraries/sweetalert.min.js',
		'libraries/dropzone.js',
		'libraries/select2.min.js',
		'buildProfile.js',
		'authNavBarNotifications.js',
		'authNavBarMessages.js'
	] , 'public/js/buildProfile.js');

	mix.scripts([
		'libraries/jquery.min.js',
		'libraries/bootstrap.min.js',
		'libraries/sweetalert.min.js',
		'authNavBarMessages.js',
		'publicMessaging.js',
		'chatSearch.js',
		'chatPictures.js',
		'authNavBarNotifications.js'
	] , 'public/js/chatBoard.js');

});