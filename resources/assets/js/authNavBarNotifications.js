let authNavBarNotifications = function(){
	let navBar = $('nav');
	let notificationsContainer = navBar.find('.navBarNotifications');
	let dropDownList = notificationsContainer.find('ul.dropdown-menu');
	let label = notificationsContainer.find('.label');

	notificationsContainer.on('click',function(){
		resetLabel();
	});

	function removeLastNotification(){
		let notifications = dropDownList.find('.navBarNotification');
		if(notifications.length >= 4){
			$(notifications[notifications.length-1]).remove();
		}
	}

	function addNewNotification(notificationContent){
		dropDownList.prepend('<li class="navBarNotification"><a>'+notificationContent+'</li></a>');
	}

	function incrementLabel(){
		let currentValue = label.html();
		if(Number(currentValue) >= 4){
			return;
		}
		if(currentValue == ''){
			label.html('1');
			return;
		}
		label.html( ( Number(label.html()) )+1);
	}

	function resetLabel(){
		label.html('');
	}

	//Exsposed API
	return {
		removeLastNotification : removeLastNotification,
		addNewNotification : addNewNotification,
		incrementLabel : incrementLabel
	}; 
};