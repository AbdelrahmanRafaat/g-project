let userPictureChanger = function(){
	let options = {
		url : '/user/'+windowLocation().getResourceId('user')+'/profile/picture/',
		params : {
			_token: $('input[name="_token"]').val()
		},
		paramName : 'picture' ,
		autoProcessQueue : true,
		addRemoveLinks   : true,
		parallelUploads  : 1,
		maxFiles : 1,
		maxFilesize : 3,
		acceptedFiles : '.jpg, .jpeg, .png, .bmp'
	};

	let dropzoneInstance = dropzoneFileUploader(document.querySelector('#profilePictureUploader') , options , false);

	dropzoneInstance.on('success' , function( file , response ){
		$('input[name="picture"][type="hidden"]').val(response);
	});

	dropzoneInstance.on('maxfilesexceeded' , function(file){
		this.removeFile(file);
	});

	dropzoneInstance.on('removedfile' , function(){
		if(dropzoneInstance.files.length == 0){
			$('input[name="picture"][type="hidden"]').val('');
		}
	});

};

let dropzoneFileUploader = function(targetDiv , options , autoDiscover){
	Dropzone.autoDiscover = autoDiscover;
	let dropzoneInstance = new Dropzone( targetDiv , options);

	let existingPhoto = $('input[name="picture"][type="hidden"]').val();
	if(windowLocation().isUpdatePath() && existingPhoto.length >= 50 ){
		let mockFile = { 
			name: existingPhoto ,
			size : 400 
		};
		dropzoneInstance.emit('addedfile', mockFile);
		dropzoneInstance.emit('thumbnail', mockFile, '/images/users/profilePictures/th-'+existingPhoto);
		dropzoneInstance.emit('complete', mockFile);
	}

	return dropzoneInstance;
};


let ManualGeoLocator = function(countries , states){
	
	function generateOptions(optionsArray){
		let options = '';

		for(let i = 0 ; i < optionsArray.length ; i++){
			options += '<option value="'+optionsArray[i]+'">'+upperCaseFirst(optionsArray[i])+'</option>';
		}
		return options;
	}

	function upperCaseFirst(string){
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
	
	function getCountries(){
		return countries;
	}

	function getCountryStates(countryName){
		let countryStates = [];
		for(let i = 0 ; i < states.length ; i++){
			if(states[i].country == countryName){
				countryStates.push(states[i]);
			}
		}
		return countryStates;
	}

	function getStateCities(countryName , stateName){
		for(let i = 0 ; i < states.length ; i++){
			if(states[i].country == countryName &&  states[i].name == stateName){
				return states[i].cities;
			}
		}
	}

	function dryStatesObjects(statesObjectsArray){
		let statesNames = [];
		for(let i = 0 ; i < statesObjectsArray.length ; i++){
			statesNames.push(statesObjectsArray[i].name);
		}
		return statesNames;
	}

	return{
		generateOptions  : generateOptions,
		getCountries     : getCountries,
		getCountryStates : getCountryStates,
		getStateCities   : getStateCities,
		dryStatesObjects : dryStatesObjects
	};
};

let buildProfileManualGeoLocator = function(addressSelectionDiv , addressInput){
	//FromServer
	let countries = ['egypt'];
	let states    = [
		{
			country : 'egypt',
			name : 'cairo',
			cities : ['masr-el-gdeda','madent-nasr','west-el-balad','el-shrouk']
		},
		{
			country : 'egypt',
			name : 'giza',
			cities : ['el-hawmdeya','giza','hadyek-el-ahram','el-haram']
		},
		{
			country : 'egypt',
			name : 'alexandria',
			cities : ['el-agamy','seedy-beshr','el-montazah']
		}
	];

	addressSelectionDiv = $(addressSelectionDiv);
	addressInput = $(addressInput);
	
	let manualGeo = ManualGeoLocator(countries , states);
	let countriesSelection = addressSelectionDiv.find('select[name="country"]');
	let statesSelection    = addressSelectionDiv.find('select[name="state"]');
	let citiesSelection    = addressSelectionDiv.find('select[name="city"]');

	countriesSelection.append(createCountriesOptions());

	countriesSelection.on('change',function(){
		let newCountry = countriesSelection.val();
		handleCountryChange(newCountry);
	});

	//abstract
	function handleCountryChange(newCountry){
		deleteOptions(statesSelection);
		statesSelection.append(createStatesOptions(newCountry));
		deleteOptions(citiesSelection);
		resetAddressInput();
		updateAddressInput(generateCountryAddress());
	}

	statesSelection.on('change',function(){
		let country  = countriesSelection.val();
		let newState = statesSelection.val();
		handleStateChange(country , newState);
	});

	//abstract
	function handleStateChange(country , newState){
		deleteOptions(citiesSelection);
		citiesSelection.append(createCitiesOptions( country , newState));
		resetAddressInput();
		updateAddressInput(generateCountryStateAddress());
	}

	citiesSelection.on('change' , function(){
		resetAddressInput();
		updateAddressInput(generateFullAddress());		
	});

	function createCountriesOptions(){
		return manualGeo.generateOptions(manualGeo.getCountries());
	}

	function createStatesOptions(countryName){
		return manualGeo.generateOptions( manualGeo.dryStatesObjects(manualGeo.getCountryStates(countryName)) );
	}

	function createCitiesOptions(countryName , stateName){
		return manualGeo.generateOptions(manualGeo.getStateCities(countryName , stateName));
	}

	function deleteOptions(selection){
		let options = selection.find('option');
		for(let i = 1 ; i < options.length ; i++){
			options.eq(i).remove();
		}
	}

	function generateCountryAddress(){
		let country  = countriesSelection.val();
		if(country !== 'nothing'){
			return country;
		}
	}

	function generateCountryStateAddress(){
		let country  = countriesSelection.val();
		let state    = statesSelection.val();

		if(country != 'nothing' && state != 'nothing' ){
			return country+','+state;
		}
	}

	function generateFullAddress(){
		let country  = countriesSelection.val();
		let state    = statesSelection.val();
		let city     = citiesSelection.val();

		if(country != 'nothing' && state != 'nothing' && city != 'nothing'){
			return country+','+state+','+city;
		}
	}

	function updateAddressInput(newValue){
		addressInput.val(newValue);
	}

	function resetAddressInput(){
		updateAddressInput('');
	}
};


let specilizationsSelector = function(){
	$('.specilizationsSelector').select2({
		placeholder: 'Select your specilizations ..' ,
		maximumSelectionLength: 3,
		allowClear: true
	});
};

let skillsSelector = function(){
	$('.skillsSelector').select2({
		placeholder: 'Select your Skills ..' ,
		maximumSelectionLength: 1,
		allowClear: true
	});
};

let salarySelector = function(){
	$('.salarySelectionBox').select2({
		placeholder: 'Select your Salary ..' ,
		allowClear: true
	});
};
	
let updateAjaxHelper = (function(){
	let updateForm = $('form.updateForm');

	updateForm.on('submit' , function(event){
		event.preventDefault();
		$.ajax({
			method: 'POST',
			url  : updateForm.attr('action'),
			data : updateForm.serialize()+'_method=PUT',
			success : function(response){
				window.location.replace(response['data']['url']);
			},
			error : function(response){
				if(response.status && response.status == 422){
					let errorsObject = response.responseJSON;
					clearErrorsList();
					let errorsTemplate = makeErrorsTemplate(errorsObject);
					showErrorsList(errorsTemplate);
					moveScrollUp();
				}
			}
		});
	});

	function makeErrorsTemplate(errorsObject){
		return makeErrorsTemplateStart()+makeErrorsList(errorsObject)+makeErrorsTemplateEnd();
	}

	function makeErrorsTemplateStart(){
		return `<div class="alert alert-danger errors col-md-12">
					<ul>`;
	}

	function makeErrorElement(errorMessage){
		return `<li>${errorMessage}</li>`;
	}
	
	function makeErrorsTemplateEnd(){
		return		`</ul>
			   </div>`;
	}

	function makeErrorsList(errorsObject){
		let validationErrorsKeys = Object.keys(errorsObject);	
		let errorsList = ``;
		for(let i = 0 ; i <  validationErrorsKeys.length ; i++){
			errorsList += makeErrorElement(errorsObject[validationErrorsKeys[i]][0]);
		}
		return errorsList;
	}

	function clearErrorsList(){
		let errorsList = updateForm.find('.errors');
		if(errorsList){
			errorsList.remove();
		}
	}

	function moveScrollUp(){
		window.scrollTo(0,0);
	}

	function showErrorsList(errorsTemplate){
		updateForm.prepend(errorsTemplate);
	}

})();

let taskerEditor = (function(){
	userPictureChanger();
	buildProfileManualGeoLocator( $('.addressSelection') , $('.addressInput') );
	specilizationsSelector();
	skillsSelector();
	salarySelector();
})();