let notificationPaginator = (function(){
	let paginationContainer = $('.notificationsPaginator');
	let paginator = paginationContainer.find('#seeMorePaginator');
	let nextLink  = paginator.find('a');
	let loadingSymbol = paginationContainer.find('.paginatorCircle'); 

	paginator.on('click' , function(){
		sendPaginationRequest();
	});

	nextLink.on('click' , function(event){
		event.preventDefault();
	});

	function sendPaginationRequest(){
		$.ajax({
			method: 'GET',
			url : nextLink.attr('href'),
			beforeSend: function(){
				changeElementVisibilty(paginator , 'none');
				changeElementVisibilty(loadingSymbol , 'block');
			},
			success : function(response){
				addNewNotifications(response[0]);
				handlePaginator(response[1]);
			}
		});
	}

	function changeElementVisibilty( element , visibilty){
		element.css('display' , visibilty);
	}

	function handlePaginator(nextPaginationUrl){
		if(nextPaginationUrl == null){
			removePaginator();
			return;
		}
		updatePaginationUrl(nextPaginationUrl);
		changeElementVisibilty(paginator , 'block');
		changeElementVisibilty(loadingSymbol , 'none');
	}

	function removePaginator(){
		paginationContainer.remove();
	}

	function updatePaginationUrl(nextPaginationUrl){
		nextLink.attr('href',nextPaginationUrl);
	}

	function addNewNotifications(newNotifications){
		$('.notifications').append(newNotifications);
	}

})();

// acceptAssignment
// declineAssignment

let taskerAssignmentHandler = (function(){
	let acceptAssignmentButton  = $('.acceptAssignment');
	let declineAssignmentButton = $('.declineAssignment');
	let assigmentButtonsDiv = acceptAssignmentButton.parent();
	let assigmentDiv  = assigmentButtonsDiv.parent();
	let updateTaskUrl = assigmentDiv.find('a').attr('href');

	acceptAssignmentButton.on('click' , function(){
		$.ajax({
			method: 'POST',
			url : updateTaskUrl,
			data : {
				_method : 'PUT',
				status  : 2
			},
			dataType : 'json',
			beforeSend: function(){
				assigmentButtonsDiv.find('button').prop('disabled' , true);
			},
			complete: function(){
				assigmentButtonsDiv.find('button').prop('disabled' , false);
			},
			success : function(response){
				swal('Job was assigned to you.' , 'Contact the client and get started' ,'success');
				assigmentButtonsDiv.remove();
			},
			error : function(response){
				swal('Something went wrong , try again');
			}
		});
	});

	declineAssignmentButton.on('click' , function(){
		$.ajax({
			method: 'POST',
			url  : updateTaskUrl,
			data : {
				_method : 'PUT',
				status  : 0
			},
			dataType : 'json',
			beforeSend: function(){
				assigmentButtonsDiv.find('button').prop('disabled' , true);
			},
			complete: function(){
				assigmentButtonsDiv.find('button').prop('disabled' , false);
			},
			success : function(response){
				assigmentButtonsDiv.remove();
			},
			error : function(response){
				swal('Something went wrong , try again');
			}
		});
	});

})();