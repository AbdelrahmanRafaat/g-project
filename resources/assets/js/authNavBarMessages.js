let authNavBarMessages = function(){
	let messagesContainer = $('li.navBarMessages');

	function addMessage(message){
		if(checkConversationExistance(message.from)){
			addMessageToExitingConversation(message);
			return;
		}
		addMessageToNewConversation(message);
	}

	function addMessageToNewConversation(message){
		if(getMessagesCount() >= 4){
			removeOldestMessage();
			addPeakMessage(message);
			return;
		}
		addPeakMessage(message);
	}

	function checkConversationExistance(conversationId){
		return (messagesContainer.find('.message[data-from="'+conversationId+'"]').length == 1) ? true : false;
	}

	function addMessageToExitingConversation(message){
		removeMessage(message.from);
		addPeakMessage(message);
	}

	function createMessageTemplate(message){
		return '<li data-from="'+message.from+'" class="message"><a href="/user/'+message.to+'/messages/?show='+message.from+'"><img class="navbarMessageAvatar" src="/images/users/profilePictures/'+message.profilePicture+'"><div class="navbarMessageUserName">'+message.senderName+'</div><div class="navbarMessageBody">'+message.content+'</div></a></li>';
	}

	function addPeakMessage(message){
		let messageTemplate = createMessageTemplate(message);
		messagesContainer.find('ul.dropdown-menu').prepend(messageTemplate);
	}

	function removeMessage(messageId){
		messagesContainer.find('.message[data-from="'+messageId+'"]').remove();
	}

	function removeOldestMessage(){
		removeMessageByIndex(getMessagesCount-1);
	}

	function removeMessageByIndex(index){
		messagesContainer.find('li.message').eq(index).remove();
	}

	function getMessagesCount(){
		return messagesContainer.find('li.message').length;
	}

	//Exsposed API
	return {
		addMessage : addMessage
	}; 
};