let sendMessageHandler = (function(){
	let sendMessageDiv = $('.sendMessage');
	let to = sendMessageDiv.attr('userId');

	sendMessageDiv.on('click' , function(){
		swal({
			title : 'Your Message',
			text  : '',
			type: 'input',
			showCancelButton: true,
			closeOnConfirm: false,
			animation: 'slide-from-top',
			inputPlaceholder: 'Write Your Message',
			confirmButtonText: 'Send' 
		},
		function(inputValue){
			if(inputValue === false){
				return false;
			}
			if(inputValue === ''){
				swal.showInputError('You need to write something!');
				return false;  
			}

			$.ajax({
				method: 'POST',
				url  : '/user/'+windowLocation().getResourceId('user')+'/profile-message/',
				data : {
					'content' : inputValue
				},
				dataType : 'json',
				beforeSend: function(){
					$('button.confirm').prop('disabled' , true);
				},
				complete: function(){
					$('button.confirm').prop('disabled' , false);
				},
				success : function(response){
					swal('Good Job' , 'Message was delivered' , 'success');
				},
				error : function(response){
					swal('Something went wrong , try again');
				}
			});

			
		});
	});

})();