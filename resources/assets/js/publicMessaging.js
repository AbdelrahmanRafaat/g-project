/**
 * Self Executing Reveal Module that handles public Messaging.
 * @Author : Abdel-rahman Rafaat Ahmed
 */
let publicMessaging = (function(){
	/**
	 * #####################################
	 * #  		  DOM Caching		 	   #
	 * #####################################
	 */
	let chatDOM = $('div.chatApp');
	let chatBoxesContainer = chatDOM.find('div.chatBoxesContainer');
	let friendList   = chatDOM.find('div.friendList');
	let friend       = friendList.find('div.friend');
	let closeButtons = chatBoxesContainer.find('div.close');
	let sendMessageButtons  = chatBoxesContainer.find('.send_message');
	let chatTextBoxes   = chatBoxesContainer.find('.message_input');
	let messagesLists   = chatBoxesContainer.find('ul.messages');
	let friendsIds = getFriendsIds();
	let latestChatBoxNumber = 0;

	/**
	 * #####################################
	 * #  		Binding DOM Events 		   #
	 * #####################################
	 */

	/**
	 * When a user clicks on a friends from his frined list.
	 * We Will Show The Chatting Box Between him and his friend.
	 * and Remove The New Messages Notifications if it exist.
	 */
	friend.on('click' , function(){
		showCorrespondingChatBox($(this));
		if(chatSearch.getSearchFlag()){
			chatSearch.endSearch();
		}
		clearNotificationsBox($(this).find('.notifications'));
	});

	/**
	 * When a user clicks on close button on the chatBox 
	 * Close the chatBox.  
	 **/
	closeButtons.on('click' , function(){
		closeChatBox($(this));
	});

	/**
	 * When a user types a message and press ENTER key.
	 * Send the message(by an Ajax Request to the server).
	 */
	chatTextBoxes.on('keypress' , function(event){
		if(event.which == 13){
			let message = getMessageOnEnter($(this));
			if(checkMessageLength(message)){
				let chatBox = $(this).parent().parent().parent().parent();
				sendMessage(message , chatBox);
			}
		}
	});

	/**
	 * When a user types a message and click SEND key.
	 * Send the message(by an Ajax Request to the server).
	 */
	sendMessageButtons.on('click' , function(){
		let message = getMessageOnClick($(this));
		if(checkMessageLength(message)){
			let chatBox = $(this).parent().parent().parent();
			sendMessage(message , chatBox);
		}
	});

	//friend , friendNumber
	//privateChat , chatBoxNumber

	/**
	 * When the user scroll up in the messages box.
	 * We Send an ajax request to fetch the more messages before starting Message.
	 */
	messagesLists.on('scroll',function(){
		let messageList = $(this);
		if( (messageList.scrollTop() > 0 && messageList.scrollTop() < 50) && messageList.attr('data-loading') == '0' && messageList.find('.message').length >= 8){
			sendLoadMoreRequest(messageList);
		}
	});

	/**
	 * Find a Friend in friendList By it`s number.
	 * @param  integer number.
	 * @return DOM-Node (.friend-Div)
	 */
	function findFriend(number){
		return friendList.find('.friend[data-friendNumber="'+number+'"]');
	}

	/**
	 * Find a NotificationsBox of a friend.
	 * @param  DOM-Node friend-Div.
	 * @return DOM-Node notificationsBox(notifications-Div).
	 */
	function findNotificationsBox(friend){
		return friend.find('.notifications');
	}

	/**
	 * Hide Notifications Box and reset the notifications counter.
	 * @param DOM-Node notificationsBox(notifications-Div).
	 */
	function clearNotificationsBox(notificationBox){
		hideNotificationsBox(notificationBox);
		nullifyNotificationsBox(notificationBox);
	}

	/**
	 * Hide Notifications Box.
	 * @param  DOM-Node notificationsBox(notifications-Div)
	 */
	function hideNotificationsBox(notificationBox){
		notificationBox.css('display' , 'none');
	}

	/**
	 * Show Notifications Box.
	 * @param  DOM-Node notificationsBox(notifications-Div)
	 */
	function showNotificationsBox(notificationBox){
		notificationBox.css('display' , 'block');
	}

	/**
	 * Update  Notifications Box Counter with a new Content.
	 * @param  DOM-Node notificationsBox(notifications-Div)
	 * @param  integer newContent
	 */
	function updateNotificationsBox(notificationBox , newContent){
		notificationBox.find('.counter').html(newContent);
	}

	/**
	 * Increment Notifications Box Counter.
	 * @param  DOM-Node notificationsBox(notifications-Div).
	 */
	function incrementNotificationsBox(notificationBox){
		let currentCount = notificationBox.find('.counter').html();
		notificationBox.find('.counter').html(parseInt(currentCount)+1);
	}

	/**
	 * Reset Notifications Box Counter to 0.
	 * @param DOM-Node notificationBox(.notifications-Div).
	 */
	function nullifyNotificationsBox(notificationBox){
		updateNotificationsBox(notificationBox , '0');
	}

	/**
	 * Find a ChatBox.
	 * @param  integer number.
	 * @return DOM-Node .privateChat-Div
	 */
	function findChatBox(boxNumber){
		return chatBoxesContainer.find('.privateChat[data-chatboxnumber="'+boxNumber+'"]');
	}

	/**
	 * Sends Ajax Request to the server(save and broadcast the message).
	 * Add The Message to ChatBox.
	 * Clear the text area.
	 * @param string message.
	 * @param DOM-Node chatBox(.privateChat-Div)
	 */
	function sendMessage(message , chatBox){
		postMessageByAjax( prepareDataForPosting(message , chatBox) );
		addMessageToChatBox(message , chatBox , 'left' , chatPictures.getAuthUserPicture());
		addMessageToNavbar(prepareMessageForNavbar(message , chatBox));
		clearChatBoxTextArea(chatBox);
	}

	/**
	 * Get The textBox value when Clicking send Button.
	 * @param DOM-Node button(.send_message-Div-Div)
	 * @return string message
	 */
	function getMessageOnClick(button){
		let message = button.parent().find('input').val();
		return message;
	}

	/**
	 * Get The textBox value when Presseing ENTER.
	 * @param DOM-Node box(.message_input-input-Div).
	 * @return string message 
	 */
	function getMessageOnEnter(box){
		let message = box.val();
		return message;
	}

	/**
	 * Clone The Message Template and with a specific message and side.
	 * @param string message
	 * @param string side
	 */
	function makeMessageTemplate(message , side , profilePicture){
		let messageTemplate = $($('.message_template').clone().html());
		messageTemplate.addClass(side);
		messageTemplate.find('.text').html(message);
		messageTemplate.addClass('appeared');
		messageTemplate.find('img').attr('src' , '/images/users/profilePictures/'+profilePicture);
		return messageTemplate;
	}

	/**
	 * Appeneds message to the DOM.
	 * @param string message.
	 * @param DOM-Node chatBox(.privateChat-Div).
	 * @param string side.
	 */
	function addMessageToChatBox(message , chatBox , side , profilePicture){
		let messagesList = chatBox.find('ul.messages');
		messagesList.append(makeMessageTemplate(message , side , profilePicture));
		moveScrollDown(chatBox);
	}

	/**
	 * Move Scroll Bar Down to the end of ChatBox.
	 * @param DOM-Node chatBox(.privateChat-Div)
	 */
	function moveScrollDown(chatBox){
		let messagesList = chatBox.find('ul.messages');
		messagesList.scrollTop(messagesList[0].scrollHeight);
	}

	/**
	 * Check if the Message length is greater than 0 chars.
	 * @param string message
	 * @return mixed(true||false)
	 */
	function checkMessageLength(message){
		message = message.trim();
		if(message.length > 0){
			return true;
		}
		return false;
	}

	/**
	 *	For a given friend(DOM-Node) show corresponding ChatBox.
	 *	@param DOM-Node friend(.friend-Div).
	 */
	function showCorrespondingChatBox(friend){
		let friendNumber = friend.attr('data-friendNumber');
		showChatBox(friendNumber);
	}

	/**
	 * show Chat Box with the given boxNumber.
	 * set latestChatBoxNumber to the boxNumber that will be viewed.
	 * @param integer boxNumber 
	 */
	function showChatBox(boxNumber){
		hideLatestChatBox();
		latestChatBoxNumber = boxNumber;
		moveScrollDown( changeBoxVisibilty(boxNumber , 'block') );
	}

	/**
	 * Change Visibilty of chatBox with a given boxNumber.
	 * @param integer boxNumber
	 * @param string  visibilty.
	 * @return DOM-Node (.privateChat-Div)
	 */
	function changeBoxVisibilty(boxNumber , visibilty){
		let chatBox = findChatBox(boxNumber);
		chatBox[0].style.display = visibilty;
		return $(chatBox[0]);
	}

	/**
	 * Hide The Latest Chat Box.
	 */
	function hideLatestChatBox(){
		if(latestChatBoxNumber != 0 ){
			changeBoxVisibilty(latestChatBoxNumber , 'none');
		}
	}

	/**
	 * Hide The Chat Box When clicking on close Button.
	 */
	function closeChatBox(closeButton){
		let boxNumber = closeButton.parent().parent().parent().parent().attr('data-chatBoxNumber');
		changeBoxVisibilty(boxNumber , 'none');
		latestChatBoxNumber = 0;
	}

	/**
	 * Prepare The Data that will be sent to the server.
	 * @param  string message
	 * @param  DOM-Node chatBox(.privateChat-Div)
	 * @return string ajaxData. 
	 */
	function prepareDataForPosting(message , chatBox){
		let broadCastTo    = chatBox.attr('data-chatBoxNumber');
		let conversationId = chatBox.attr('data-conversationNumber');
		let token      = chatBox.find('input[name="_token"]').val();
		let profilePicture = chatPictures.getAuthUserPicture();
		let ajaxData   = '_token='+token+'&to='+broadCastTo+'&conversation_id='+conversationId+'&message='+message+'&profilePicture='+profilePicture;
		return ajaxData;
	}

	function prepareMessageForNavbar(messageContent , chatBox){
		let message = {};
		message.from = chatBox.attr('data-chatBoxNumber');
		message.to   = getIdFromUri();
		message.profilePicture = chatPictures.getUserPicture(message.from).replace('/images/users/profilePictures/','');
		message.senderName = chatBox.find('div.title').html();
		message.content    = messageContent;
		return message;
	}

	function addMessageToNavbar(message){
		authNavBarMessages().addMessage(message);
	}

	/**
	 * Send Ajax Request to server.
	 */
	function postMessageByAjax(messageWithData){
		$.ajax({
			method: 'POST',
			url : '/user/'+getIdFromUri()+'/messages/',
			data : messageWithData
		});
	}

	/**
	 * Prepare The Data that will be sent to the server.
	 * @param  DOM-Node messageList(ul.messages)
	 * @return string ajaxData. 
	 */
	function prepareDataForLoadMoreRequest(messageList){
		let token  = chatDOM.find('input[name="_token"]').val();
		let startingMessage = messageList.find('[name="firstMessage"]');
		let conversationId  = messageList.parent().parent().attr('data-conversationNumber');
		return '_token='+token+'&startingMessage='+startingMessage.val()+'&conversationId='+conversationId;
	}

	/**
	 * Sends Ajax Request to load More Messages From Server.
	 */
	function sendLoadMoreRequest(messageList){
		let startingMessage = messageList.find('[name="firstMessage"]');
		let noMessagesAnyMore = false;
		$.ajax({
			method: 'GET',
			url  : '/user/'+getIdFromUri()+'/messages/loadMore',
			data : prepareDataForLoadMoreRequest(messageList),
			beforeSend: function(){
				messageList.attr('data-loading' , '1');
			},
			complete: function(){
				if(noMessagesAnyMore){
					messageList.attr('data-loading' , '1');
					return;
				}
				messageList.attr('data-loading' , '0');
			},	
			success : function(response){
				if(response != 'false'){
					startingMessage.remove();
					messageList.prepend(response);
					return;
				}
				noMessagesAnyMore = true;
			}
		});
	}

	/**
	 * Gets the user id From the address Bar
	 * @return int user id
	 */
	function getIdFromUri(){
		let path = window.location.pathname;
		let id = path.split('/')[2];
		return id;
	}

	/**
	 * For a given chatBox clear it`s textArea.
	 * @param DOM-Node chatBox(.privateChat-Div)
	 */
	function clearChatBoxTextArea(chatBox){
		chatBox.find('.message_input').val('');
	}


	/**
	 * getFriendsIds
	 * @return Array ids.
	 */
	function getFriendsIds(){
		let ids = [];
		for(let i = 0 ; i < friend.length ; i++){
			let number = $(friend[i]).attr('data-friendnumber');
			ids.push(number);
		}
		return ids;
	}


	/**
	 * This module needs to be moved to it`s own file and required using browserfiy
	 * chatBoardQueryStringHandler.js
	 */
	
	/**
	 * getQueryString description]
	 * @return String queryString
	 */
	function getQueryString(){
		return window.location.search;
	}

	/**
	 * [checkQueryString description]
	 * @param  {[type]} queryString [description]
	 * @return {[type]}             [description]
	 */
	function checkQueryString(queryString){
		let regex = /\?show=[0-9]+/ig;
		return (queryString.match(regex)) ? true : false;
	}
		
	/**
	 * [getChatBoxNumber description]
	 * @param  {[type]} queryString [description]
	 * @return {[type]}             [description]
	 */
	function getChatBoxNumber(queryString){
		return queryString.split('=')[1];
	}

	/**
	 * [chatBoxNumberExist description]
	 * @param  {[type]} chatBoxNumber [description]
	 * @return {[type]}               [description]
	 */
	function chatBoxNumberExist(chatBoxNumber){
		return (friendsIds.indexOf(chatBoxNumber) == -1) ? false : true ;
	}

	/**
	 * [handleQueryString description]
	 * @return {[type]} [description]
	 */
	function handleQueryString(){
		let queryString = getQueryString();
		let chatBoxNumber = getChatBoxNumber(queryString);
		if( checkQueryString(queryString) && chatBoxNumberExist(chatBoxNumber) ){
			showChatBox(chatBoxNumber);
		}
	}
	handleQueryString();


	/*publicMessaging API*/
	return {
		findChatBox : findChatBox,
		addMessageToChatBox : addMessageToChatBox,
		findFriend : findFriend,
		showNotificationsBox : showNotificationsBox,
		findNotificationsBox : findNotificationsBox,
		incrementNotificationsBox : incrementNotificationsBox,
		friendList : friendList,
		friend : friend,
		showChatBox : showChatBox,
		friendsIds : friendsIds
	};

})();