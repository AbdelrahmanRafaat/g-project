/**
 * Self Executing Reveal Module that handles chatSearch.
 * @Author : Abdel-rahman Rafaat Ahmed
 */
let chatSearch = (function(){
	/**
	 * #####################################
	 * #  		  DOM Caching		 	   #
	 * #####################################
	 */
	let friendList = publicMessaging.friendList;
	let friends    = publicMessaging.friend;
	let chatSearchTextBox = friendList.find('#search');
	let searchFlag   = false;
	

	/**
	 * #####################################
	 * #  		Binding DOM Events 		   #
	 * #####################################
	 */
	
	/**
	 * When Typing Something on the searchBox , we start the search if it`s length > 0 .
	 */	
	chatSearchTextBox.on('input' , function(){
		let target = $(this).val();
		if( checkSearchInputLength(target) ){
			startSearch(target);
		}
	});

	/**
	 * When Pressing "BACKSPACE" || "DELETE" keys , we check if the searchBox is empty to end the search and reset everyThing.
	 */
	chatSearchTextBox.on('keyup' , function(event){
		if(event.which == 8 || event.which == 46){
			if(chatSearchTextBox.val().length == 0){
				endSearch();
			}
		}
	});

	/**
	 * Check the length of input which was entered in the searchBox 
	 * @param string searchInput
	 * @return boolean  
	 */
	function checkSearchInputLength(searchInput){
		searchInput = searchInput.trim();
		if(searchInput.length > 0){
			return true;
		}
		return false;
	}

	/**
	 * When The user Enter Something in the searchBox.
	 * we Hide All Friends , filter friends , show the matching ones. 
	 * @param string searchInput
	 */
	function startSearch(searchInput){
		searchFlag = true;
		hideAllFriends();
		filterFriends(searchInput);
	}

	/**
	 * filter the friends , show the matching ones.
	 * @param string searchInput
	 */
	function filterFriends(searchInput){
		friends.filter(function(index , friend){
			let friendName = $(friend).find('.friendName').html();
			if(friendName.toLowerCase() === searchInput.toLowerCase() || (friendName.toLowerCase()).includes(searchInput)){
				showFriend($(friend));
			}
		});
	}

	/**
	 * hideAllFriends.
	 */
	function hideAllFriends(){
		friends.css('display','none');
		friends.next('div.chatFriendListSeparators').css('display' , 'none');
	}

	/**
	 * showAllFriends
	 */
	function showAllFriends(){
		friends.next('div.chatFriendListSeparators').css('display' , 'block');
		friends.css('display','block');
	}

	/**
	 * hide Friend.
	 * @param DOM-Node (.friend-Div)
	 */
	function hideFriend(friend){
		friend.css('display','none');
		friend.next('div.chatFriendListSeparators').css('display' , 'none');
	}

	/**
	 * Show Friend.
	 * @param DOM-Node (.friend-Div)
	 */
	function showFriend(friend){
		friend.css('display','block');
		friend.next('div.chatFriendListSeparators').css('display' , 'block');
	}

	/**
	 * When the Search is done,
	 * Reset Every thing.
	 */
	function endSearch(){
		showAllFriends();
		clearChatSearchTextBox();
		searchFlag = false;
	}

	/**
	 * clearChatSearchTextBox
	 */
	function clearChatSearchTextBox(){
		chatSearchTextBox.val('');
	}

	/**
	 * return the value of searchFlag.
	 */
	function getSearchFlag(){
		return searchFlag;
	}

	/**
	 * chatSearch API
	 */
	return {
		getSearchFlag : getSearchFlag ,
		endSearch : endSearch
	};
	
})();