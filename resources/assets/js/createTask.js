let taskCreator = function(){
	let createForm = $('#createTask');
	let taskCreatorNavigator = divNavigator(createForm , 0);
	taskCreatorNavigator.init();
	let navigationButton   = $('[type="button"].next');
	let noTaskersFlag = false;
	let customLocationTrigger = $('button.custom_location_trigger');
	let progressbar = $('#progressbar');
	let progressbarElements = progressbar.find('li');
	let taskersDiv = $('.taskers');
	let selectedTasker = false;
	let taskDataObject = {};

	customLocationTrigger.on('click' , function(event){
		event.preventDefault();
		$('.location').remove();
		$('.customLocation').append(`<input type="text" name="custom_location" placeholder="Task Custom Location" class="createTaskInput" />`);
	});

	taskersDiv.on('click' , '.taskerSelector' , function(event){
		selectedTasker = $(event.toElement).attr('data-taskerId');
		progressbarElements.eq(2).addClass('active');
		taskCreatorNavigator.navigateForward();
		taskDataObject['tasker_id'] = $(event.toElement).attr('data-taskerId');
		navigationButton.val('Finish');
		swal('Tasker Selected','Tasker was selected successfully.','success');
		return;
	});

	navigationButton.on('click' , function(event){
		event.preventDefault();
		let currentIndexNavigation = taskCreatorNavigator.findCurrentVisibleIndex();

		if(currentIndexNavigation == 0){
			if(createForm.find('[name="title"]').val() == ''){
				swal('Task Title is required');
				return;
			}

			if(createForm.find('[name="description"]').val() == ''){
				swal('Task description is required');
				return;
			}

			if(createForm.find('.locationSelector').val() == undefined && createForm.find('[name="custom_location"]').val() == undefined){
				swal('Task Location is required , Select a location or enter a custom one.');
				return;
			}

			if(createForm.find('.specializationSelector').val() == undefined){
				swal('Task specialization is required.');
				return;
			}

			if(createForm.find('.skillsSelector').val() == undefined){
				swal('Select at least one Skill for your task.');
				return;
			}

			taskDataObject['title'] = createForm.find('[name="title"]').val();
			taskDataObject['description']       = createForm.find('[name="description"]').val();
			taskDataObject['specialization_id'] = createForm.find('.specializationSelector').val();
			taskDataObject['skills']      = createForm.find('.skillsSelector').val();
			taskDataObject['location_id'] = createForm.find('.locationSelector').val();


			if(createForm.find('.locationSelector').val() && createForm.find('[name="custom_location"]').val() == undefined){
				taskDataObject['location_id'] = createForm.find('.locationSelector').val();

				$.ajax({
					method: 'GET',
					url : '/taskers/',
					data : {
						'location' : createForm.find('.locationSelector').val(),
						'specialization' : createForm.find('.locationSelector').val()
					},
					dataType: 'json',
					beforeSend: function(){
						navigationButton.attr('disabled' , true);
					},
					complete: function(){
						navigationButton.attr('disabled' , false);
					},
					success : function(response){
						let taskersTemplate = ``;
						if(response.data.length == 0){
							noTaskersFlag = true;
							let taskersTemplate = makeNoTaskersTemplate();
						}else{
							for(let tasker of response.data){
								taskersTemplate += makeTaskerTemplate(tasker);
							}
						}
						taskersDiv.append(taskersTemplate);
						progressbarElements.eq(1).addClass('active');
						taskCreatorNavigator.navigateForward();
					},
					error : function(response){
						swal(
							'Sorry',
							'Something went wrong .. try again',
							'warning'
						);
						return;
					}
				});
			}else{
				if(createForm.find('[name="custom_location"]').val() == ''){
					swal('Task location is required');
					return;
				}

				taskDataObject['custom_location'] = createForm.find('[name="custom_location"]').val();
				progressbarElements.eq(1).addClass('active');
				progressbarElements.eq(2).addClass('active');
				taskCreatorNavigator.navigateForward();
				taskCreatorNavigator.navigateForward();
				navigationButton.val('Finish');
				swal('Since you selected a custom location' , 'Your task will be moved to pending page so taskers can apply to it later');
			}
		}else if(currentIndexNavigation == 1){
			if(noTaskersFlag == false){
				swal({
					title: 'You did not select any tasker !',
					text:  'Your task will be moved to pending page so taskers can apply to it later!',
					type:  'info',
					showCancelButton: true,
					closeOnConfirm: true
				},function(){
					progressbarElements.eq(2).addClass('active');
					taskCreatorNavigator.navigateForward();
					navigationButton.val('Finish');
					return;
				});
			}
		}else if(currentIndexNavigation == 2){
			if($('[name="price"]').val() == ''){
				swal('Enter Task Price ..');
				return;
			}

			if(isNaN( $('[name="price"]').val() )){
				swal('Task Price must be a number ..');
				return;
			}

			taskDataObject['price'] = $('[name="price"]').val();
			taskDataObject['payment_method'] = $('[name="payment_method"]').val();
			
			$.ajax({
				method: 'POST',
				url  : createForm.attr('action'),
				data : taskDataObject,
				dataType : 'json',
				beforeSend: function(){
					navigationButton.prop('disabled' , true);
				},
				complete: function(){
					navigationButton.prop('disabled' , false);
				},
				success : function(response){
					window.location.replace('/tasks/'+(response.data)['id']);
				},
				error : function(response){
					swal('Error !' , 'Something went wrong , try again.' , 'warning');
					return;
				}
			});
		}
	});

	function makeTaskerTemplate(taskerData){
		return `<div class="col-md-4 col-sm-6">
            <div class="card-container">
                <div class="card">
                    <div class="front">
                        <div class="cover">
                            <img src="/images/card-cover.png"/>
                        </div>
                        <div class="user">
                            <img class="img-circle" src="/images/users/profilePictures/th-${taskerData['picture']}"/>
                        </div>
                        <div class="content">
                            <div class="main">
                                <h3 class="name">${taskerData['name']}</h3>
                                <p class="profession">Tasker</p>

                                <p class="text-center">${taskerData['bio']}</p>
                            </div>
                            <div class="footer">
                                <div class="rating">
                                    <i class="fa fa-mail-forward"></i> Work Statistics
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="back">
                        <div class="header">
                            <!-- link to tasker Profile -->
                            <h5 class="motto"><a href="/${taskerData['profile']}/">View Full Profile</a></h5>
                        </div>
                        <div class="content">
                            <div class="main">
                                <h4 class="text-center bolder">Specialized In </h4>
                                <br>
                                <p class="text-center">${taskerData['specializations']}...</p>

                                <div class="stats-container">
                                    <div class="stats">
                                        <h4>${taskerData['rating']}%</h4>
                                        <p>
                                            Rating
                                        </p>
                                    </div>
                                    <div class="stats">
                                        <h4>${taskerData['tasks_completed']}</h4>
                                        <p>
                                            Tasks
                                        </p>
                                    </div>
                                    <div class="stats">
	                                    <h4>${taskerData['price']}$</h4>
                                        <p>
                                        	Price
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="footer">
                            <div class="social-links text-center">
                                <div class="btn btn-sm btn-success taskerSelector" data-taskerId=${taskerData['id']}>
                                    Choose This Tasker
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>`;
	}

	function makeNoTaskersTemplate(){
		return `<h3>No taskers found .. your task will be moved to pending page so taskers can apply to it later.</h3>`;
	}

};



let specializationSelector = function(){
	$('.specializationSelector').select2({
		placeholder : 'Task Specialization ..',
		allowClear  : true
	});
};


let locationSelector = function(){
	$('.locationSelector').select2({
		placeholder : 'Task Location ..',
		allowClear  : true
	});
};


let skillsSelector = function(){
	$('.skillsSelector').select2({
		placeholder : 'Task Required Skills ..',
		allowClear  : true,
		maximumSelectionLength: 5
	});	
};


let divNavigator = function(parentDiv , which = 0){
	parentDiv = $(parentDiv);
	let navigationElementsArray;
	let navigationElementsVisibilityArray = [];

	function init(){
		navigationElementsArray = parentDiv.find('div[data-navigationNumber]');
		if(navigationElementsArray.length > 0){
			displayWhichAndHideOthers();
		}
	}

	function displayWhichAndHideOthers(){
		for(let i = 0 ; i < navigationElementsArray.length ; i++){
			if(i === which){
				navigationElementsArray.eq(i).css('display' , 'block');
				navigationElementsVisibilityArray.push('block');
				continue;
			}
			navigationElementsArray.eq(i).css('display' , 'none');
			navigationElementsVisibilityArray.push('none');
		}
	}

	function navigateForward(){
		if(isForwardAvailable()){
			let currentVisibleIndex = findCurrentVisibleIndex();
			//hideCurrent
			changeElementVisibility(currentVisibleIndex   , 'none');
			//showForward
			changeElementVisibility(currentVisibleIndex+1 , 'block');
			return;
		}
		console.log('No More Forward , Go back nigga!');
	}

	function isForwardAvailable(){
		return ((findCurrentVisibleIndex())+1 <= (navigationElementsArray.length)-1);
	}
	
	function navigateBackward(){
		if(isBackwardAvailable()){
			let currentVisibleIndex = findCurrentVisibleIndex();
			//hideCurrent
			changeElementVisibility(currentVisibleIndex   , 'none');
			//showBackward
			changeElementVisibility(currentVisibleIndex-1 , 'block');
			return;
		}
		console.log('No More Backward , move ur ass forward!');
	}

	function isBackwardAvailable(){
		return ((findCurrentVisibleIndex())-1 > -1);
	}

	function changeElementVisibility(elementIndex , visibility){
		navigationElementsArray[elementIndex] = $(navigationElementsArray[elementIndex]).css('display' , visibility);
		navigationElementsVisibilityArray[elementIndex] = visibility;
	}


	function findCurrentVisibleDiv(){
		return navigationElementsArray[findCurrentVisibleIndex()];
	}

	function findCurrentVisibleIndex(){
		return navigationElementsVisibilityArray.indexOf('block');
	}

	//Exsposed API
	return {
		init : init,
		navigateForward  : navigateForward,
		navigateBackward : navigateBackward,
		findCurrentVisibleDiv : findCurrentVisibleDiv ,
		findCurrentVisibleIndex : findCurrentVisibleIndex
	};

};


taskCreator();
specializationSelector();
locationSelector();
skillsSelector();