let chatPictures = (function(){
	let profilePictures = {};
	let otherUsers = publicMessaging.friend;

	//this should change !
	function addAuthUser(){
		profilePictures[getIdFromUri()] = $('[name="authUserProfilePicture"]').val();
	}


	function addOtherUsers(){
		for(let i = 0 ; i < otherUsers.length ; i++){
			profilePictures[$(otherUsers[i]).attr('data-friendnumber')] = $(otherUsers[i]).find('img').attr('src');
		}
	}

	/**
	 * Gets the user id From the address Bar
	 * @return int user id
	 */
	function getIdFromUri(){
		let path = window.location.pathname;
		let id = path.split('/')[2];
		return id;
	}
	
	function getAllPictures(){
		return profilePictures;
	}

	function getAuthUserPicture(){
		return profilePictures[getIdFromUri()];
	}

	function getUserPicture(id){
		return profilePictures[id];
	}

	addAuthUser();
	addOtherUsers();

	//Exsposed API
	return {
		getAllPictures : getAllPictures,
		getAuthUserPicture : getAuthUserPicture,
		getUserPicture : getUserPicture
	};
})();