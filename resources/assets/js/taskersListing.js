Vue.component('taskers',{
	template: '#taskersTemplate',
	data: function(){
		return {
			list:[],
			locations:[],
			specializations:[],
		}
	},
	methods:{
		fetchTaskersList: function() {
			this.$http.get('api/taskers', function(tasks) {
	        this.list = tasks;
	    	this.locations = [];
	    	this.specializations = [];

	    		// push all taskerslocations on locations[]
				for(i = 0; i < this.list.length ; i++){
					for(j = 0; j < this.list[i].locations.length ; j++){
						var taskersLocations = this.list[i].locations[j];
						if(this.locations.indexOf(taskersLocations.name) == -1){
							this.locations.push(taskersLocations.name.toLowerCase());
						}
					}	
				}

				//push all specializations name on specializations[]
				for(i = 0; i < this.list.length ; i++){	
					for(j = 0; j < this.list[i].specializations.length ; j++){
						var specializationTask = this.list[i].specializations[j];

						if(this.specializations.indexOf(specializationTask) == -1){
							this.specializations.push(specializationTask.name.toLowerCase());
						}
					}
				}

	        }.bind(this));
		}
	},
	ready: function() {
		this.fetchTaskersList();
	},
});

new Vue({
	el: 'body',
	data:{
		searchBox:'',
		location:'',
		specialization:''
	},
});