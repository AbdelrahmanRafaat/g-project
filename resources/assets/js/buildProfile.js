/**
 * Handles The Process of building up the profile ..
 * 1-tooltipsActivator
 * 2-typePicker
 * 3-validator(stepsValidators - serverValidators)
 * 4-navigator(stepsNavigator)
 * 5-geolocator
 * 6-profilePictureChanger
 */

/*typeSelection -> Unbind , Remove Div*/
/*Client -> Remove 2 Divs*/

//Contains domCaching|EventBinding.
let buildProfileModule = function(){

	let containerDiv  = $('form');
	let taskerOrClient = containerDiv.find('div.taskerOrClient');
	let userEmailAddressBioDiv = containerDiv.find('div.userEmailAddressBio');
	let taskerInfo = containerDiv.find('div.taskerInfo');
	let taskerLocationsDiv = containerDiv.find('div.taskerLocations');


	typePicker(taskerOrClient);
	specilizationsPicker(taskerInfo);

	buildProfileManualGeoLocator(userEmailAddressBioDiv.find('.addressSelection') , userEmailAddressBioDiv.find('.addressInput') );
	taskersLocations();

	buildProfilePictureChanger();
	buildProfiletooltipsActivator();
	buildProfileMultiSelectActivator();
	
	buildProfileNavigator(containerDiv);

};

/*Error Handling , Exceptions .. loud|silent mode , More Validations(files...) , custom messages*/
let clientValidator = function(form , rules , validationSteps , silent){
	
	let inputElements;
	let selectElements;
	let textAreaElements;
	
	let elements = {};
	let elementsNames = [];
	let errorBag = {};

	function init(){
		inputElements    = form.find('input');
		selectElements   = form.find('select');
		textAreaElements = form.find('textarea');

		for(let i = 0 ; i < inputElements.length ; i++){
			let inputElement = $(inputElements[i]);
			let elementType  = inputElement.prop('type');
			let elementName  = inputElement.prop('name');
			if(elementType != 'button'){
				addElement(elementName , elementType);
			}
		}

		for(let i = 0 ; i < selectElements.length ; i++){
			let selectName = $(selectElements[i]).prop('name');
			addElement(selectName , 'select');
		}

		for(let i = 0 ; i < textAreaElements.length ; i++){
			let textareaName = $(textAreaElements[i]).prop('name');
			addElement(textareaName , 'textarea');
		}

		addEventListenerToUpdateValue();
	}


	function addElement(elementName , elementType){
		if( !elementExist(elementName) && elementHasRules(elementName) ){
			elements[elementName] = {
				type  : elementType,
				value : (elementType == 'radio' || elementType == 'select' || elementType == 'checkbox') ? [] : '',
				elementRules : (rules[elementName] == undefined) ? '' :  rules[elementName].split('|').map(function(rule){return rule.trim().toLowerCase();})
			};
			elementsNames.push(elementName);
		}
	}

	function elementExist(elementName){
		return elements.hasOwnProperty(elementName);
	}

	function elementHasRules(elementName){
		return Object.keys(rules).indexOf(elementName) != -1;
	} 


	function addEventListenerToUpdateValue(){
		for(let i = 0; i < elementsNames.length ; i++){
			switch( elements[ elementsNames[i] ].type ){
			case 'text':
			case 'password':
			case 'email':
			case 'textarea':
				addTextListener(elementsNames[i]);
				break;
			case 'radio':
			case 'checkbox':
				addCheckListener(elementsNames[i]);
				break;
			case 'select':
				addSelectListener(elementsNames[i]);
				break;
			}
		}
	}

	function addTextListener(textElementName){
		form.find('[name="'+textElementName+'"]').on('input' , function(){
			updateElementValue(textElementName , $(this).val());
		});
	}

	function addSelectListener(selectElementName){
		form.find('select[name="'+selectElementName+'"]').on('change',function(){
			if($(this).val() == null){
				updateElementValue(selectElementName , []);
				return;
			}
			updateElementValue(selectElementName , $(this).val());
		});
	}

	function addCheckListener(checkElementName){
		form.find('[name="'+checkElementName+'"]').on('change',function(){
			let values = $('[name="'+checkElementName+'"]:checked').map(function () {
				return this.value;
			}).get();
			updateElementValue(checkElementName , values);
		});
	}

	function updateElementValue(elementName , newValue){
		elements[elementName].value = newValue;
	}

	function validateStep(stepNumber){
		errorBag = {};
		let stepElements = getStepElements(stepNumber);
		for(let i = 0 ; i < stepElements.length ; i++){
			validateElement(stepElements[i]);
		}
		return (emptyErrorBag()) ? true : errorBag;
	}

	function getStepElements(stepNumber){
		if(validationSteps[stepNumber]){
			return validationSteps[stepNumber].split('|');
		}
	}

	function validateOneElement(elementName){
		errorBag = {};
		validateElement(elementName);
		return (emptyErrorBag()) ? true : errorBag;
	}

	function validateAll(){
		errorBag = {};
		for(let i = 0 ; i < elementsNames.length ; i++){
			validateElement(elementsNames[i]);
		}
		return (emptyErrorBag()) ? true : errorBag;
	}

	function validateElement(elementName){
		let element = elements[ elementName ];
		let elementRules = element.elementRules;
		for(let j = 0 ; j < elementRules.length ; j++){
			let valueType = getElementValueType(element);
			let ruleValidationResult = findRuleValidator(elementRules[j] , element , valueType);
			if(ruleValidationResult != true){
				addErrorToErrorBag( elementName , ruleValidationResult);
			}
		}
	}

	function getElementValueType(element){
		if(element.value.constructor === String){
			return 'string';
		}else if(element.value.constructor === Array){
			return 'list';
		}
		//throwException .. unkown value
	}

	function findRuleValidator(rule , element , valueType){
		if(valueType == 'string'){
			if(rule == 'required'){
				return (requiredStringValidator(element.value)) ? true : requiredStringErrorMessage();
			}
			if(isMinMaxValidation(rule)){
				return mapMinMax(rule , element , 'string');
			}
		}else if(valueType == 'list'){
			if(rule == 'required'){
				return (requiredListValidator(element.value)) ? true : requiredListErrorMessage();
			}
			if(isMinMaxValidation(rule)){
				return mapMinMax(rule , element , 'list');
			}
		}
	}

	function addErrorToErrorBag(elementName , error){
		if(!errorBag[ elementName ]){
			errorBag[ elementName ] = [elementName + error];
			return;
		}
		errorBag[ elementName ].push(elementName + error);
	}

	function emptyErrorBag(){
		return (Object.keys(errorBag).length > 0) ? false : true;
	}

	function isMinMaxValidation(rule){
		let regex = /(min|max){1}:{1}[0-9]+/ig;
		return rule.match(regex);
	}

	function mapMinMax(rule , element , type){
		let condition = rule.split(':')[0];
		let count = rule.split(':')[1];

		if(condition == 'max' && type == 'list'){
			return (maxListValidator( element.value , count) ) ? true : maxListErrorMessage(count);
		}
		else if(condition == 'max' && type == 'string'){
			return (maxStringValidator(element.value , count)) ? true : maxStringErrorMessage(count);
		}
		else if(condition == 'min' && type == 'list'){
			return (minListValidator( element.value , count) ) ? true : minListErrorMessage(count);
		}
		else if(condition == 'min' && type == 'string'){
			return (minStringValidator(element.value , count)) ? true : minStringErrorMessage(count);
		}
	}

	function requiredStringValidator(string){
		return (string.length === 0) ? false : true;
	}

	function requiredStringErrorMessage(){
		return ' is required.';
	}

	function minStringValidator(string , count){
		return (string.length > count) ? true : false;
	}

	function minStringErrorMessage(count){
		return ' must be at least '+count+' characters.';
	}

	function maxStringValidator(string , count){
		return (string.length <= count) ? true : false;
	}

	function maxStringErrorMessage(count){
		return ' is too long (max '+count+' characters).';
	}

	function requiredListValidator(list){
		return (list.length > 0) ? true : false;
	}

	function requiredListErrorMessage(){
		return ' selection is required.';
	}

	function minListValidator(list , count){
		return (list.length > count) ? true : false;
	}

	function minListErrorMessage(count){
		return ' must have at least '+count+' selections.';
	}

	function maxListValidator(list , count){
		return (list.length <= count) ? true : false;
	}

	function maxListErrorMessage(count){
		return ' can`t have more than '+count+' selections';
	}



	//Exsposed API
	return {
		init : init,
		elements : elements,
		validateAll : validateAll,
		validateStep : validateStep,
		validateOneElement : validateOneElement
	};
};

let buildProfileServerValidator = function(profileNavigator , withEmail){
	
	let emailInput = $('input[name="email"][type="email"]');
	let usernameInput = $('input[name="username"][type="text"]');
	let token = $('[name="_token"]');

	function validateUsername( triggerButton , navigateForward , callback , serverCallBack , serverData){//serverData ?!!
		$.ajax({
			method: 'POST',
			url : '/users/buildProfile/checkUsername',
			data : 'username='+usernameInput.val()+'&_token='+token.val(),
			success : function(){
				console.log('Success');
				if(callback){
					callback(triggerButton , navigateForward , false , serverCallBack , serverData);
				}else{
					if(navigateForward){
						profileNavigator.navigateForward();
					}else{
						serverCallBack(serverData);//serverData ?!!
					}
				}
			},
			error : function(){
				console.log('WTF');
				if(!callback){
					feedBackProvider().showErrorMessage('username is already taken , try another one.');
				}else{
					callback( triggerButton , navigateForward , 'username is already taken , try another one.');
				}
			},
			beforeSend: function(){
				$(triggerButton).prop('disabled' , true);
			},
			complete: function(){
				$(triggerButton).prop('disabled' , false);
			}
		});
	}

	function validateEmail( triggerButton , navigateForward , previousValidationErrors , serverCallBack , serverData){//serverData ?!!
		$.ajax({
			method: 'POST',
			url : '/users/buildProfile/checkEmail',
			data : 'email='+emailInput.val()+'&_token='+token.val(),
			success : function(){
				if(previousValidationErrors){
					feedBackProvider().showErrorMessage(previousValidationErrors);
				}else{
					if(navigateForward){
						profileNavigator.navigateForward();
					}else{
						serverCallBack(serverData); //serverData ?!!!
					}
				}
			},
			error : function(){
				if(previousValidationErrors){
					feedBackProvider().showErrorMessage(previousValidationErrors+'\n'+'Email is already taken , try another one.');
				}else{
					feedBackProvider().showErrorMessage('Email is already taken , try another one.');
				}
			},
			beforeSend: function(){
				$(triggerButton).prop('disabled', true);
			},
			complete: function(){
				$(triggerButton).prop('disabled', false);
			}
		});
	}

	function validateUsernameAndEmail(triggerButton , navigateForward , serverCallBack , serverData){
		if(withEmail){
			validateUsername( triggerButton , navigateForward , validateEmail , serverCallBack , serverData);
		}else{
			validateUsername(triggerButton , navigateForward , false , serverCallBack , serverData);
		}
	}

	return{
		validateEmail : validateEmail,
		validateUsername : validateUsername,
		validateUsernameAndEmail : validateUsernameAndEmail
	}; 
};

/*enhancement -> Animation , callbacks|promise like syntax , */
let modulesNavigator = function(parentDiv , which = 0){
	parentDiv = $(parentDiv);
	let navigationElementsArray;
	let navigationElementsVisibilityArray = [];

	function init(){
		navigationElementsArray = parentDiv.find('div[data-navigationNumber]');
		if(navigationElementsArray.length > 0){
			displayWhichAndHideOthers();
		}
	}

	function displayWhichAndHideOthers(){
		for(let i = 0 ; i < navigationElementsArray.length ; i++){
			if(i === which){
				navigationElementsArray.eq(i).css('display' , 'block');
				navigationElementsVisibilityArray.push('block');
				continue;
			}
			navigationElementsArray.eq(i).css('display' , 'none');
			navigationElementsVisibilityArray.push('none');
		}
	}

	function navigateForward(){
		if(isForwardAvailable()){
			let currentVisibleIndex = findCurrentVisibleIndex();
			//hideCurrent
			changeElementVisibility(currentVisibleIndex   , 'none');
			//showForward
			changeElementVisibility(currentVisibleIndex+1 , 'block');
			return;
		}
		console.log('No More Forward , Go back nigga!');
	}

	function isForwardAvailable(){
		return ((findCurrentVisibleIndex())+1 <= (navigationElementsArray.length)-1);
	}
	
	function navigateBackward(){
		if(isBackwardAvailable()){
			let currentVisibleIndex = findCurrentVisibleIndex();
			//hideCurrent
			changeElementVisibility(currentVisibleIndex   , 'none');
			//showBackward
			changeElementVisibility(currentVisibleIndex-1 , 'block');
			return;
		}
		console.log('No More Backward , move ur ass forward!');
	}

	function isBackwardAvailable(){
		return ((findCurrentVisibleIndex())-1 > -1);
	}

	function changeElementVisibility(elementIndex , visibility){
		navigationElementsArray[elementIndex] = $(navigationElementsArray[elementIndex]).css('display' , visibility);
		navigationElementsVisibilityArray[elementIndex] = visibility;
	}


	function findCurrentVisibleDiv(){
		return navigationElementsArray[findCurrentVisibleIndex()];
	}

	function findCurrentVisibleIndex(){
		return navigationElementsVisibilityArray.indexOf('block');
	}

	//Exsposed API
	return {
		init : init,
		navigateForward  : navigateForward,
		navigateBackward : navigateBackward,
		findCurrentVisibleDiv : findCurrentVisibleDiv ,
		findCurrentVisibleIndex : findCurrentVisibleIndex
	};
};

let buildProfilePictureChanger = function(){
	
	let options = {
		url : '/user/'+getIdFromUri()+'/profile/picture/',
		params : {
			_token: $('input[name="_token"]').val()
		},
		paramName : 'profilePicture' ,
		autoProcessQueue : true,
		addRemoveLinks   : true,
		parallelUploads  : 1,
		maxFiles : 1,
		maxFilesize : 3,
		acceptedFiles : '.jpg, .jpeg, .png, .bmp'
	};

	let dropzoneInstance = dropzoneFileUploader(document.querySelector('#profilePictureUploader') , options , false);

	dropzoneInstance.on('success' , function( file , response ){
		$('input[name="profilePicture"][type="hidden"]').val(response);
	});

	dropzoneInstance.on('maxfilesexceeded' , function(file){
		this.removeFile(file);
	});

	dropzoneInstance.on('removedfile' , function(){
		if(dropzoneInstance.files.length == 0){
			$('input[name="profilePicture"][type="hidden"]').val('');
		}
	});

	$('div.defaultProfilePicture').on('click',function(){
		$(this).css('display','none');
		$('div.dropzoneUploader').css('display','block');
	});
};

let dropzoneFileUploader = function(targetDiv , options , autoDiscover){
	Dropzone.autoDiscover = autoDiscover;
	let dropzoneInstance = new Dropzone( targetDiv , options);
	return dropzoneInstance;
};

let ManualGeoLocator = function(countries , states){
	
	function generateOptions(optionsArray){
		let options = '';

		for(let i = 0 ; i < optionsArray.length ; i++){
			options += '<option value="'+optionsArray[i]+'">'+upperCaseFirst(optionsArray[i])+'</option>';
		}
		return options;
	}

	function upperCaseFirst(string){
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
	
	function getCountries(){
		return countries;
	}

	function getCountryStates(countryName){
		let countryStates = [];
		for(let i = 0 ; i < states.length ; i++){
			if(states[i].country == countryName){
				countryStates.push(states[i]);
			}
		}
		return countryStates;
	}

	function getStateCities(countryName , stateName){
		for(let i = 0 ; i < states.length ; i++){
			if(states[i].country == countryName &&  states[i].name == stateName){
				return states[i].cities;
			}
		}
	}

	function dryStatesObjects(statesObjectsArray){
		let statesNames = [];
		for(let i = 0 ; i < statesObjectsArray.length ; i++){
			statesNames.push(statesObjectsArray[i].name);
		}
		return statesNames;
	}

	return{
		generateOptions  : generateOptions,
		getCountries     : getCountries,
		getCountryStates : getCountryStates,
		getStateCities   : getStateCities,
		dryStatesObjects : dryStatesObjects
	};
};

let buildProfileManualGeoLocator = function(addressSelectionDiv , addressInput){
	//FromServer
	let countries = ['egypt'];
	let states    = [
		{
			country : 'egypt',
			name : 'cairo',
			cities : ['masr-el-gdeda','madent-nasr','west-el-balad','el-shrouk']
		},
		{
			country : 'egypt',
			name : 'giza',
			cities : ['el-hawmdeya','giza','hadyek-el-ahram','el-haram']
		},
		{
			country : 'egypt',
			name : 'alexandria',
			cities : ['el-agamy','seedy-beshr','el-montazah']
		}
	];

	addressSelectionDiv = $(addressSelectionDiv);
	addressInput = $(addressInput);
	
	let manualGeo = ManualGeoLocator(countries , states);
	let countriesSelection = addressSelectionDiv.find('select[name="country"]');
	let statesSelection    = addressSelectionDiv.find('select[name="state"]');
	let citiesSelection    = addressSelectionDiv.find('select[name="city"]');

	countriesSelection.append(createCountriesOptions());

	countriesSelection.on('change',function(){
		let newCountry = countriesSelection.val();
		handleCountryChange(newCountry);
	});

	//abstract
	function handleCountryChange(newCountry){
		deleteOptions(statesSelection);
		statesSelection.append(createStatesOptions(newCountry));
		deleteOptions(citiesSelection);
		resetAddressInput();
		updateAddressInput(generateCountryAddress());
	}

	statesSelection.on('change',function(){
		let country  = countriesSelection.val();
		let newState = statesSelection.val();
		handleStateChange(country , newState);
	});

	//abstract
	function handleStateChange(country , newState){
		deleteOptions(citiesSelection);
		citiesSelection.append(createCitiesOptions( country , newState));
		resetAddressInput();
		updateAddressInput(generateCountryStateAddress());
	}

	citiesSelection.on('change' , function(){
		resetAddressInput();
		updateAddressInput(generateFullAddress());		
	});

	function createCountriesOptions(){
		return manualGeo.generateOptions(manualGeo.getCountries());
	}

	function createStatesOptions(countryName){
		return manualGeo.generateOptions( manualGeo.dryStatesObjects(manualGeo.getCountryStates(countryName)) );
	}

	function createCitiesOptions(countryName , stateName){
		return manualGeo.generateOptions(manualGeo.getStateCities(countryName , stateName));
	}

	function deleteOptions(selection){
		let options = selection.find('option');
		for(let i = 1 ; i < options.length ; i++){
			options.eq(i).remove();
		}
	}

	function generateCountryAddress(){
		let country  = countriesSelection.val();
		if(country !== 'nothing'){
			return country;
		}
	}

	function generateCountryStateAddress(){
		let country  = countriesSelection.val();
		let state    = statesSelection.val();

		if(country != 'nothing' && state != 'nothing' ){
			return country+','+state;
		}
	}

	function generateFullAddress(){
		let country  = countriesSelection.val();
		let state    = statesSelection.val();
		let city     = citiesSelection.val();

		if(country != 'nothing' && state != 'nothing' && city != 'nothing'){
			return country+','+state+','+city;
		}
	}

	function updateAddressInput(newValue){
		addressInput.val(newValue);
	}

	function resetAddressInput(){
		updateAddressInput('');
	}
};

let taskersLocations = function(){
	let visibleLocations = 0;
	let container   = $('div.taskerLocations');
	let addLocationButton = container.find('div.addLocationButton');
	let locations = container.find('.taskerLocation');
	let removeLocationButtons = locations.find('.removeLocation');
	let labels = [];
	let manualGeoLocators = [];

	function init(){
		for(let i = 0 ; i < locations.length ; i++){
			if(locations.eq(i).css('display') == 'block'){
				visibleLocations++;
			}
			labels.push(locations.eq(i).find('label').html());
			manualGeoLocators.push( buildProfileManualGeoLocator(locations.eq(i) , locations.eq(i).find('input.addressInput')) );
		}
	}

	init();

	function getLastVisibleLocation(){
		let lastVisibleLocation;
		for(let i = 0 ; i < locations.length ; i++){
			if(locations.eq(i).css('display') == 'block'){
				lastVisibleLocation = locations.eq(i);
			}
		}
		return (lastVisibleLocation) ? lastVisibleLocation : false; 
	}

	function getFirstUnvisibleLocation(){
		for(let i = 0 ; i < locations.length ; i++){
			if(locations.eq(i).css('display') == 'none'){
				return locations.eq(i);
			}
		}
		return false;
	}

	addLocationButton.on('click',function(){
		let firstUnvisibleLocation = $(getFirstUnvisibleLocation());
		if(visibleLocations > 0){
			addLocationToExisitingOnes(firstUnvisibleLocation);
			handleAddLocationButton();
			return;
		}

		addfirstLocation(firstUnvisibleLocation);
		handleAddLocationButton();
		return;
	});

	function addLocationToExisitingOnes(firstUnvisibleLocation){
		$(firstUnvisibleLocation).css('display' , 'block');
		visibleLocations++;
		for(let i = 0 , j = 0 ; i < locations.length ; i++){
			if($(locations[i]).css('display') == 'block'){
				$(locations[i]).find('label').html(labels[j]);
				j++;
			}
		}
	}

	function addfirstLocation(firstUnvisibleLocation){
		firstUnvisibleLocation = $(firstUnvisibleLocation);
		firstUnvisibleLocation.find('label').html(labels[0]);
		firstUnvisibleLocation.css('display' , 'block');
		visibleLocations++;
	}

	removeLocationButtons.on('click',function(){
		let lastVisibleLocation = getLastVisibleLocation();
		if(lastVisibleLocation){

			let lastVisibleLocationNumber = Number($(lastVisibleLocation).attr('data-locationNumber'));
			let removedLocation = $(this).parent().parent().parent();
			removedLocation.find('input.addressInput').val('');
			removedLocation.find('select[name="country"]').val('nothing').change();
			let removedLocationNumber = Number(removedLocation.attr('data-locationNumber'));
			
			if(lastVisibleLocationNumber > removedLocationNumber){
				swapLabels(removedLocation , removedLocationNumber);
			}

			removedLocation.css('display','none');
			visibleLocations--;
			handleAddLocationButton();
		}
	});
	
	function swapLabels(removedLocation , removedLocationNumber){
		let oldLabel = $(removedLocation).find('label').html();
		let newLabel = '';
		for(let i = removedLocationNumber+1 ; i < locations.length ; i++){
			if($(locations[i]).css('display') == 'block'){//swapLabels
				newLabel = $(locations[i]).find('label').html();
				$(locations[i]).find('label').html(oldLabel);
				oldLabel = newLabel;
			}
		}
	}

	function handleAddLocationButton(){
		if(visibleLocations >= locations.length){
			$(addLocationButton).css('display','none');
			return;
		}
		$(addLocationButton).css('display','block');
	}
};

let typePicker = function(taskerOrClient){
	let chooseTypeDiv = $(taskerOrClient).find('div.choice');

	chooseTypeDiv.on('click' , function(){
		let selectedType = $(this);
		let selectAccountType  = selectedType.attr('data-accountType');
		selectedType.addClass('active').find('input[name="type"]').prop('checked' , true);
		disableOtherType(selectAccountType);
	});

	function disableOtherType(selectAccountType){
		if(selectAccountType == 'tasker'){
			chooseTypeDiv.eq(0).removeClass('active').find('input[name="type"]').prop('checked' , false).change();
		}else if(selectAccountType == 'client'){
			chooseTypeDiv.eq(1).removeClass('active').find('input[name="type"]').prop('checked' , false).change();
		}
	}
};

let specilizationsPicker = function(taskerInfo){
	taskerInfo.find('div.choice').on('click' , function(){
		let selectedSpecilization = $(this);
		if(selectedSpecilization.hasClass('active')){
			selectedSpecilization.removeClass('active').find('input[name="specilizations"]').prop('checked' , false).change();
		}else{
			selectedSpecilization.addClass('active').find('input[name="specilizations"]').prop('checked' , true).change();
		}
	});
};

let buildProfiletooltipsActivator = function(){
	$('[data-toggle="tooltip"]').tooltip();
};

let buildProfileMultiSelectActivator = function(){
	$('.skillsSelectionBox').select2({placeholder: 'Select your skills ..'});
	$('.salarySelectionBox').select2({ placeholder: 'Select your expected salary ..' , maximumSelectionLength: 1});
	$('.genderSelectionBox').select2({ placeholder: 'Select your gender ..' , maximumSelectionLength: 1});
};

let buildProfileClientValidator = function(containerDiv){
	let rules = {
		type   : 'required',
		username : 'required|max:40|min:3',
		gender   : 'required',
		bio  : 'required|min:10|max:500',
		specilizations : 'required|max:3',
		skills : 'required',
		salary : 'required'
	};
	let steps = {
		0 : 'type',
		2 : 'specilizations|skills|salary'
	};

	if(!emailInputExist()){
		steps[1] = 'username|bio|gender';
	}else{
		rules.email = 'required';
		steps[1] = 'email|username|bio|gender';
	}

	let validator = clientValidator(containerDiv , rules , steps);
	validator.init();

	return {
		validator : validator
	};
};

//Spagetti As Fuck --> refactor
let buildProfileNavigator = function(containerDiv){
	let profileNavigator = modulesNavigator(containerDiv);
	profileNavigator.init();

	let profileValidator = buildProfileClientValidator(containerDiv).validator;
	let serverValidator  = buildProfileServerValidator(profileNavigator , emailInputExist() );

	let dataCollector = buildProfileDataCollector();

	let finishButton = $('input[name="finish"][type="button"]');
	let nextButton   = $('input[name="next"][type="button"]');
	

	finishButton.on('click',function(){
		let currentNavigationIndex = profileNavigator.findCurrentVisibleIndex();

		if(currentNavigationIndex == 1){
			let stepValidationResult   = profileValidator.validateStep(currentNavigationIndex);
			let addressValidationCheck = addressValidator().isValidAddress($('input[name="address"][type="text"]').val());
			let validationErrorMessage = '';
			if( addressValidationCheck == false || stepValidationResult != true){
				if(stepValidationResult != true){
					validationErrorMessage = createErrorMessage(stepValidationResult);
					if(addressValidationCheck == false){
						validationErrorMessage += ' Select country , state and city for your address.';
					}
				}else{
					validationErrorMessage = 'Select country , state and city for your address.';
				}
			}

			if(validationErrorMessage != ''){
				feedBackProvider().showErrorMessage(validationErrorMessage);
				return;
			}

			let otherElements = {};
			otherElements.address = $('input[name="address"][type="text"]').val();
			if($('input[type="hidden"][name="profilePicture"]').val().length > 48){
				otherElements.profilePicture = $('input[type="hidden"][name="profilePicture"]').val();
			}
			dataCollector.setValidatedElements(profileValidator.elements);
			dataCollector.setOtherElements(otherElements);

			serverValidator.validateUsernameAndEmail($(this) , false , dataSender().sendClientData , dataCollector.getOverAllClientData());
			return;
			
		}

		if(currentNavigationIndex == 3){
			dataCollector.setValidatedElements(profileValidator.elements);
			let otherElements = {};
			let firstLocation   = $('input[name="firstLocation"][type="text"]').val();
			let secoundLocation = $('input[name="secoundLocation"][type="text"]').val(); 
			let thirdLocation   = $('input[name="thirdLocation"][type="text"]').val();

			otherElements.address = $('input[name="address"][type="text"]').val();
			if($('input[type="hidden"][name="profilePicture"]').val().length > 48){
				otherElements.profilePicture = $('input[type="hidden"][name="profilePicture"]').val();
			}

			if($('.taskerLocation[data-locationNumber="0"]').css('display') == 'block'){
				if(addressValidator().isValidAddress(firstLocation)){
					otherElements.firstLocation = firstLocation;
				}else{
					feedBackProvider().showErrorMessage('Select country , state and city for every location you have.');
					return;
				}
			}

			if($('.taskerLocation[data-locationNumber="1"]').css('display') == 'block'){
				if(addressValidator().isValidAddress(secoundLocation)){
					otherElements.secoundLocation = secoundLocation;
				}else{
					feedBackProvider().showErrorMessage('Select country , state and city for every location you have.');
					return;
				}
			}

			if($('.taskerLocation[data-locationNumber="2"]').css('display') == 'block'){
				if(addressValidator().isValidAddress(thirdLocation)){
					otherElements.thirdLocation = thirdLocation;
				}else{
					feedBackProvider().showErrorMessage('Select country , state and city for every location you have.');
					return;
				}
			}
			
			dataCollector.setOtherElements(otherElements);
			dataSender().sendTaskerData(dataCollector.getOverAllTaskerData());
		}
	});

	nextButton.on('click' , function(){
		let currentNavigationIndex = profileNavigator.findCurrentVisibleIndex();
		let stepValidationResult   = profileValidator.validateStep(currentNavigationIndex);
		let addressValidationCheck = addressValidator().isValidAddress($('input[name="address"][type="text"]').val());
		let validationErrorMessage = '';
		if( (addressValidationCheck == false || stepValidationResult != true) && currentNavigationIndex == 1 ){
			if(stepValidationResult != true){
				validationErrorMessage = createErrorMessage(stepValidationResult);
				if(addressValidationCheck == false){
					validationErrorMessage += ' Select country , state and city for your address.';
				}
			}else{
				validationErrorMessage = 'Select country , state and city for your address.';
			}
		}
		if(currentNavigationIndex != 1){
			if(stepValidationResult != true){
				validationErrorMessage = createErrorMessage(stepValidationResult);
			}
		}

		if(validationErrorMessage != ''){
			feedBackProvider().showErrorMessage(validationErrorMessage);
			return;
		}

		if(currentNavigationIndex == 0 &&  profileValidator.elements.type.value[0] == 'client'){
			removeRequiredTaskerInfo();
			removeNextButton();
			showFinishButton();
		}
		if(currentNavigationIndex == 2 ){
			removeNextButton();
			showFinishButton();
		}

		if(currentNavigationIndex == 1 ){
			serverValidator.validateUsernameAndEmail($(this) , true);
			return;
		}

		profileNavigator.navigateForward();
	});

	function createErrorMessage(stepValidationResult){
		let errors = Object.keys(stepValidationResult);
		let errorMessage = '';
		for(let i = 0 ; i < errors.length ; i++){
			for(let j = 0 ; j < stepValidationResult[errors[i]].length ; j++){
				errorMessage += stepValidationResult[errors[i]][j] +' \n ';
			}
		}
		return errorMessage;
	}

	function removeRequiredTaskerInfo(){
		containerDiv.find('div.taskerInfo').remove();
		containerDiv.find('div.taskerLocations').remove();
	}

	function removeNextButton(){
		nextButton.remove();
	}

	function showFinishButton(){
		finishButton.css('display','inline-block');
	}
};

let feedBackProvider = function(){
	function showErrorMessage(message){
		swal({
			title : 'Fix the following errors.',
			text  :  message,
			type  : 'warning',
			confirmButtonText : 'Ok'
		});
	}

	//Exsposed API
	return {
		showErrorMessage : showErrorMessage
	}; 
};

let emailInputExist = function(){
	return ($('input[name="email"][type="email"]').length > 0) ? true : false; //not disabled
};

let buildProfileDataCollector = function(){
	let validatedElements;
	let otherElements;

	let overAlldata = {};

	function setValidatedElements(data){
		validatedElements = data;
	}

	function setOtherElements(data){
		otherElements = data;
	}

	function getOverAllClientData(){
		overAlldata = {};

		let validatedElementsKeys = Object.keys(validatedElements);
		let otherElementsKeys = Object.keys(otherElements);
		let taskersOnlyKeys = [ 'specilizations' , 'skills' , 'salary' ,'firstLocation' , 'secoundLocation' , 'thirdLocation'];
		
		for(let i = 0 ; i < validatedElementsKeys.length ; i++){
			if(taskersOnlyKeys.indexOf(validatedElementsKeys[i]) == -1){
				overAlldata[validatedElementsKeys[i]] = validatedElements[validatedElementsKeys[i]].value;
			}
		}

		for(let i = 0 ; i < otherElementsKeys.length ; i++){
			if(taskersOnlyKeys.indexOf(otherElementsKeys[i]) == -1){
				overAlldata[otherElementsKeys[i]] = otherElements[otherElementsKeys[i]];
			}
		}
		return overAlldata;
	}

	function getOverAllTaskerData(){
		overAlldata = {};

		let validatedElementsKeys = Object.keys(validatedElements);
		let otherElementsKeys = Object.keys(otherElements);
		
		for(let i = 0 ; i < validatedElementsKeys.length ; i++){
			overAlldata[validatedElementsKeys[i]] = validatedElements[validatedElementsKeys[i]].value;
		}

		for(let i = 0 ; i < otherElementsKeys.length ; i++){
			overAlldata[otherElementsKeys[i]] = otherElements[otherElementsKeys[i]];
		}
		return overAlldata;
	}

	return{
		setValidatedElements : setValidatedElements,
		setOtherElements : setOtherElements,
		getOverAllClientData  : getOverAllClientData,
		getOverAllTaskerData  : getOverAllTaskerData
	};
};

//should be added as a feature to validator 
//validateExternalElement
//matchPattern
let addressValidator = function (){
	function isValidAddress(value){
		let regex = /[a-z]+,[a-z]+,[a-z]+/ig;
		return (value.match(regex) != null ) ? true : false;
	}

	return {
		isValidAddress : isValidAddress
	};

};

let dataSender = function(){

	function sendClientData(clientData){
		swal({
			title : 'Saving your data.' ,
			text  : 'it will take some secounds.' ,
			type  : 'info',
			showCancelButton  : false,
			closeOnConfirm    : false,
			showConfirmButton : false
		});
		$.ajax({
			method: 'POST',
			url  : '/user/'+getIdFromUri()+'/buildProfile/createClientProfile',
			data : 'clientData='+JSON.stringify(clientData)+'&_token='+$('[name="_token"][type="hidden"]').val(),
			beforeSend: function(){
				$('.btn[name="finish"][type="button"]').prop('disabled' , true);
			},
			complete: function(){
				$('.btn[name="finish"][type="button"]').prop('disabled' , false);
			},
			success : function(){
				swal('Good job!' , 'Your profile is complete ^_^' , 'success');
				setTimeout(function(){
					window.location.replace('/');
				},1200);
			},
			error : function(){
				feedBackProvider().showErrorMessage('Sorry , Something went wrong please try again.');
			}
		});
	}

	function sendTaskerData(taskerData){
		swal({
			title : 'Saving your data.' ,
			text  : 'it will take some secounds.' ,
			type  : 'info',
			showCancelButton  : false,
			closeOnConfirm    : false,
			showConfirmButton : false
		});

		$.ajax({
			method: 'POST',
			url  : '/user/'+getIdFromUri()+'/buildProfile/createTaskerProfile',
			data : 'taskerData='+JSON.stringify(taskerData)+'&_token='+$('[name="_token"][type="hidden"]').val(),
			beforeSend: function(){
				$('.btn[name="finish"][type="button"]').prop('disabled' , true);
			},
			complete: function(){
				$('.btn[name="finish"][type="button"]').prop('disabled' , false);
			},
			success : function(){
				swal('Good job!' , 'Your Profile is Completed ^_^' , 'success');
				setTimeout(function(){
					window.location.replace('/');
				},1200);
			},
			error : function(){
				feedBackProvider().showErrorMessage('Sorry , Something went wrong please try again.');
			}
		});
		
	}

	return {
		sendClientData : sendClientData,
		sendTaskerData : sendTaskerData
	};

};

let getIdFromUri = function(){
	let path = window.location.pathname;
	let id = path.split('/')[2];
	return id;
};

buildProfileModule();