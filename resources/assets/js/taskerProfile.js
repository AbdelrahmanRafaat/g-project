let taskerAssignmentHandler = (function(){

	let acceptAssignmentButton  = $('.acceptAssignment');
	let declineAssignmentButton = $('.declineAssignment');
	let assigmentButtonsDiv = acceptAssignmentButton.parent();
	let taskDiv    = assigmentButtonsDiv.parent().parent().parent();
	let taskStatus = assigmentButtonsDiv.parent().parent();
	let updateTaskUrl = taskDiv.find('a').attr('href');


	acceptAssignmentButton.on('click' , function(){
		$.ajax({
			method: 'POST',
			url : updateTaskUrl,
			data : {
				_method : 'PUT',
				status  : 2
			},
			dataType : 'json',
			beforeSend: function(){
				assigmentButtonsDiv.find('button').prop('disabled' , true);
			},
			complete: function(){
				assigmentButtonsDiv.find('button').prop('disabled' , false);
			},
			success : function(response){
				taskDiv.find('.task-status').find('div').eq(1).html('Assigned - Work in progress..');
				swal('Job was assigned to you.' , 'Contact the client and get started' ,'success');
				assigmentButtonsDiv.remove();
			},
			error : function(response){
				swal('Something went wrong , try again');
			}
		});
	});

	declineAssignmentButton.on('click' , function(){
		$.ajax({
			method: 'POST',
			url  : updateTaskUrl,
			data : {
				_method : 'PUT',
				status  : 0
			},
			dataType : 'json',
			beforeSend: function(){
				assigmentButtonsDiv.find('button').prop('disabled' , true);
			},
			complete: function(){
				assigmentButtonsDiv.find('button').prop('disabled' , false);
			},
			success : function(response){
				taskDiv.find('.task-status').find('div').eq(1).html('You declined this task..');
				assigmentButtonsDiv.remove();
			},
			error : function(response){
				swal('Something went wrong , try again');
			}
		});
	});

})();

let markCompletedHandler = (function(){
	let markCompletedButton = $('.markCompleted');
	let buttonContainerDiv  = markCompletedButton.parent(); 
	let taskDiv = buttonContainerDiv.parent().parent().parent();
	let updateTaskUrl = taskDiv.find('a').attr('href');

	markCompletedButton.on('click' , function(){
		$.ajax({
			method: 'POST',
			url : updateTaskUrl,
			data : {
				'_method' : 'PUT',
				'status'  : 3
			},
			dataType : 'json',
			beforeSend: function(){
				markCompletedButton.prop('disabled' , true);
			},
			complete: function(){
				markCompletedButton.prop('disabled' , false);
			},
			success : function(response){
				taskDiv.find('.task-status').find('div').eq(1).html('Completed..');
				buttonContainerDiv.remove();
				swal('Good Job', 'Client will review your work.' , 'success');
			},
			error : function(response){
				swal('Something went wrong , try again');
			}
		});
	});

})();


let sendMessageHandler = (function(){
	let sendMessageDiv = $('.sendMessage');
	let to = sendMessageDiv.attr('userId');

	sendMessageDiv.on('click' , function(){
		swal({
			title : 'Your Message',
			text  : '',
			type: 'input',
			showCancelButton: true,
			closeOnConfirm: false,
			animation: 'slide-from-top',
			inputPlaceholder: 'Write Your Message',
			confirmButtonText: 'Send' 
		},
		function(inputValue){
			if(inputValue === false){
				return false;
			}
			if(inputValue === ''){
				swal.showInputError('You need to write something!');
				return false;  
			}

			$.ajax({
				method: 'POST',
				url  : '/user/'+windowLocation().getResourceId('user')+'/profile-message/',
				data : {
					'content' : inputValue
				},
				dataType : 'json',
				beforeSend: function(){
					$('button.confirm').prop('disabled' , true);
				},
				complete: function(){
					$('button.confirm').prop('disabled' , false);
				},
				success : function(response){
					swal('Good Job' , 'Message was delivered' , 'success');
				},
				error : function(response){
					swal('Something went wrong , try again');
				}
			});

			
		});
	});

})();