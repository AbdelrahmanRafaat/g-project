let acceptOfferHandler = (function(){
    let acceptOfferButtons = $('.acceptOffer');
    
    acceptOfferButtons.on('click' , function(event){
        event.preventDefault();
        let taskerId = $(this).attr('data-taskerId');
    
        $.ajax({
            method: 'POST',
            url  : window.location.pathname,
            dataType : 'json',
            data : {
                '_method'   : 'PUT',
                'status'    : 2, 
                'tasker_id' : taskerId
            },
            beforeSend: function(){
                acceptOfferButtons.prop('disabled' , true);                
            },
            complete: function(){
                acceptOfferButtons.prop('disabled' , false);                
            },
            success : function(response){
                swal('Good Job' , response.data.tasker_name + ' was assigned to your task.' , 'success');
                acceptOfferButtons.remove();
                $('span.status').html('Assigned to <a href="'+response.data.profile_url+'">'+response.data.tasker_name+'</a>');
            },
            error : function(response){
                swal('Something went wrong , try again');
            }
        });
    });

})();

let writeReviewHandler = (function(){
    let reviewSubmitButton = $('button.reviewSubmitButton');
    let reviewModalTrigger = $('.reviewModalTrigger');

    reviewSubmitButton.on('click' , function(event){
        event.preventDefault();
        let writeReviewModal = $('#reviewModal'); 
        
        let rating = writeReviewModal.find('[name="rating"]').val();
        let review = writeReviewModal.find('[name="review"]').val();

        if(review == ''){
            swal('Review is required.');
            return;
        }


        $.ajax({
            method: 'POST',
            url : '/tasks/'+windowLocation().getResourceId('tasks')+'/reviews/',
            data : {
                rating : rating,
                review : review
            },
            dataType : 'json',
            beforeSend: function(){
                reviewSubmitButton.prop('disabled' , true);
            },
            complete: function(){
                reviewSubmitButton.prop('disabled' , false);
            },
            success : function(response){
                writeReviewModal.modal('hide');
                reviewModalTrigger.remove();
                swal('Good Job' , 'Review was submited successfuly.' , 'success');
            },
            error : function(response){
                swal('Something went wrong , try again');
            }
        });
    });

})();

let makeOfferHandler = (function(){
    let makeOfferSubmitButton = $('.offerSubmitButton');
    let makeOfferModalTrigger = $('.offerModalTrigger');

     makeOfferSubmitButton.on('click' , function(event){
        event.preventDefault();

        let makeOfferModal = $('#offerModal');
        let price = makeOfferModal.find('input[name="price"]').val();
        let description = makeOfferModal.find('[name="description"]').val(); 
        
        if(price == '' || description == ''){
            swal('Price and description are required to make the offer.');
            return;
        }else if(isNaN(price)){
            swal('Offer price must be a number.');
            return;
        }

        $.ajax({
            method : 'POST',
            url    : '/tasks/'+windowLocation().getResourceId('tasks')+'/offers/',
            data   : {
                price : price,
                description : description
            },
            dataType : 'json',
            beforeSend: function(){
                makeOfferSubmitButton.prop('disabled' , true);
            },
            complete: function(){
                makeOfferSubmitButton.prop('disabled' , false);
            },
            success : function(response){
                makeOfferModalTrigger.remove();
                makeOfferModal.modal('hide');
                //add offer
                swal('Good Job' , 'offer was sent to the client.' , 'success');
            },
            error : function(response){
                swal('Something went wrong , try again');
            }
        });

    });
})();

let markCompletedHandler = (function(){
    let markCompletedButton = $('.markCompleted');
    let buttonContainerDiv  = markCompletedButton.parent(); 
    let updateTaskUrl = window.location.pathname;

    markCompletedButton.on('click' , function(){
        $.ajax({
            method: 'POST',
            url : updateTaskUrl,
            data : {
                '_method' : 'PUT',
                'status'  : 3
            },
            dataType : 'json',
            beforeSend: function(){
                markCompletedButton.prop('disabled' , true);
            },
            complete: function(){
                markCompletedButton.prop('disabled' , false);
            },
            success : function(response){
                $('.status').html('Completed..');
                buttonContainerDiv.remove();
                swal('Good Job', 'Client will review your work.' , 'success');
            },
            error : function(response){
                swal('Something went wrong , try again');
            }
        });
    });

})();