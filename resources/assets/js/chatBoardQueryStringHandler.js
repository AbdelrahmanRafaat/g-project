let chatBoardQueryStringHandler = function(){
	
	function getQueryString(){
		return window.location.search;
	}

	function checkQueryString(queryString){
		let regex = /\?show=[0-9]+/ig;
		return (queryString.match(regex)) ? true : false;
	}
	
	function getChatBoxNumber(queryString){
		return queryString.split('=')[1];
	}

	function chatBoxNumberExist(chatBoxNumber){
		return (publicMessaging.friendsIds.indexOf(chatBoxNumber) == -1) ? false : true ;
	}

	function handle(){
		let queryString = getQueryString();
		let chatBoxNumber = getChatBoxNumber(queryString);
		if( checkQueryString(queryString) && chatBoxNumberExist(chatBoxNumber) ){
			publicMessaging.showChatBox(chatBoxNumber);
		}
	}

	//Exsposed API
	return {
		handle : handle
	}; 
}