    <!--Specializations/Categories Div--> 
    <div class= "containerSec col-xs-10">


        <div class="containerCard">
            <!--Loop Through the specialization array and display rows , it should be cached-->
            <div class="row">
				<!--Each Row Contain 3 -->          	
            	<div class="col-md-4">
                    <a href="#">
                        <div class="card card-img card-orange">
                            <div class="icon">
                                <img src="{{ asset('images/home-1.jpg') }}" class="img-card">
                            </div>
                            <div class="text">
                                <h4 class="h4">Shopping</h4>
                                <p class="p">Get any thing from geroceries.</p>
                            </div>
                        </div>
                    </a>
                </div>
             <!--row ending-->
            </div>
            <!--containerCard ending-->
        </div>

        <!--Show More Categories(if there is more categories to be loaded)-->
        <div>
	        <button class="btn btn-primary" id="show-specialiaztion">
	        	Show more
	        </button>
        </div>

        <!--Suggest New Specialization , should be moved up -->
        <div>
        	<!--When this Button Gets clicked a modal should be viewed-->
	        <button class="btn btn-primary pull-right" id="more_specialize" data-toggle="modal"  data-target="#myModal" >
				Suggest New Specialization
	        </button>
	    </div>

	    <!--Suggest New Category Modal-->
	    <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
               	<div class="modal-content">


                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                        	&times;
                        </button>
                        <h3 class="modal-title">
                        	Create New Task
                        </h3>
                    </div>

                   <!-- Modal body -->
                   <div class="modal-body">
                       <form class="form-horizontal" role="form">

                           <div class="form-group">
                                <label  class="col-sm-2 control-label">
                                	Name:
                                </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="title" placeholder="Specialization Name.."/>
                                </div>
                            </div>
						  
                        	<div class="form-group">
                                <label class="col-sm-2 control-label">
                                	Description:
                                </label>
                                <div class="col-sm-10">
	                                <textarea class="form-control" rows="4" id="desc" placeholder="Description..">
	                                </textarea>
                                </div>
                            </div>

						  
                            <div class="form-group">
                                <label class="col-sm-2 control-label">
                                	Skills:
                                </label>
                                <div class="col-sm-10">
                                  <input type="text" class="form-control" id="desc" placeholder="Require Skills.."/>
                                </div>
                            </div>

                        <!--Modal Form ending-->
                        </form>
                   <!-- Modal body ending-->
                   </div>

                    <!-- Modal Footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                        	Close
                        </button>
                        <button class="btn btn-primary" data-dismiss="modal" id="">
                        	Save changes
                        </button>
                    <!--Modal Footer ending-->
                    </div>
                </div>
            </div>
        <!--Suggest New Category Modal-->    
        </div>
	<!--containerSec ending-->
</div>