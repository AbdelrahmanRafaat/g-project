@extends('app')

@section('header')
	<link rel="stylesheet" type="text/css" href="css/home.css">
@endsection

@section('content')
	@include('partials/authNavBar')
	<div class="containerMain ">
		@include('/home/headerSection')
		@include('/home/specilizations')
	</div>
@endsection

@section('footer')
	<script type="text/javascript" src="js/home.js"></script>
@endsection