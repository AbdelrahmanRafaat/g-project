<div class="taskerLocations" data-navigationNumber="3">
	<!--Additional Locations-->
	<div>
		<div class="row">
			<div class="col-sm-offset-3 col-sm-8">
				<h5 class="bolder">
					Locations you will be available to work in ?
				</h5>
			</div>
		</div>
		<br><br>
	</div>

	<div class="taskerLocation" data-locationNumber="0">

		<div class="row">
	    	<div class="col-xs-8 col-xs-offset-2">
	    		<div class="form-group">
			        <label for="firstLocation" class="bolder">First Location :</label>
			        <input class="form-control addressInput" name="firstLocation" placeholder="Location ..." type=
			        "text" disabled>
	            </div>
	    	</div>
	    </div>
	    
	    <div class="firstLocationSelection">
	        <div class="row">
	            <div class="col-sm-12">

	                <!--countrySelectionBox-->
	                <div class="col-sm-4">
	                    <div class="form-group">
	                        <select name="country" class="countrySelectionBox form-control">
	                            <option value="nothing">Country</option>
	                        </select>
	                    </div>
	                </div>

	                <!--stateSelectionBox-->
	                <div class="col-sm-4">
	                    <div class="form-group">
	                        <select name="state" class="stateSelectionBox form-control">
	                            <option value="nothing">State</option>
	                        </select>
	                    </div>
	                </div>


	                <!--citySelectionBox-->
	                <div class="col-sm-4">
	                    <div class="form-group">
	                        <select name="city" class="citySelectionBox form-control">
	                            <option value="nothing">City</option>
	                        </select>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>

	     <div class="row">
			<div class="col-xs-1 col-xs-offset-5">
	    		<div class="removeLocation">
					<div class="removeLocationButton">
						<span>x</span>
					</div>
				</div>
			</div>
		</div>

	</div>

	<hr>

	<div class="taskerLocation" data-locationNumber="1">
		<div class="row">
	    	<div class="col-xs-8 col-xs-offset-2">
	    		<div class="form-group">
			        <label for="secoundLocation" class="bolder">Secound Location :</label>
			        <input class="form-control addressInput" name="secoundLocation" placeholder="Location ..." type=
			        "text" disabled>
	            </div>
	    	</div>
	    </div>
	    
	    <div class="secoundLocationSelection">
	        <div class="row">
	            <div class="col-sm-12">

	                <!--countrySelectionBox-->
	                <div class="col-sm-4">
	                    <div class="form-group">
	                        <select name="country" class="countrySelectionBox form-control">
	                            <option value="nothing">Country</option>
	                        </select>
	                    </div>
	                </div>

	                <!--stateSelectionBox-->
	                <div class="col-sm-4">
	                    <div class="form-group">
	                        <select name="state" class="stateSelectionBox form-control">
	                            <option value="nothing">State</option>
	                        </select>
	                    </div>
	                </div>


	                <!--citySelectionBox-->
	                <div class="col-sm-4">
	                    <div class="form-group">
	                        <select name="city" class="citySelectionBox form-control">
	                            <option value="nothing">City</option>
	                        </select>
	                    </div>
	                </div>

	            </div>
	        </div>
	    </div>

		<div class="row">
			<div class="col-xs-1 col-xs-offset-5">
	    		<div class="removeLocation">
					<div class="removeLocationButton">
						<span>x</span>
					</div>
				</div>
			</div>
		</div>

	    <hr>
	</div>


	<div class="taskerLocation" data-locationNumber="2">
		<div class="row">
	    	<div class="col-xs-8 col-xs-offset-2">
	    		<div class="form-group">
			        <label for="thirdLocation" class="bolder">Third Location :</label>
			        <input class="form-control addressInput" name="thirdLocation" placeholder="Location ..." type=
			        "text" disabled>
	            </div>
	    	</div>
	    </div>
	    
	    <div class="thirdLocationSelection">
	        <div class="row">
	            <div class="col-sm-12">

	                <!--countrySelectionBox-->
	                <div class="col-sm-4">
	                    <div class="form-group">
	                        <select name="country" class="countrySelectionBox form-control">
	                            <option value="nothing">Country</option>
	                        </select>
	                    </div>
	                </div>

	                <!--stateSelectionBox-->
	                <div class="col-sm-4">
	                    <div class="form-group">
	                        <select name="state" class="stateSelectionBox form-control">
	                            <option value="nothing">State</option>
	                        </select>
	                    </div>
	                </div>


	                <!--citySelectionBox-->
	                <div class="col-sm-4">
	                    <div class="form-group">
	                        <select name="city" class="citySelectionBox form-control">
	                            <option value="nothing">City</option>
	                        </select>
	                    </div>
	                </div>

	            </div>
	        </div>
	    </div>

	   <div class="row">
			<div class="col-xs-1 col-xs-offset-5">
	    		<div class="removeLocation">
					<div class="removeLocationButton">
						<span>x</span>
					</div>
				</div>
			</div>
		</div>

	</div>


	<br><br>
	<div class="addLocationButton">
		<div class="row">
			<div class="col-xs-offset-5">
				<input type="button" class="btn btn-warning btn-round btn-sm" value="Add A Location"/>
			</div>
		</div>
	</div>
	<br><br>
</div>