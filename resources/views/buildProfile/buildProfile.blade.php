@extends('app')

@section('header')
	<link rel="stylesheet" type="text/css" href="/css/buildProfile.css">
@endsection

@section('content')
	@include('/partials/authNavBar')
	@include('/buildProfile/container')
		@include('/buildProfile/taskerOrClient')
		@include('/buildProfile/userEmailAddressBio')
		@include('/buildProfile/taskerInfo')
		@include('/buildProfile/taskerLocations')
	@include('/buildProfile/containerEnding')
@endsection

@section('footer')
	<script type="text/javascript" src="/js/buildProfile.js"></script>
@endsection