<div class="userEmailAddressBio" data-navigationNumber="1">
	
    <div class="profilePictureUploader">
		 <div class="choice">
            <div class="defaultProfilePicture">
                <a data-placement="top" data-toggle="tooltip" title="Change your default profile picture.">
                    <div class="icon">
                    	<i class="icon-user defaultProfilePictureIcon"></i>
                    </div>
                </a>
                <div class="bolder">Your Picture</div>
            </div>    
            <input type="hidden" value="" name="profilePicture"/>
            <div class="row dropzoneUploader">
                <div class="col-xs-5 col-xs-offset-4">
                    <div class="form-group">
                        <div class="dropzone dz-clickable" id="profilePictureUploader">
                            <div class="dz-message">
                                upload your picture
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
	</div>

    {!! csrf_field() !!}
    
    
	<hr>
    @if(!isset(Auth::user()->email))
        <div class="row">
        	<div class="col-xs-8 col-xs-offset-2">
        		<div class="form-group">
    		        <label for="email" class="bolder">Email :</label>
    		        <input class="form-control" name="email" placeholder="Email Address"
    		        type="email">
    		    </div>
        	</div>
        </div>
    @endif

    <br>

    <div class="row">
    	<div class="col-xs-8 col-xs-offset-2">
    		<div class="form-group">
                <label for="username" class="bolder">User name :</label>
		        <input class="form-control" name="username" placeholder="User Name"
		        type="text">
		    </div>
    	</div>
    </div>

    <br>

    <div class="row">
        <div class="col-sm-12">
            <div class="col-xs-8 col-xs-offset-2">
                <div class="form-group">
                    <label for="gender" class="bolder">Gender :</label>
                    <select multiple="multiple" name="gender" class="genderSelectionBox form-control" style="width: 100%;">
                        <option value="1"> Male  </option>
                        <option value="2"> Female </option>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <br>

    <div class="row">
    	<div class="col-xs-8 col-xs-offset-2">
    		<div class="form-group">
		        <label for="address" class="bolder">Address :</label>
		        <input class="form-control addressInput" name="address" placeholder="Where do you live ?" type=
		        "text" disabled>
            </div>
    	</div>
    </div>
    
    <div class="addressSelection">
        <div class="row">
            <div class="col-sm-12">

                <!--countrySelectionBox-->
                <div class="col-sm-4">
                    <div class="form-group">
                        <select name="country" class="countrySelectionBox form-control">
                            <option value="nothing">Country</option>
                        </select>
                    </div>
                </div>

                <!--stateSelectionBox-->
                <div class="col-sm-4">
                    <div class="form-group">
                        <select name="state" class="stateSelectionBox form-control">
                            <option value="nothing">State</option>
                        </select>
                    </div>
                </div>


                <!--citySelectionBox-->
                <div class="col-sm-4">
                    <div class="form-group">
                        <select name="city" class="citySelectionBox form-control">
                            <option value="nothing">City</option>
                        </select>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <br>

    <div class="row">
    	<div class="col-xs-8 col-xs-offset-2">
    		<div class="form-group">
		        <label for="bio" class="bolder">Bio :</label> 
		        <!--Text Area-->
		         <textarea class="form-control" rows="5" name="bio" placeholder=
		        "Small Description about yourself:"></textarea>
		    </div>
    	</div>
    </div>

</div>