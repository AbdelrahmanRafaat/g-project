<div class="taskerOrClient" data-navigationNumber="0">
    <h4 class="info-text">What do you want to be ?</h4>
    <div class="row">
        <div class="col-sm-12">

            <div class="col-sm-4 col-sm-offset-2">
                <div class="choice" data-accountType="client">
                    <a data-placement="top" data-toggle="tooltip" title="you want help with some task.">
                        <input name="type" type="radio" value="client" >
                        <div class="icon">
                            <img class="img-responsive img-circle chooseTypeImage"  src="{{ asset('images/HireMe.jpg') }}" />
                        </div>
                    </a>
                    <h6>Client</h6>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="choice" data-accountType="tasker">
                    <a data-placement="top" data-toggle="tooltip" title="you have a service to offer.">
                        <input name="type" type="radio" value="tasker">
                        <div class="icon">
                            <img class="img-responsive img-circle chooseTypeImage" src="{{ asset('images/6421440_orig.jpeg') }}" />
                        </div>
                    </a>
                    <h6>Tasker</h6>
                </div>
            </div>
        </div>
    </div>
</div>