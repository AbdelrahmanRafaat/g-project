<div class="taskerInfo" data-navigationNumber="2">
	<!--Drop Down List of specilizations , skills and salary , rearrange them depending on the specilizations if any is set-->
	<div class="row">
		<div class="col-sm-offset-4">
			<div class="pickingSpecilizationsHeader">
				What services can you offer ?
			</div>
			<!-- Maximum 3-->
		</div>
	</div>

	@for($i = 0; $i < count($specializations) ; $i++)
		@if( $i % 3 == 0 )
			<div class="row">
				<div class="col-sm-12">
		@endif
					<div class="col-sm-4">
						<div class="choice">
		                    <a data-placement="top" data-toggle="tooltip" title="you want help with {{ $specializations[$i]->name }} task.">
		                        <input name="specilizations" type="checkbox" value="{{ $specializations[$i]->id }}" >
		                        <div class="icon">
		                            <img class="img-responsive img-circle chooseTypeImage"  src="{{ asset('images/specializations/'.$specializations[$i]->image_path) }}" />
		                        </div>
		                    </a>
		                    <h6>{{ $specializations[$i]->name }}</h6>
		                </div>
					</div>
		@if( ($i+1) % 3 == 0 || $i == count($specializations)-1 )
				</div>
			</div>
		@endif
	@endfor

	<br>
	
	<hr>

	<!--Skills-->
	<div class="row">
		<div class="col-sm-offset-4">
			<div class="pickingSpecilizationsHeader">
				What skills do you have ?
			</div>
		</div>
	</div>

	<br><br>
	
	<div class="row">
		<div class="col-sm-12">
			<div class="col-xs-9 col-xs-offset-2">
				<div class="form-group">
					<select name="skills" class="skillsSelectionBox form-control" multiple="multiple" style="width: 100%;">
						@foreach($skills as $skill)
							<option value="{{$skill->id}}">{{$skill->name}}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
	</div>
	<hr>

	<div class="row">
		<div class="col-sm-offset-4">
			<div class="pickingSpecilizationsHeader">
				Expected salary per hour ?
				<div class="col-sm-offset-2">
					($/h)
				</div>
			</div>
		</div>
	</div>
	<br><br>

	<!--Salary-->
	<div class="row">
		<div class="col-sm-12">
			<div class="col-xs-9 col-xs-offset-2">
				<div class="form-group">
					<select multiple="multiple" name="salary" class="salarySelectionBox form-control" style="width: 100%;">
						<option value="1"> 1  : 5  </option>
						<option value="2"> 5  : 10 </option>
						<option value="3"> 10 : 30 </option>
						<option value="4">Above 30</option>
					</select>
				</div>
			</div>
		</div>
	</div>

</div>
<br>