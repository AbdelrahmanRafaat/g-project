<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Task Helper</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link rel="stylesheet" type="text/css" href="{{ asset('css/fonts.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset("fontawesome/css/font-awesome.css") }}">
	<link rel="stylesheet" type="text/css" href="{{ asset("fontawesome/css/font-awesome.min.css") }}">
	<link rel="stylesheet" type="text/css" href="{{ asset("assetsFonts/css/pe-icon-7-stroke.css") }}">
	
	@yield('header')
</head>
<body>

@yield('content')
@yield('footer')

@if(\Auth::check())
	@include('/sockets/socketPayloadMapper')
	@include('/sockets/generalSockets')
	@include('/sockets/navBarMessagesSocket')
	@include('/sockets/navBarNotificationsSocket')
@endif

@yield('eventBroadCasting')

@include('partials/flashMessaging')

</body>
</html>