            <footer class="footer">
                <div class="container">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="#">
                                Home
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Company
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="social-area pull-right">                
                        <a class="btn btn-social btn-facebook btn-simple">
                            <i class=" icon-facebook-rect"></i>
                        </a>
                        <a class="btn btn-social btn-twitter btn-simple">
                            <i class="icon-twitter"></i>
                        </a>
                    </div>
                    <div class="copyright">
                        &copy; 2016 <a href="#">TaskHelper Team</a>, made with love
                    </div>
                </div>
            </footer>