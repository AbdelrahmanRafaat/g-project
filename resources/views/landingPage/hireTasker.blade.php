            <div class="section section-presentation">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="description">
                                <h4 class="header-text orangeText">Hire a Tasker</h4>
                                <p>Hire a specific tasker in a specific specializtion while posting your task. <br> <a href="#specializations"><span class="btn btn-warning btn-fill">See All Specilizations</span></a></p>
                                <p>Or Just Post your task and all tasker in your location will be notified and apply for the task.<br>
                                Negoitiatie with taskers in many ways and assign who fits for the task.<br>
                                When the task is done you can rate the tasker , and write a review for his work.</p>
                                <!-- <span class="btn btn-warning btn-fill">Post Your Task.</span> -->
                            </div>
                        </div>
                        <!--How It works -->
                        <div class="col-md-5 col-md-offset-1 hidden-sm hidden-md hidden-xs">
                            <img src="{{ asset('images/hire1.jpg') }}" class="img responsive " />
                        </div>
                    </div>
                </div>
            </div>