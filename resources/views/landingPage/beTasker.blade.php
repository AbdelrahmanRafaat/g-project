            <div class="section section-demo">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="description-carousel" class="carousel fade " data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item">
                                        <img src="{{ asset('images/clean_small.jpg') }}"  alt="Clean">
                                    </div>
                                    <div class="item active">
                                        <img src="{{ asset('images/work.jpg') }}"  alt="Work">
                                    </div>
                                    <div class="item">
                                        <img src="{{ asset('images/design.jpg') }}"  alt="Design">
                                    </div>
                                </div>
                                <ol class="carousel-indicators carousel-indicators-orange">
                                    <li data-target="#description-carousel" data-slide-to="0" class=""></li>
                                    <li data-target="#description-carousel" data-slide-to="1" class="active"></li>
                                    <li data-target="#description-carousel" data-slide-to="2" class=""></li>
                                </ol>
                            </div>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            <h4 class="header-text orangeText">Be a Tasker</h4>
                            <p>
                                <div class="stepsNumberingDiv"><span class="stepsNumbering"> 1 </span> Sign up as a tasker. <br></div>
                                <div class="stepsNumberingDiv"><span class="stepsNumbering"> 2 </span> Confirm your registeration. <br></div>
                                <div class="stepsNumberingDiv"><span class="stepsNumbering"> 3 </span> Select One or more Specilization.<br></div>
                                <div class="stepsNumberingDiv"><span class="stepsNumbering"> 4 </span> Update Profile with needed information. <br></div>
                                <div class="stepsNumberingDiv"><span class="stepsNumbering"> 5 </span> get Hired when a client Hires you or notified when a task in your location is posted. <br></div>
                            </p>
                            <a href="{{url('/auth/register')}}"><span class="btn btn-warning btn-fill" data-button="info">Be a tasker</span></a>
                        </div>
                    </div>
                </div>
            </div>