@extends('app')

@section('header')
	<link rel="stylesheet" type="text/css" href="{{asset("/css/landingPage.css")}}">
    
@endsection

@section('content')
    <div class="landing-page landing-page1">
    	@include('/landingPage/navbar')
    	@include('/landingPage/headerJumbotron')
    	@include('/landingPage/simpleWords')
    	@include('/landingPage/hireTasker')
    	@include('/landingPage/beTasker')
    	@include('/landingPage/specializations')
    	@include('/landingPage/features')
    	@include('/landingPage/people')
    	@include('/landingPage/searchEnding')
    	@include('/landingPage/footer')
	</div>
@endsection

@section('footer')
	<script type="text/javascript" src="{{asset("/js/landingPage.js")}}"></script>
@endsection