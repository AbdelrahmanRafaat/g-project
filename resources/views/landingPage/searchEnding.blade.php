<div class="section section-no-padding">
    <div class="parallax filter-gradient orange" data-color="orange">
        <div class="parallax-background">
            <img class ="parallax-background-image" src="{{ asset('images/bg3.jpg') }}"/>
        </div>
        <div class="info">
            <h1>Where to find taskers or tasks?</h1>
            <p>You can serach and filter taskers or tasks with many options.</p>
            <a href="{{url('/auth/register')}}" class="btn btn-neutral btn-lg btn-fill">Taskers Search.</a>
        </div>
    </div>
</div>