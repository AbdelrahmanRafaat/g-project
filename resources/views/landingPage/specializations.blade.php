<div class="section section-jobs section-features" id="specializations">
    <div class="container">
        <h4 class="header-text text-center">Hire for a Range of Needs Around Your Home</h4>

        <div class="row">
        
        @foreach($allSpecializations as $spe)

            <div class="col-md-4">
                <a href="{{ url('auth/register') }}">
                    <div class="card card-img card-orange">
                        <div class="icon">
                            <img src="{{ asset('images/specializations/'.$spe->image_path) }}">
                        </div>
                        <div class="text">
                            <h4>{{$spe->name}}</h4>
                            <p>{{$spe->description}}</p>
                        </div>
                    </div>
                </a>
            </div>

          @endforeach
        
        
        
           
        </div>

      
    </div>
</div>