        <nav class="navbar navbar-transparent navbar-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button id="menu-toggle" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                    </button>
                    <a href="">
                        <div class="logo-container">
                            <div class="logo">
                                <img src="{{ asset('images/logo.png') }}" alt="Task Helper Logo">
                            </div>
                            <div class="brand">
                                Task Helper
                            </div>
                        </div>
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="example" >
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#howItWorks">
                                How it works 
                            <i class="icon-help-circled"></i>
                            </a>
                        </li>

                        <li>
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        </li>

                        <li>
                            <a href="{{url('/auth/register')}}" id="registerModal" > 
                                <i class="icon-user"></i> Register
                            </a>
                        </li>

                        <li>
                            <a href="{{url('/auth/login')}}" id="loginModal" >
                                <i class="icon-login"></i> Login
                            </a>
                        </li>

                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
        </nav>