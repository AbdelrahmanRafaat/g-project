        <div class="wrapper">
            <div class="parallax filter-gradient orange" data-color="orange">
                <div class="parallax-background">
                    <img class="parallax-background-image" src="{{ asset('images/Plumber_0.jpg') }}">
                </div>
                <div class= "container">
                    <div class="row">
                        <!-- Header Image -->
               
                            </div>
                        <!--Site Description-->
                        <div class="col-md-10 col-md-offset-1">
                            <div class="description">
                                <h2>&nbsp;</h2>
                                <br>
                                <h5>
                                    You have a task that needs to be done , but you don`t have the time.<br><br>
                                    You are Good at something and want to make some extra money.<br><br>
                                    Task Helper is the best place to<a href="{{url('/auth/register')}}"> <span class="btn btn-warning btn-fill getStarted">get started </span> </a>
                                </h5>
                            </div>
                            <!-- In Case , We Have Made Mobile App For the site. -->
                         <!--    <div class="buttons">
                                <button class="btn btn-fill btn-neutral">
                                <i class="icon-apple"></i> Appstore
                                </button>
                                <button class="btn btn-simple btn-neutral">
                                <i class="icon-android"></i>
                                </button>
                                <button class="btn btn-simple btn-neutral">
                                <i class="icon-windows"></i>
                                </button>
                            </div -->>
                        </div>
                    </div>
                </div>
            </div>