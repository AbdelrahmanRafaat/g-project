            <div class="section section-features">
                <div class="container">
                    <h4 class="header-text text-center">Our Unique Features ..</h4>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card card-orange">
                                <div class="icon">
                                    <i class=" icon-clock"></i>
                                </div>
                                <div class="text">
                                    <h4>Saves You Time</h4>
                                    <p>TaskHelper helps you live smarter, giving you time to focus on what’s most important.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-orange">
                                <div class="icon">
                                    <i class="icon-bell"></i>
                                </div>
                                <h4>Real-time Notifications.</h4>
                                <p>
                                    Taskers will be notified if clients post a task which fit him , or when he is assigned to a task.<br>
                                    Client will be notified if a tasker apply for his job.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-orange">
                                <div class="icon">
                                    <i class="icon-thumbs-up-alt"></i>
                                </div>
                                <h4>Easy to Get Help</h4>
                                <p>Select the task you need done, then choose the Tasker you’d like to work with.</p>
                            </div>
                        </div>
                    </div>

                    <!--Secound Features Row-->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card card-orange">
                                <div class="icon">
                                    <i class="icon-chat-empty"></i>
                                </div>
                                <div class="text">
                                    <h4>Seamless Communication</h4>
                                    <p>Online communication makes it easy for you to stay in touch with your Tasker or the client.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-orange">
                                <div class="icon">
                                    <i class="icon-dollar"></i>
                                </div>
                                <h4>Two Payment Methods.</h4>
                                <p>
                                    You can use our secure online payment.<br>
                                    Or just old fashion hand to hand payment.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-orange">
                                <div class="icon">
                                    <i class="icon-chart"></i>
                                </div>
                                <h4>Manage your information and get verification.</h4>
                                <p>Verified taskers are requested than other taskers.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>