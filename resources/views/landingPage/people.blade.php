<div class="section section-testimonial">
    <div class="container">
        <h4 class="header-text text-center">What people think</h4>
        <div id="carousel-example-generic" class="carousel fade" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item">
                    <div class="mask">
                        <img src="{{ asset('images/faces/face-4.jpg') }}">
                    </div>
                    <div class="carousel-testimonial-caption">
                        <p>Ahmed Alaa</p>
                        <h3>"It really Helped Me , and saved me alot of time!"</h3>
                    </div>
                </div>
                <div class="item active">
                    <div class="mask">
                        <img src="{{ asset('images/faces/face-3.jpg') }}">
                    </div>
                    <div class="carousel-testimonial-caption">
                        <p>Abdel-rahman Ahmed</p>
                        <h3>"This is one of the most awesome apps I've ever seen! Wish you luck !"</h3>
                    </div>
                </div>
                <div class="item">
                    <div class="mask">
                        <img src="{{ asset('images/faces/face-3.jpg') }}">
                    </div>
                    <div class="carousel-testimonial-caption">
                        <p>Mohamed Salah</p>
                        <h3>"Loving this! Just picked it up the other day. Thank you for the work you put into this."</h3>
                    </div>
                </div>
            </div>
            <ol class="carousel-indicators carousel-indicators-orange">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>
        </div>
    </div>
</div>