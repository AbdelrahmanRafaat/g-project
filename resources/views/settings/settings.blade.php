@extends('app')

@section('header')
	<link rel="stylesheet" type="text/css" href="{{ asset("css/settings.css") }}">
@endsection

@section('content')
	@include('partials/authNavBar')
		
		<br><br>    
  		
  		<form action="{{ action('ProfilesController@update') }}" method="post" enctype="multipart/form-data" >
	  		<input name="_method" type="hidden" value="PUT">
	  		<input type="hidden" name="_token" value="{{ csrf_token() }}">
              
           <div class="form-group col-md-12 imag-profile">
                <!--Dropzone-->
            </div>
                   
            <div class="container col-md-12">

            	<div class="form-group col-md-5">
          			<label for="Profile-pic">
          				Profile Pictuer : <i class="fa fa-cloud-upload fa-4x" aria-hidden="true"></i>
          			</label>
          			<input type="file" name="profile_picture_path" class="form-control " id="Profile-pic">
				</div>
	         
             
            	<div class="form-group col-md-5">
            		<label for="Name">Name:</label>
	            	<input type="text"  id ="Name" name="name" class="form-control" value="{{ $UpdateUser['name']}}">
            	</div>

            	<div class="form-group col-md-5">
            		<label for="E-mail">E-mail:</label>
              		<input type="text" id="E-mail" name="email" class="form-control" value="{{ $UpdateUser['email']}}">
	          	</div>
              
            	<div class="form-group col-md-5">
             		<label for="P-assword ">Password :</label>
		  			<input type="password"  id="P-assword" name="password" class=" newpass form-control" placeholder="New password" >
          		</div>
          
          		<div class="form-group col-md-5">
             		<label for="R-password ">Confirm-Password :</label>
	      			<input type="password" id="R-password" name="confirm-password" class="form-control" placeholder="Confirm New password" >
				</div>
	         
                <div class="form-group col-md-5">
                    <label for="Gender">Gender:</label>
			    	@if($UpdateUser['gender'] == '1')
			       		<select name="gender" id="Gender" class="form-control">
			        		<option value="1">male</option>
			        		<option value="0">female</option>
			        	</select>
			        @else
			        	<select name="gender" class="form-control">
			        		<option value="0">female</option>
			        		<option value="1">male</option>
			        	</select>
			    	 @endif
				</div>
	    
				<div class="form-group col-md-5">
		            <label for="Phone">Phone:</label>
			      	<input type="text" name="phone"  id ="Phone" class="form-control" value="{{ $UpdateUser['phone']}}">
		        </div>
          
	           	<div class="form-group col-md-10">
					<br>
					<input type="submit" name="save-changes" class="btn btn-defualt save-btn " value="Save Changes">
	           		<input type="button" name="delete-account" class="btn btn-link delete-btn" value="Delete Account">
				</div>
		
				<div class="error-div">
					@include('errors/list')
				</div>
        
        	</div>
    	</form>
@endsection

@section('footer')
	<script type="text/javascript" src="{{ asset("js/settings.js") }}"></script>
		
@endsection
