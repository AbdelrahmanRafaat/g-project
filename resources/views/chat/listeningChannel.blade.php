<script type='text/javascript'>
	/**
	 * Self Executing Reveal Module that handles socket Messaging.
	 * @Author : Abdel-rahman Rafaat Ahmed
	 */
	var soketMessaging = (function(){
		var socket      = io('http://192.168.10.10:3000');
		var userChannel =  {{\Auth::user()->id}};

		/**
		 * Every User listen on his Channel ,
		 * when some data comes through this channel add it to the DOM.
		 */
		socket.on('user:'+userChannel , function(message){
			addBrodcastedMessage(message);
		});

		/**
		 * Handles the Process of adding the broadcasted message to the DOM.
		 * @param string message.
		 */
		function addBrodcastedMessage(message){
			var chatBox  = publicMessaging.findChatBox(message.from);
			publicMessaging.addMessageToChatBox(message.content , chatBox , 'right');
			if(chatBox.css('display') == 'none'){
				handleNotificationBox(message.from);
			}
		};

		/**
		 * IF the ChatBox that will receive the new message is not showed.
		 * we Increment the counter in the notificationsBox and show it up.
		 */
		function handleNotificationBox(boxNumber){
			friend = publicMessaging.findFriend(boxNumber);
			var notificationsBox = publicMessaging.findNotificationsBox(friend);
			publicMessaging.showNotificationsBox(notificationsBox);
			publicMessaging.incrementNotificationsBox(notificationsBox);
		};

	})();
</script>