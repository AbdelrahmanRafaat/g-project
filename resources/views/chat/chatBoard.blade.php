@extends('app')

@section('header')
	<link rel="stylesheet" type="text/css" href="/css/chatBoard.css">
@endsection

@section('content')
	@include('/partials/authNavBar')
	<div class="chatApp fullHeight">
		<div class="row fullHeight">
			@include('chat/friendsList')

			@include('chat/chatBoxesContainer')

			@include('chat/messageTemplate')
	</div>
		</div>
@endsection

@section('footer')
	<script type="text/javascript" src="/js/chatBoard.js"></script>
@endsection

@section('eventBroadCasting')
	@include('/sockets/chatBoardSocket')
@endsection