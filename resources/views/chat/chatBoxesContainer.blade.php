<div class="chatBoxesContainer">
	@foreach($conversations as $conversation)
		<div class="col-xs-9">
			<div class="privateChat" data-chatBoxNumber="{{ $conversation['conversationUsers']->first()->id }}" data-conversationNumber="{{$conversation['conversationId'] }}">
				<div class="chat_window">
					<div class="top_menu">
						<div class="buttons">
							<div class="button close"></div>
						</div>
						<div class="title">{{ $conversation['conversationUsers']->first()->name }}</div>
					</div>
					<ul class="messages" data-loading="0">
						@for($i = count($conversation['conversationMessages'])-1 ; $i >= 0 ; --$i)
							{{--*/ $side = "right" /*--}}
							{{--*/ $profilePicture = $conversation['conversationUsers']->first()->profile_picture_path /*--}}
							@if($conversation['conversationMessages'][$i]->user->id == \Auth::user()->id)
								{{--*/ $side = "left" /*--}}
								{{--*/ $profilePicture = \Auth::user()->profile_picture_path /*--}}
							@endif
							<li class="message appeared {{ $side }}">
								@if($i == count($conversation['conversationMessages'])-1)
									<input type="hidden" value="{{ $conversation['conversationMessages'][$i]->id }}" name="firstMessage" />
								@endif
								<div class="avatar">
									<img class="avatar" src="/images/users/profilePictures/{{ $profilePicture }}">
								</div>
								<div class="text_wrapper">
									<div class="text">
										{{ $conversation['conversationMessages'][$i]->content }}
									</div>
								</div>
							</li>
						@endfor
					</ul>
					<div class="bottom_wrapper clearfix">
						<div class="message_input_wrapper">
							<input class="message_input" placeholder="Type your message here..." />
						</div>
						<div class="send_message">
							<span class="text">Send</span>
							<span class="icon"></span>
						</div>
					</div>
				</div>
				{{ csrf_field() }}
			</div>
		</div>
	@endforeach
</div>