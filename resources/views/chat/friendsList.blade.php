<div class="col-xs-3 rightFloating fullHeight friendList">
	@include('chat/chatSearch')
	<div>
		@foreach($conversations as $conversation)
			<div class="friend" data-friendNumber="{{$conversation['conversationUsers']->first()->id}}">
				<span class="friendAvatar">
					<div class="notifications">
						<div class="new-message">
							<span class="counter">0</span>
						</div>
					</div>
					<img src="/images/users/profilePictures/{{$conversation['conversationUsers']->first()->profile_picture_path}}">
				</span>
				<span class="friendName">{{$conversation['conversationUsers']->first()->name}}</span>
			</div>
			<div class="chatFriendListSeparators">
				<br><br><br>
			</div>
		@endforeach
	</div>
</div>