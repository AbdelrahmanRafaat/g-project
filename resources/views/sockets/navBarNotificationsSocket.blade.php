<script type="text/javascript">
	var navBarNotificationsSocket = function(){
		var authNavBarNotificationsHandler = authNavBarNotifications(); 

		function addBroadcastedNotification(notification){
			authNavBarNotificationsHandler.incrementLabel();
			authNavBarNotificationsHandler.removeLastNotification();
			authNavBarNotificationsHandler.addNewNotification(notification.content);
		}
		
		//Exsposed API
		return {
			addBroadcastedNotification : addBroadcastedNotification
		}; 
	}
</script>