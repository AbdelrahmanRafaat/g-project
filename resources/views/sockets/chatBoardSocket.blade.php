<script type="text/javascript">
	var chatBoardSocket = function(){

		/**
		 * Handles the Process of adding the broadcasted message to the DOM.
		 * @param string message.
		 */
		function addBroadcastedMessage(message){
			var chatBox  = publicMessaging.findChatBox(message.from);
			publicMessaging.addMessageToChatBox(message.content , chatBox , 'right' , message.profilePicture);
			if(chatBox.css('display') == 'none'){
				handleNotificationBox(message.from);
			}
		}

		/**
		 * IF the ChatBox that will receive the new message is not showed.
		 * we Increment the counter in the notificationsBox and show it up.
		 */
		function handleNotificationBox(boxNumber){
			friend = publicMessaging.findFriend(boxNumber);
			var notificationsBox = publicMessaging.findNotificationsBox(friend);
			publicMessaging.showNotificationsBox(notificationsBox);
			publicMessaging.incrementNotificationsBox(notificationsBox);
		}

		//Exsposed API
		return {
			addBroadcastedMessage : addBroadcastedMessage
		};
	}
</script>