<script type="text/javascript">
	var  navBarMessagesSocket = function(){
		function addBroadcastedMessage(message){
			authNavBarMessages().addMessage(message);
		}
	
		//Exsposed API
		return {
			addBroadcastedMessage : addBroadcastedMessage 
		};
	}
</script>