<script src="/js/socket.io-1.4.5.js"></script>
<script type="text/javascript">
    var socket      = io('http://192.168.10.10:3000');
	var userChannel =  {{\Auth::user()->id}};

	/**
	 * Every User listen on his Channel ,
	 * when some data comes through this channel add it to the DOM.
	 */
	socket.on('user:'+userChannel , function(payload){
		payloadMapper().map(payload);
	});
</script>