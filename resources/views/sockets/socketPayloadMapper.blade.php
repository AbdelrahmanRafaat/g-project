<script type="text/javascript">
	var payloadMapper = function(){

		function mapToNavBarNotifications(notification){
			navBarNotificationsSocket().addBroadcastedNotification(notification);
		}

		function mapToNavBarMessages(message){
			navBarMessagesSocket().addBroadcastedMessage(message);
		}

		function mapToChatBoard(message){
			chatBoardSocket().addBroadcastedMessage(message);
		}
		
		function mapToNotificationsPage(notification){
			notificationsPageSocket().addBroadcastedNotification(notification);
		}

		function isChatBoardPage(){
			var currentPage    = window.location.pathname;
			var chatBoardRegex = /\/user\/[0-9]+\/messages/ig;
			return (currentPage.match(chatBoardRegex)) ? true: false;
		}

		function isNotificationsPage(){
			var currentPage    = window.location.pathname;
			var notificationsPageRegex = /\/user\/[0-9]+\/notifications/ig;
			return (currentPage.match(notificationsPageRegex)) ? true : false;
		}

		function map(payload){
			if(payload.dataType == 'message'){
				mapToNavBarMessages(payload);
				if(isChatBoardPage()){
					mapToChatBoard(payload);
				}
			}else if(payload.dataType == 'notification'){
				mapToNavBarNotifications(payload);
				if(isNotificationsPage()){
					mapToNotificationsPage(payload);
				}
			}
		}

		//Exsposed API
		return {
			map : map
		};
	}
</script>