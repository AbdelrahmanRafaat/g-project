    <div class="notifications">
        @foreach($notifications as $notification)
            <div class="row">
                <div class="col-xs-12">
                    <div class="notification">
                        <div class="row">
                            <div class="notificationHeader col-xs-offset-6">
                                <i class="icon-user"></i>
                            </div>
                            <br><br>
                        </div>
                        <div class="row">
                            <div class="notificationBody">
                                <?php
                                    /**
                                     * Move away to a seperate formater class
                                     */
                                    $content = '';
                                    if($notification->type == 1){                                
                                        $content = "You were assigned to <a href=\"/tasks/{$notification->task->id}/\">{$notification->task->title}</a>";
                                        if($notification->task->status == 1){
                                            $content .= "<div class=\"\">
                                                <button class=\"btn btn-success acceptAssignment\">Accept</button> <button class=\"btn btn-danger col-xs-offset-1 declineAssignment\">Decline</button>
                                            </div>";
                                        }
                                    }

                                    if($notification->type == 2){
                                       $content = "{$notification->task->tasker->user->name} Accepted your task ( <a href=\"/tasks/{$notification->task->id}/\">{$notification->task->title}</a> ) Assigment.";
                                       if($notification->task->payment_method == 2 && $notification->task->status == 2){
                                            $content .= "<div class=\"\">
                                                            <button class=\"btn btn-info col-xs-offset-1\" href=\"/tasks/{notification->task->id}/payments/\">Complete Payment Process</button>
                                                        </div>";
                                       }
                                    }

                                    if($notification->type == 3){
                                        $content  = "Since you accepted the tasker offer Complete the payment process.";
                                        if($notification->task->payment_method == 2 && $notification->task->status == 2){
                                            $content .= "<div class=\"\">
                                                        <button class=\"btn btn-info col-xs-offset-1\" href=\"/tasks/{notification->task->id}/payments/\">Complete Payment Process</button>
                                                    </div>";    
                                        }
                                    }

                                    if($notification->type == 4 && $notification->task->payment_method == 2 && $notification->task->status == 3){
                                        $content = "<a href=\"/tasks/{$notification->task->id}/\"> {$notification->task->title} </a> Payment was completed.";
                                    }

                                    if($notification->type == 5){
                                        $content = "Tasker refused your assigment , your task ( <a href=\"/tasks/{$notification->task->id}/\">{$notification->task->title}</a> ) will be moved to pending tasks page.";
                                    }

                                    if($notification->type == 6){
                                        $content = "You got a new offer for your task ( <a href=\"/tasks/{$notification->task->id}/\">{$notification->task->title}</a> ).";   
                                    }

                                    if($notification->type == 7){
                                        $content = "{$notification->task->user->name} accepted your offer on ( <a href=\"/tasks/{$notification->task->id}/\">{$notification->task->title}</a> )";
                                    }

                                    if($notification->type == 9){
                                        $content = "Tasker marked a task as completed";
                                        if( empty($notification->review) ){
                                            $content = "<div class=\"\"> <a href=\"/tasks/{$notification->task->id}/\">Write a review.</a></div>";
                                        }
                                    }

                                    if($notification->type == 10){
                                        $content = "{$notification->task->user->name} wrote a review for your work on ( <a href=\"/tasks/{$notification->task->id}/\">{$notification->task->title}</a> )";
                                    }
                                ?>

                                {!! $content !!}
                            </div>
                            <div class="notificationTime">
                                {{ $notification->created_at }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br><hr>
        @endforeach
    </div>
