@extends('app')

@section('header')
	<link rel="stylesheet" type="text/css" href="/css/notifications.css">
@endsection

@section('content')
	@include('/partials/authNavBar')
    <div class="col-xs-12" >
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2 whiteBackGround">
                <div class="col-xs-offset-5">
                    <h2 class="grayColor">Notifications</h2>
                </div>
                
                <hr>
			    @include('/notification/notificationBody')
			    @if($notifications->hasMorePages())
			    	@include('/notification/notificationsSeeMorePaginator')
				@endif
			</div>
		</div>
	</div>
@endsection

@section('footer')
	<script type="text/javascript" src="/js/notifications.js"></script>
@endsection

@section('eventBroadCasting')
	@include('/sockets/socketPayloadMapper')
	@include('/sockets/generalSockets')
	@include('/sockets/navBarNotificationsSocket')
	@include('/sockets/navBarMessagesSocket')
	@include('/sockets/notificationsPageSocket')
@endsection