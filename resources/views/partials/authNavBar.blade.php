<nav class=" navbar-ct-grey navbar navbar-inverse" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand  navbar-brand-logo" href="/">
                <div class="logo">
                    <img alt="Task Helper Logo" src="{{ asset('images/logo.png') }}">
                    </div>
                <div class="brand">
                    Task Helper
                </div>
            </a>
        </div>


        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right scrollable-menu scrollbar" role= "menu">
                <!--Notifications-->
                <li class="dropdown navBarNotifications">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <span class="label"></span>
                        <i class="pe-7s-bell">
                        </i>
                        <p>
                            Notifications
                        </p>
                    </a>
                    <ul class="dropdown-menu">
                       
                        <li class="divider"></li>
                        <li class="footer">
                            <a href="/user/{{\Auth::user()->id}}/notifications/">See all</a>
                        </li>
                    </ul>
                </li>
                <!--Messages-->
                <li class="dropdown navBarMessages">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="">
                        <span class="label"></span>
                        <i class="pe-7s-mail">
                        </i>
                        <p>
                            Message
                        </p>
                    </a>
                    <ul class="dropdown-menu">
                        
                        
                        <li class="divider"></li>
                        
                    </ul>
                </li>
                <li>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="">
                        <i class="fa fa-wrench navBarCustomIcon" style="font-size:180%;"></i>
                        <p>Tasks</p>
                    </a>
                    @if(Auth::user()->tasker !== NULL)
                        <!--tasker-->
                        <ul class="dropdown-menu" style="font-size:14px; font-family: 'Roboto';">
                            <li>
                                <a href="/user/{{Auth::user()->id}}/">View my tasks</a>
                            </li>
                            <li>
                                <a href="/user/{{Auth::user()->id}}/offers/">View my offers</a>
                            </li>
                            <li class="divider"></li>
                        </ul>
                    @else
                        <ul class="dropdown-menu" style="font-size:14px; font-family: 'Roboto';">
                            <li>
                                <a href="/user/{{Auth::user()->id}}/">View my tasks</a>
                            </li>
                            <li>
                                <a href="/user/{{Auth::user()->id}}/tasks/create/">Create a new task</a>
                            </li>
                            <li class="divider"></li>
                        </ul>
                    @endif

                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="">
                        <i class="pe-7s-user"></i>
                        <p>
                            <b class="caret"></b>
                        </p>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="/user/{{Auth::user()->id}}/">Profile</a>
                        </li>
                        <li>
                            <a href="/pending">Pending Page</a>
                        </li>
                        <li>
                            <a href="/user/{{Auth::user()->id}}/edit/">Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="/auth/logout/">Log out</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>