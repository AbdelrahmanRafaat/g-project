<nav class="navbar navbar-ct-grey navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="row">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand navbar-brand-logo">
                    <div class="logo"><img alt="Task Helper Logo" src=
                    "{{ asset('images/logo.png') }}"></div>
                    <div class="brand">
                        Task Helper
                    </div>
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right scrollable-menu scrollbar" role= "menu">
                   <li>
                        <a href="#">
                            <i class="icon-bell" style="font-size:180%;"></i>
                            <p>
                                How it works ? 
                            </p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="icon-bell" style="font-size:180%;"></i>
                            <p>
                                Login 
                            </p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>