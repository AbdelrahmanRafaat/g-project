@if( session()->has('flashMessage') )
	<script>
		swal({
			title : "{{ session('flashMessage.title') }}",
			text  : "{{ session('flashMessage.text') }}",
			type  : "{{ session('flashMessage.type') }}",
			timer : 2000,
			showConfirmButton : false
		});
	</script>
@endif

@if( session()->has('flashMessageOverlay') )
	<script>
		swal({
			title : "{{ session('flashMessageOverlay.title') }}",
			text  : "{{ session('flashMessageOverlay.text') }}",
			type  : "{{ session('flashMessageOverlay.type') }}",
			confirmButtonText : "{{ session('flashMessageOverlay.buttonText') }}"
		});
	</script>
@endif


@if( session()->has('flashMessageConfirmationOverlay') )
	<script>
		swal({
			title : "{{ session('flashMessageConfirmationOverlay.title') }}",
			text  : "{{ session('flashMessageConfirmationOverlay.text') }}",
			type  : "{{ session('flashMessageConfirmationOverlay.type') }}",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: false,
			confirmButtonText : "{{ session('flashMessageConfirmationOverlay.buttonText') }}"
		},function(){ //Triggered When Clicking on the Resend confirmation email
			document.location = "{{action('Auth\AuthController@getReconfirmRegister', session('flashMessageConfirmationOverlay.email'))}}";
		});
	</script>
@endif

@if( session()->has('flashMessageAside') )
	<script>
		$('div.container').append('<div class = "asideMessage">' +"{{ session('flashMessageAside.text') }}"+ '</div>');
		$('div.asideMessage').css('display' , 'block');
		function hideSideMessage(){
			$('div.asideMessage').css('display' , 'none');
		}
		setInterval( hideSideMessage , 3000);
	</script>
@endif