
<div class="hold-transition login-page ">



<div class="overlay">
  

<div class="login-box">
 
  <div class="login-box-body">
  <div class="login-logo">
    <a href="/landingPage"><b>Task</b>helper</a>
  </div><!-- /.login-logo --> 
    <p class="login-box-msg">Sign in to start your session</p>
    <form action="{{ action('Auth\AuthController@postLogin') }}" method="post">
    {!! csrf_field() !!}
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-sm-8">
          <div class="checkbox ">
            <label>
              <input type="checkbox" name="remember"> Remember Me
            </label>
          </div>
        </div><!-- /.col -->
        <div class="col-sm-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div><!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="{{ action('Auth\AuthController@getSocialLogin' , ['provider'=>'facebook']) }}" class="btn btn-block btn-social btn-facebook btn-flat">
      <i class="fa fa-facebook"></i> Sign in using Facebook
      </a>
      <a href="{{ action('Auth\AuthController@getSocialLogin' , ['provider'=>'google']) }}" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+
      </a>
  <a href="{{ action('Auth\AuthController@getSocialLogin' , ['provider'=>'twitter']) }}" class="btn btn-block btn-social btn-twitter btn-flat">
  <i class="fa fa-twitter"></i> Sign in using Twitter
  </a>

    </div><!-- /.social-auth-links -->

    <a href="#">I forgot my password</a><br>
    <a href="{{url('/auth/register')}}" class="text-center">Register a new membership</a>

  </div><!-- /.login-box-body -->
</div><!-- /.login-box -->
</div>

</div>

