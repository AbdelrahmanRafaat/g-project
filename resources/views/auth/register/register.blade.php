@extends('app')

@section('header')
		<link rel="stylesheet" type="text/css" href="{{asset("/css/login.css")}}">
@endsection

@section('content')
	@include('/errors/list')
	@include('auth/register/registerForm')
@endsection

@section('footer')
@endsection