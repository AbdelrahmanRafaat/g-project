<div class="hold-transition register-page ">
      
          <div class="overlay">

    <div class="register-box">
     

      <div class="register-box-body">
          
           <div class="register-logo">
        <a href="/landingPage"><b>Task</b>helper</a>
      </div>
          
        <p class="login-box-msg">Register a new membership</p>
        
        <form action="{{ action('Auth\AuthController@postRegister') }}" method="post">
        {!! csrf_field() !!}
          <div class="form-group has-feedback">
            <input type="text " name="name" class="form-control" placeholder="Full name">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="email" name="email" class="form-control" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <!-- <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Retype password">
            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
          </div> -->
          <div class="row">
            <div class="col-sm-8">
              <div class="checkbox">
                <label>
                  <input type="checkbox" > I agree to the <a href="#">terms</a>
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-sm-4">
              <button id=
                    "User_Register" 
                    class="btn btn-primary btn-block btn-flat">Register</button>
            </div><!-- /.col -->
          </div>
        </form>

        <div class="social-auth-links text-center">
          <p>- OR -</p>
          <a href="{{ action('Auth\AuthController@getSocialLogin' , ['provider'=>'facebook']) }}" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using Facebook</a>
          <a href="{{ action('Auth\AuthController@getSocialLogin' , ['provider'=>'google']) }}" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using Google+</a>
          <a href="{{ action('Auth\AuthController@getSocialLogin' , ['provider'=>'twitter']) }}" class="btn btn-block btn-social btn-twitter btn-flat"><i class="fa fa-twitter"></i> Sign in using Twitter</a>   </div>

        <a  href=
                    "{{ action('Auth\AuthController@getLogin') }}"class="text-center">I already have a membership</a>
      </div>
    </div>
      </div>

    <script>

    </script>
  </div>