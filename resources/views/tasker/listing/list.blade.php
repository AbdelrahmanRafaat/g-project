<!-- This Div caontain "taskers" template -->
<div class="container right-section containerMain" >
    <taskers></taskers>
</div>

<template id="taskersTemplate">
    
    <!--Taskers Filter-->
    <div class="form-group col-md-4">
        <label for="sel1">
            Specialization :
        </label>
        <select class="form-control" v-model="specialization" >
            <option value="" selected>All</option>
            <option v-for="specialization in specializations">@{{ specialization }}</option>
        </select>
    </div>  

    <div class="form-group col-md-4">
        <label for="sel1">
            Location :
        </label>
        <select class="form-control" id="select" v-model="location" >
            <option value="" selected>All</option>
            <option v-for="userLocation in locations " >@{{ userLocation }}</option>
        </select>
    </div>

    <div class=" form-group col-md-3" >
        <label class="text-center">Search For Any Tasker</label>
        <input type="text" name="searchForTasker" class="form-control" v-model='searchBox' placeholder="Search for any Tasker u want" />
    </div>
    
    <!--End Taskers Filter-->    

    <div class="media col-md-12" v-for="user in list | filterBy searchBox | filterBy location | filterBy specialization">
        <div class="media-left media-middle"></div>
        <div class="media-body">
            <div class="col-md-3 left ">
                 <img src="images/users/profilePictures/@{{user.user.profile_picture_path}}" style="max-width: 200px;">
            </div>  
            <div class="col-md-9 right"> 
                <div>
                    <h2 class="media-heading" style="padding: 1px;margin: 1px">
                        <a href="user/@{{ user.user.id }}">@{{ user.user.name }}</a>
                    </h2>

                    <h3>
                        <span class="salary" id="salary" >@{{ user.price }}</span>
                    </h3> <!-- Price -->
                    
                    <!-- Tasker Location -->
                    <i class="icon-location"></i>
                    <span v-for="location in user.locations">@{{ location.name }}</span>

                    <br>
                    <!-- Tasker Specialization -->
                    <i class="icon-thumbs-up-alt"></i>
                    <span v-for="specialization in user.specializations">@{{specialization.name}}</span>
                    <hr>
                </div>
                <div>
                    <span><b>How I Can help:</b></span>
                    <p>@{{ user.bio }}</p>
                    <a class="pull-right btn btn-primary assignBtn" href="user/@{{ user.user.id }}"> View Profile</a>
                </div>
            </div>
        </div>   
    </div>
</template>
