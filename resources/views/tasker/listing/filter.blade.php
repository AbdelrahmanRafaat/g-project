	<!--Taskers Filter-->
	<div class="col-md-3 left-section containerMain">
		<div class="form-group">
            <label for="sel1">
            	SORTED BY:
            </label>
            <select class="form-control" id="select">
                <option>Recomended</option>
                <option>Price(Lowest To Highest)</option>
                <option>Price(Highest To Lowest)</option>
                <option>Most Reviews</option>
                <option>Highest Rating</option>
            </select>
        </div>

        <div class="form-group">
          	<label for="sel1">
          		TASK DATE:
          	</label>
        	<input type="date" class="form-control">
        </div>

        <div class="form-group">
          	<label for="sel1">
          		TASK TIME:
          	</label>
          	<select class="form-control" id="select">
                <option>Any Time 8am - 8pm</option>
                <option>Morning 8am - 12pm</option>
                <option>Afternoon 12pm - 4pm</option>
                <option>Evening 4pm - 8pm</option>
        	</select>
        </div>
    <!--Taskers Filter ending-->
    </div>