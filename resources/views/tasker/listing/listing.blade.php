@extends('app')

@section('header')
	<link rel="stylesheet" type="text/css" href="css/taskersListing.css">
@endsection

@section('content')
	<div id="app">		
		@include('partials/authNavBar')
		@include('tasker/listing/list')
	</div>
@endsection

@section('footer')
	<script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.17/vue.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.0/vue-resource.min.js"></script>
	<script type="text/javascript" src="{{ asset('js/taskersListing.js') }}"></script>
@endsection

