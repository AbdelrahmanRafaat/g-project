<div id="about-tab" class="tabContent  tab-pane fade ">
    <div class="hello">
        <div class="col-xs-12">
            <div class="col-xs-5">
                <div class="form-group">
                    <label class="label-taskr">
                        Name:
                    </label>
                    <label>
                        <h5>
                            <p class="p-about name">{{ $user->name }}</p>
                        </h5>
                    </label>
                    <br>
                    <label  class="label-taskr" for="Bio">
                        Bio:
                    </label>
                    <h5>
                        <p class="p-about bio">
                            {{ $user->profile->bio }}
                        </p>
                    </h5>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>