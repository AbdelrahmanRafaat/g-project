@extends('app')

@section('header')
	<link rel="stylesheet" type="text/css" href="/css/taskerProfile.css">

@endsection

@section('content')
	@include('/partials/authNavBar')
	@include('/tasker/profile/headerSection')
	@include('/tasker/profile/tabHost')
@endsection

@section('footer')
	<script src="/js/taskerProfile.js"></script>
@endsection