<div class="containerMainn col-md-12">
    <div class="container col-md-12" id="about_tasker">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                            <a data-toggle="tab" href="#hire-me-tab" id="cont">About</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#review-tab" id="cont">Reviews</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tasks-tab" id="cont">Tasks</a>
                        </li>
                    </ul>

                    <div class="tab-content col-xs-offset-1">
                        @include('/tasker/profile/hireMeTab')
                        @include('/tasker/profile/reviewTab')
                        @include('/tasker/profile/tasks')
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>
 </div>