<div id="hire-me-tab" class="tabContent  tab-pane fade in active">
    <div class="hire-me">
        {{--*/ 
            switch($user->tasker->price) {
                case 1:
                    $price = "1 : 5 $ / Hour";
                break;
                case 2:
                    $price = "5 : 10 $ / Hour";
                break;
                case 3:
                    $price = "10 : 30 $ / Hour";
                break;
                case 4:
                    $price = "More than 30 $ / Hour";
                break;
            }
        /*--}}

        <h3 class="price-hire bolder"> <i class="fa fa-money" aria-hidden="true"></i> &nbsp; Price :<span> {{$price}} </span></h3>

        <br>
        
        <h3 class="specialization-hire bolder"> <i class="fa fa-align-left" aria-hidden="true"></i> &nbsp;  Specializations : </h3>
        @foreach($user->tasker->specializations as $specialization)
            <li class="specialization-item" style="margin-left: 150px;">
                {{$specialization->name}}
            </li>
        @endforeach
        
        <h3 class="skills-hire bolder"> <i class="fa fa-align-left" aria-hidden="true"></i> &nbsp;  Skills : </h3>
        @foreach($user->tasker->skills as $skill)
            <li class="skillname-hire" style="margin-left: 90px;">
                {{$skill->name}}
            </li>
        @endforeach

        <h3 class="skills-hire bolder"> <i class="fa fa-user" aria-hidden="true"></i> &nbsp;  Bio : </h3>
        <div class="col-xs-6 skillname-hire" style="margin-left: 60px;">
            {{ $user->profile->bio }}
        </div>
        <br><br><br><br>
    </div>
</div>