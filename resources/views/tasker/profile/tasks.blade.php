    <br><br>
    <div id="tasks-tab" class="tabContent  tab-pane fade">
        @foreach($user->tasker->tasks as $task)
            <div class="task col-xs-6">
                <div class="task-title">
                    <div class="task-title-label">
                        <i aria-hidden="true" class="fa fa-lightbulb-o"></i>
                         &nbsp; Task Title :
                    </div>
                     <div class="task-title-content" style="margin-left: 80px">
                        <a href="/tasks/{{$task->id}}">{{ $task->title }}</a>
                    </div>
                </div>
                <br>
                <div class="task-description">
                    <div class="task-description-label">
                        <i aria-hidden="true" class="fa fa-pencil"></i>
                        &nbsp; Task description :
                    </div> 
                    <div class="task-description-content" style="margin-left: 80px">
                        {{ $task->description }}
                    </div>
                </div>
                <br>
                 
                    @if($task->status == 1){
                        $status = "Assigned - waiting for your response. <br>";
                            $acceptAssignment = "<div class=\"\">
                                <button class=\"btn btn-success acceptAssignment\">Accept</button> <button class=\"btn btn-danger col-xs-offset-1 declineAssignment\">Decline</button>
                            </div>";
                    }@elseif($task->status == 2){
                        $status = "Assigned - work in progress.<br>";
                        $markAsCompleted =    "<div class=\"\">
                                <button class=\"btn btn-success markCompleted\">Mark as Completed</button>
                            </div>";
                    }@elseif($task->status == 3){
                        $status = "Completed";
                    }
                    @endif
                
                <div class="task-status">
                    <div class="task-status-label">
                        <i aria-hidden="true" class="fa fa-flag"></i>
                        &nbsp; Task status
                    </div>
                    <div class="task-status-content" style="margin-left: 80px">
                        {!! $status !!}
                        @if($task->status == 1)
                            @can('acceptAssignment' , $task)
                                {!! $acceptAssignment !!}
                            @endcan
                        @elseif($task->status == 2)
                            @can('markCompleted' , $task)
                                {!! $markAsCompleted !!}
                            @endcan
                        @endif
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <br>
            <hr><hr><hr><hr><hr>
        @endforeach
    </div>
