 <div id="review-tab" class="tabContent  tab-pane fade">
    <div class="Review-filter">
        @foreach($user->tasker->tasks as $task)
            @if($task->review)
                <br>
                <h5 class="label-taskr" style="margin-left:50px;" >
                    <img  class="profilePictuer-tasker" id="profilePictuer-Review" src="/images/users/profilePictures/{{ $task->user->profile_picture_path }}"/>
                    <a href="/users/{{$task->user->id}}">
                        {{$task->user->name}}
                    </a>
                </h5>
                <p class="p-about bolder" style="margin-left:150px; font-size: 15px;">
                    <i class="fa fa-angle-double-right bolder" aria-hidden="true" style="color:#F47857"></i> {{$task->review}}
                </p>
                <hr>
            @endif
        @endforeach
        <br><br>
    </div>
</div>