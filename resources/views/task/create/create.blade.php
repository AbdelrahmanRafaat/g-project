@extends('app')

@section('header')
	<link rel="stylesheet" type="text/css" href="/css/createTask.css">
    <!-- <link href="http://netdna.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet"> -->
@endsection

@section('content')
		@include('/partials/authNavBar')
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				@include('/task/create/create_task_form')
			</div>
		</div>
	</div>
@endsection

@section('footer')
	<script src="/js/createTask.js"></script>
@endsection