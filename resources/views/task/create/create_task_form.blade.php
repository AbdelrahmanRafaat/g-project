<!-- multistep form -->
<form id="createTask" action="{{action('TasksController@store' , ['id' => $user->id])}}" method="POST">
	{!!csrf_field()!!}

	<!-- progressbar -->
	<ul id="progressbar">
		<li class="active">Task Details</li>
		<li>Choose a tasker</li>
		<li>Payment Details</li>
	</ul>

	<fieldset>

		
		<div class="taskInfo" data-navigationNumber="0">
			<h5 class="bolder">(Fill Task Information)</h5>
			<hr>

			<input type="text" name="title" placeholder="Task Title" class="createTaskInput"/>
			
			<textarea class="form-control createTaskInput" rows="5" name="description" placeholder="Small Description about the task:"></textarea>

			<br>
			<hr>
			<div class="location">
				<label for="location_id">Location</label>
		        <select name="location_id" class="locationSelector form-control col-xs-10" style="width: 75%;">
		            @foreach($locations as $location)
		            	<option value="{{$location->id}}">{{$location->name}}</option>
		        	@endforeach
		        </select>
				<button type="button" class="custom_location_trigger">OR use custom location</button>
		    </div>

		    <div class="customLocation">
		    	
		    </div>

		    <div class="clearfix"></div>
		    <hr>

		    <div class="clearfix"></div>
		    <br>
			<div class="specialization">
				<label for="specialization_id" style="margin-right: 12px;">Specialization</label>
		        <select name="specialization_id" class="specializationSelector form-control" style="width: 75%;">
		            @foreach($specializations as $specialization)
		            	<option value="{{$specialization->id}}">{{$specialization->name}}</option>
		        	@endforeach
		        </select>
		    </div>

		    <div class="clearfix"></div>
			<br>
			<div class="skills">
				<label for="skills[]" style="margin-right: 35px;">Task Skills</label>
		        <select name="skills[]" multiple="multiple" class="skillsSelector form-control " style="width: 75%;">
		            @foreach($skills as $skill)
		            	<option value="{{$skill->id}}">{{$skill->name}}</option>
		        	@endforeach
		        </select>
		    </div>

		    <br>
		</div>


		<div class="taskerInfo" data-navigationNumber="1">

			<h1 style="color: orange">Tasker Selection</h1>
			<p>(Taskers in task location and have the same specialization as the task.)</p>
			<hr>
			<br>

			<!-- <div class="filter-options" style="margin:auto;">
				<label class="radio-inline" style="margin-right: 140px; "><input type="radio" value="1" name="taskers-filter">Sort by rating</label>
				<label class="radio-inline"><input type="radio" value="2" name="taskers-filter">Sort by price</label>
			</div>
			<div class="clearfix"></div>
			<br>
			<hr> -->

			<div class="taskers"> 
								
		    </div> <!--Taskers-->
	    </div><!--taskerInfo-->











		<div class="paymentInfo" data-navigationNumber="2">
			<div class="form-group row">
			    <label for="price" class="col-sm-2 form-control-label" style="margin-top: 10px">Task Price</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control createTaskInput" name="price" placeholder="Task Price">
			    </div>
			</div>

	        <div class="form-group row">
			    <label class="col-sm-2 form-control-label" style="margin-top: 10px" for="payment_method">Payment Method</label>
			    <div class="col-sm-10">
				    <select name="payment_method" class="form-control">
				        <option value="1">Hand To Hand</option>
		        		<option value="2">Paypal</option>
				    </select>
				</div>
			</div>
		</div>

		<div class="clearfix"></div>
		<input type="button" name="next" class="next action-button" value="Next"/>
	</fieldset>

</form>
