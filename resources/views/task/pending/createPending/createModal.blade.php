<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
        <div class="modal-content">
        
	        <!-- Modal Header -->
	       	<div class="modal-header">
	           	<button type="button" class="close" data-dismiss="modal">&times;</button>
	           	<h3 class="modal-title">Create New Task</h3>
	       	</div>

           <!-- Modal body -->
          <div class="modal-body">
              <form class="form-horizontal" role="form">
                <div class="create-task-input">
                  	<div class="form-group">
	                    	<label class="col-sm-2 control-label">
	                      		Description
	                    	</label>
	                    	<div class="col-sm-10">
		                      	<textarea class="form-control" rows="4" id="desc" placeholder="Description..">                                  
		                      	</textarea>
		                    </div>
	                  </div>
                	</div>
              	</form>
           	</div>
                       
            <!-- Modal Footer -->
         	<div class="modal-footer">
            	<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
             	<button class="btn btn-primary" data-dismiss="modal" id="">Save changes</button>
        	</div>

        </div>
    </div>
</div>