@extends('app')

@section('header')
	<link rel="stylesheet" type="text/css" href="css/createPending.css">
@endsection

@section('content')
	@include('partials/authNavBar')
	
	<div class="containerMain col-xs-12">
		<div class="col-md-12">
			<h2>
				No Taskers Available !!
			</h2>
            <button class="btn btn-primary btn-block create-btn" data-toggle="modal"  data-target="#myModal">
            	Create Pending Task
            </button>

            @include('/task/pending/createPending/createModal')
		</div>
	</div>
@endsection

@section('footer')
	<script type="text/javascript" src="js/createPending.js"></script>	
@endsection