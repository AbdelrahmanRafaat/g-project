@extends('app')

@section('header')
	<link rel="stylesheet" type="text/css" href="css/pendingListing.css">
@endsection

@section('content')
	@include('/partials/authNavBar')
		<div class="containerMain col-md-12">
			<div class="container col-md-12" >
				<div class="row">
					<!--Offset Div-->
					<div class="container-fluid col-md-2"></div>	
					@include('/task/pending/listing/list')
					<!--Filters should Be Made and included-->
					@include('/task/pending/listing/filter')
				</div>
			</div>
		</div>
@endsection

@section('footer')
	<script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.17/vue.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.0/vue-resource.min.js"></script>
	<script type="text/javascript" src="js/pendingListing.js"></script>	
@endsection