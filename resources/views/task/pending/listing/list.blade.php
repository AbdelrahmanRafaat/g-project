<div class="container" id="task-pending" id="apps" >
  
  <br>
  <!--Taskers Filter-->
    <div class="form-group col-md-12" style="padding:15px; background-color:#EEE">
        <label for="sel1">
            Select Specialization:
        </label>
        <select class="form-control" v-model="specialization">
            <option value="" selected>All</option>
            <option v-for="specializationName in pendingTasks">@{{ specializationName.specialization.name }}</option>
        </select>
    </div>

    <div  class="row" v-for="task in pendingTasks | filterBy specialization | limitBy count offset" >
        <div class="media-left media-middle"></div>
        <div class="media-body" style="background-color:#fff">
            <div class="col-md-3 left ">
               <a href="#">
                  <div class="card card-img card-orange" ">
                      <div class="icon ">
                          <img src="images/specializations/@{{task.specialization.image_path}}" class="img-card ">
                      </div>
                      <div class="text">
                          <h4> @{{ task.specialization.name }} </h4>
                          <p> @{{ task.specialization.description }}</p>
                      </div>.
                  </div>
                </a>
            </div>  
            <div class="col-md-9 right"> 
                <div>
                    <h2 class="media-heading" style="padding: 1px;margin: 1px">
                        <a href="/tasks/@{{task.id}}">@{{ task.title }}</a>
                    </h2>

                    <h3>
                        <span class="salary" id="salary" style="background-color:#eee; padding:12px;border-radius:10px; float:right">@{{ task.price }}</span>
                    </h3> <!-- Price -->

                    <!-- Tasker Location -->
                    <i class="icon-location"></i>
                    <span>@{{task.location.name}}</span>
                    <br>
                    
                    <i class="icon-thumbs-up-alt"></i>
                    <span >@{{ task.description }}</span>
                    
                    <hr>
                </div>
                <div>
                    <span><b>How I Can help:</b></span>
                    <p>@{{ task.created_at }}</p>
                    <a class="pull-right btn btn-primary assignBtn" href="user/@{{ user.user.id }}"> View Tasker</a>
                </div>
            </div>
        </div>   
    </div>
</div>

