@extends('app')

@section('header')
	<link rel="stylesheet" type="text/css" href="/css/showTask.css">
@endsection

@section('content')
		@include('/partials/authNavBar')
		@include('/task/show/headerSection')

		<div class="section-two">
	        <div class="container">

	            <ul class="nav nav-tabs">
	                <li class="active">
	                    <a data-toggle="tab" href="#Info">Information</a>
	                </li>
	                <li>
	                    <a data-toggle="tab" href="#Offers">Offers</a>
	                </li>
	            </ul>

	            <div class="tab-content">
					@include('/task/show/taskInfoTab')
					@include('/task/show/taskOffersTab')
				</div>

			</div>
		</div>

		@can('makeOffer' , $task)
			@include('/task/show/offerModal')
		@endcan

		@can('writeReview' , $task)
			@include('/task/show/reviewModal')
		@endcan
		
@endsection

@section('footer')
	<script src="/js/showTask.js"></script>
@endsection