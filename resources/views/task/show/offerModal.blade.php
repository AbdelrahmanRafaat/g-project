<div class="modal fade" id="offerModal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button"> &times; </button>
                <h4 class="modal-title hmodalTitle">Offer details</h4>
            </div>


            <div class="modal-body">
                <label for="price"> Price: </label>
                <input class="form-control" name="price" />
                <br>
                <label for="description"> Description: </label> 
                <textarea class="form-control" name="description" rows="4"></textarea>
            </div>


            <div class="modal-footer">
                <button class="btn bttn btn-hover offerSubmitButton" type="submit">Submit</button> 
            </div>
            
        </div>
    </div>
</div>