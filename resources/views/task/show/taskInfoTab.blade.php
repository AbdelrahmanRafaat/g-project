<div class="tab-pane fade in active info" id="Info">
    <div class="info-content">
        
        @can('writeReview' , $task)
            @if($task->review == NULL)
                <label class="pull-right reviewModalTrigger">
                    <button class="btn bttn btn-hover" data-target="#reviewModal" data-toggle="modal">
                        <label class="pull-right">
                            Write  Review
                        </label>
                    </button>
                </label>
            @endif
        @endcan

        @can('markCompleted' , $task)
            <label class="pull-right">
                <button class="btn bttn btn-hover markCompleted">
                    <label class="pull-right">
                        Mark as Completed
                    </label>
                </button>
            </label>
        @endcan

        <br>
        <div class="clearfix"></div>

        <label class="Task-owner"><span><i aria-hidden="true" class="fa fa-user"></i>Task Creator : </span>
            <span class="owner-name">
                {{$task->user->name}}
            </span>
        </label>

        <hr>
        <br>

        <label class="Task-title">
            <span>
                <i aria-hidden="true" class="fa fa-lightbulb-o fa-2x"></i> Task Title:
            </span>
            <span class="title-name">
                {{$task->title}}
            </span>
        </label>

        <hr>
        <br>

        <label class="Task-Description">
            <span><i aria-hidden="true" class="fa fa-pencil fa-2x"></i>Task Description:</span>
            <span class="Description">
               {{$task->description}}
            </span>
        </label>

        <hr>
        <br>

        <label class="Task-Spcialization">
            <span>
            <i aria-hidden="true" class="fa fa-th"></i>Spcialization :
            </span>
            <span class="Spcialization">
                {{$task->specialization->name}}
            </span>
        </label>

        <hr>
        <br>

        <label class="Task-skills">
            <span><i aria-hidden="true" class="fa fa-list"></i>
                skills :
            </span>
            <span class="Spcialization"></span><!--remove-->
        </label>

        <div class="form-group" id="group"><!--remove id-->
            <label class="Task-skills">
                @foreach($task->skills as $skill)
                    <li style="padding-left:90px"> {{$skill->name}} </li>
                    <br>
                @endforeach
            </label>
        </div>

        <hr>
        <label class="Task-status"><span><i aria-hidden="true" class="fa fa-flag"></i> Task status :</span>
             {{--*/ 
                switch($task->status) {
                    case 0:
                        $status = "Pending.";
                        break;
                     case 1:
                        $status = "Assigned for <a href=\"/user/{$task->tasker->user->id}/\">{$task->tasker->user->name}</a> - waiting for {$task->tasker->user->name} response.";
                        break;
                     case 2:
                        $status = "Assigned for <a href=\"/user/{$task->tasker->user->id}/\">{$task->tasker->user->name}</a> - work in progress.";
                        break;
                     case 3:
                        $status = "Completed By <a href=\"/user/{$task->tasker->user->id}/\">{$task->tasker->user->name}</a>.";
                        break;
                }
            /*--}}
            <span class="status">
                {!! $status !!}
            </span>
        </label>

        <hr>
        <br>

    </div>
</div>