 <div class="tab-pane fade offer" id="Offers">
    
    @can('makeOffer' , $task)
	    @if($task->status == 0)
		    <div class="row offerModalTrigger">
		        <label class="pull-right">
		            <button class="btn bttn btn-hover" data-target="#offerModal" data-toggle="modal">
		                <label class="pull-right">
		                    Make an offer
		                </label>
		            </button>
		        </label>
		    </div> <!--Make an offer button-->
		@endif
	@endcan

	@if($task->offers->isEmpty())
		<div class="emptyOffers">
			<br>
			<h2 style="text-align : center;"> This task has no offers yet.</h2>
		</div>
	@endif

	@foreach($task->offers as $offer)
	    <div class="offer-content">
	        <div class="row">
	            <div class="container col-md-12">
	                <div class="pull-left">
	                    <div class="card card-img card-orange">
	                        <div class="icon">
	                            <img class="img-card" src="/images/users/profilePictures/{{$offer->tasker->user->profile_picture_path}}">
	                        </div>

	                        <div class="text-card">
	                            <h4>
	                            <i aria-hidden="true" class="fa fa-user"></i> {{$offer->tasker->user->name}}
	                            </h4>
	                            <h6>
	                                <i aria-hidden="true" class="fa fa-map-marker fa-2x"></i>
	                                {{$offer->tasker->user->profile->address}}
	                            </h6>
	                        </div>
	                    </div>
	                </div>

	                <form>
	                    <fieldset>
	                        <div class="content-offer">
	                            <label class="offer-Price">
	                                <span><i aria-hidden="true" class="fa fa-usd fa-2x"></i>offer Price:</span> 
	                                <span class="Price">{{$offer->price}}$</span>
	                            </label>

	                            <hr>
	                            <br>
	                            <label class="Task-Description">
	                                <span>
	                                    <i aria-hidden="true" class="fa fa-pencil fa-2x"></i>
	                                    Offer Description :
	                                </span>
	                                <span class="Description">
	                                   {{$offer->description}}
	                                </span>
	                            </label>
	                        </div>
	                    </fieldset><!--remove-->
	                </form><!--remove-->
	            </div>
	            @can('acceptOffer' , $task)
		            @if($task->status == 0)
			            <button class="btn bttn btn-hover pull-right acceptOffer" data-taskerId="{{$offer->tasker->id}}" type="button">
			                Accept Offer
			            </button>
			        @endif
			    @endcan
	        </div>
	        <hr>
	    </div>
	@endforeach

</div>
