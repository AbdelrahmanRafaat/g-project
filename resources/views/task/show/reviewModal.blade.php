<div class="modal fade" id="reviewModal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button"> &times; </button>

                <h4 class="modal-title hmodalTitle">Write A Review</h4>
            </div>


            <div class="modal-body">
                <label for="rating"> Rating : </label>
                <select name="rating" class="form-control">
                    <option value="1">1 / 5</option>
                    <option value="2">2 / 5</option>
                    <option value="3">3 / 5</option>
                    <option value="4">4 / 5</option>
                    <option value="5">5 / 5</option>
                </select>
                <hr>

                <div class="form-group">
                    <label for="review" class="bolder">Review :</label> 
                    <!--Text Area-->
                    <textarea class="form-control" name="review" rows="4" placeholder="Write Review"></textarea>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn bttn btn-hover reviewSubmitButton" type="submit">Submit</button> 
            </div>
            
        </div>
    </div>
</div>