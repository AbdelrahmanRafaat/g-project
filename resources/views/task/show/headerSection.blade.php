<div class="section-one">
    <img class="cover" src="/images/BIKE_thumb.jpg">

    <div class="content-header">
        <img src="/images/users/profilePictures/{{$task->user->profile_picture_path}}">

        <h3><!--remove-->
        	Help <a href="/user/{{$task->user->id}}/">{{$task->user->name}}</a> with his task.
        </h3>


        <h6>
        	<i aria-hidden="true" class="fa fa-map-marker fa-2x"></i>
        	@if($task->location)
				{{$task->location->name}}
			@elseif($task->custom_location)
				{{$task->custom_location}}
			@endif
        </h6>

    </div>
</div>