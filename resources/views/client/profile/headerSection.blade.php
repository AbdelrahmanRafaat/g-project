<!--header-->
<div class="containerMain col-md-12">
    <div class="container col-md-12" id="about_tasker">
        <div class="row" id ="header_tasker" >
            <div class="slider">
                <div class="container">
                    <div class="profil">
                        <img class="profilePictuer-tasker" id="profilePictuer-Review"  alt="profilePictuer-tasker" src="/images/users/profilePictures/th-{{$user->profile_picture_path }}"/>
                        <div class="title">
                            <h1>
                                <p>Hello , I'am {{ $user->name }} </p>
                            </h1> 
                            <span id="spann" class="icon-location"></span><span class="spanh">{{ $user->profile->address }}</span>
                            <br>
                            <div class="clearfix"></div>
                            @if(request()->id != Auth::user()->id && Auth::check())
                                <div class="sendMessage" userId="{{request()->id}}">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
</div>
