
<!--Tasks And Review !-->
<div class="tab-pane fade" id="menu1">
  <div class="col-md-12">
    <div class="task-part">

        @foreach($user->tasks as $task)
            <div class="task col-xs-6">
                <div class="task-title">
                    <div class="task-title-label">
                        <i aria-hidden="true" class="fa fa-lightbulb-o"></i>
                         &nbsp; Task Title :
                    </div>
                     <div class="task-title-content" style="margin-left: 80px">
                        <a href="/tasks/{{$task->id}}">{{ $task->title }}</a>
                    </div>
                </div>
                <br>
                <div class="task-description">
                    <div class="task-description-label">
                        <i aria-hidden="true" class="fa fa-pencil"></i>
                        &nbsp; Task description :
                    </div> 
                    <div class="task-description-content" style="margin-left: 80px">
                        {{ $task->description }}
                    </div>
                </div>
                <br>
                {{--*/ 
                    switch($task->status) {
                    case 0:
                        $status = "Pending.";
                        break;
                     case 1:
                        $status = "Assigned for <a href=\"/user/{$task->tasker->user->id}/\">{$task->tasker->user->name}</a> - waiting for {$task->tasker->user->name} response.";
                        break;
                     case 2:
                        $status = "Assigned for <a href=\"/user/{$task->tasker->user->id}/\">{$task->tasker->user->name}</a> - work in progress.";
                        break;
                     case 3:
                        $status = "Completed By <a href=\"/user/{$task->tasker->user->id}/\">{$task->tasker->user->name}</a>.";
                        break;
                }
                /*--}}
                <div class="task-status">
                    <div class="task-status-label">
                        <i aria-hidden="true" class="fa fa-flag"></i>
                        &nbsp; Task status
                    </div>
                    <div class="task-status-content" style="margin-left: 80px">
                        {!! $status !!}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <br>
            <hr><hr><hr><hr><hr>
        @endforeach
        
    </div>
  </div>
</div>





