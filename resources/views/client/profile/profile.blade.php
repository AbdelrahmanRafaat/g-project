@extends('app')

@section('header')
	<link rel="stylesheet" type="text/css" href="/css/clientProfile.css">
@endsection

@section('content')
	@include('/partials/authNavBar')
	@include('/client/profile/headerSection')
	@include('/client/profile/tabHost')
@endsection

@section('footer')
	<script type="text/javascript" src="/js/clientProfile.js"></script>
@endsection
