<!--Info && tasks-->
<div class="clearfix"></div>
<br><br>
<div class="containerMain col-md-12">
    <div class="container col-md-12" id="about_pad">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                          <a data-toggle="tab" href="#home" id="cont">About</a>
                        </li>
                        <li>
                          <a data-toggle="tab" href="#menu1" id="cont">My Tasks</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <br>
    				    @include('/client/profile/taskTab')
    				    @include('/client/profile/aboutTab')
    			    </div>
                    <br><br>
                </div>
            </div>
       	</div>
	</div>
</div>