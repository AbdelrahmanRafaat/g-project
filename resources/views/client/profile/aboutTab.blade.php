<div id="home" class="tab-pane fade in active">
    <div class="edit-about">
        <div class="col-md-12">
            <div class="col-md-5">
                <div class="form-group">

                    <span class="aboutTabLabel">
                         <span class="icon-user"></span>
                    </span>
                    <span class="aboutTabContent">
                        {{ $user->name }}
                     </span>

                    <div class="clearfix"></div>
                    <br>

                    <span class="aboutTabLabel">
                         <span class="icon-mail"></span>
                    </span>
                    <span class="aboutTabContent">
                        {{ $user->email }}
                    </span>

                    <div class="clearfix"></div>
                    <br>

                    <span class="aboutTabLabel">
                        <span class=" icon-phone"></span>
                    </span>
                    <span class="aboutTabContent">
                        {{ $user->phone }}
                     </span>

                    <div class="clearfix"></div>
                    <br>

                    <span class="aboutTabLabel">
                        <span class="icon-flag"></span>
                    </span>
                    <span class="aboutTabContent">
                        I created {{ $user->tasks->count() }} tasks in TaskHelper.
                    </span>

                    <div class="clearfix"></div>
                    <br>
                    
                </div>
            </div>
        </div> 
    </div>
    <div class="clearfix"></div>
    <br>
</div>