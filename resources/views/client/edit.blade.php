@extends('app')

@section('header')
  <link rel="stylesheet" type="text/css" href="/css/edit-client.css">
@endsection

@section('content')
  @include('/partials/authNavBar')
    <br><br>

      <div class="container col-md-11 col-md-offset-1">
        <form action="{{ action('ProfilesController@update' , ['id' => $user->id]) }}" method="POST" class="updateForm" enctype="multipart/form-data">

          <input name="_method" type="hidden" value="PUT">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
              <div class="profilePictureUploader">
              <input type="hidden" value="{{$user->profile_picture_path}}" name="picture"/>
              <div class="row dropzoneUploader">
                  <div class="col-xs-2 col-xs-offset-4">
                      <div class="form-group">
                          <div class="dropzone dz-clickable" id="profilePictureUploader">
                              <div class="dz-message">
                                  Click to upload
                                  <br>
                                  <span class="note">
                                      (1 Picture Only)
                                  </span>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <br>
          
          <div>
            <h2>Personal Info .. </h2>
            <br>
          </div>

          <div class="form-group col-md-6">
              <label for="name">Name</label>
              <input type="text" name="name" value="{{ $user->name }}" class="form-control">
          </div>

          <div class="form-group col-md-6">
              <label for="email">Email</label>
              <input type="text" name="email" value="{{ $user->email }}" class="form-control">
          </div>

          <div class="form-group col-md-6">
              <label for="username">Username</label>
              <input type="text" name="username" value="{{ $user->username }}" class="form-control">
          </div>

          <div class="form-group col-md-6">
              <label for="phone">Phone</label>
              <input type="text" name="phone" value="{{ $user->phone }}" class="form-control">
          </div>

          <div class="form-group col-md-6">
              <label for="status">gender</label>
              <select name="gender" class="form-control">
                  <option value="0" {{($user->gender == 1) ? "selected=\"selected\" " : ""}} >Male</option>
                  <option value="1" {{($user->gender == 2) ? "selected=\"selected\" " : ""}} >Female</option>        
              </select>
          </div>
    
          <div class="row">
              <div class="col-xs-6">
                 <div class="form-group">
                    <label for="bio" class="bolder">Bio :</label> 
                    <textarea class="form-control" rows="5" name="bio" placeholder="Small Description about yourself:">
                      {{$user->profile->bio}}
                    </textarea>
                </div>
              </div>
            </div>

            <br><br>

          <div class="row">
              <div class="col-xs-8 col-xs-offset-2">
                <div class="form-group">
                    <label for="address" class="bolder">New Address : (old Address : {{$user->profile->address}})</label>
                    <input class="form-control addressInput" name="address" placeholder="Where do you live ?" type="text">
                    </div>
              </div>
            </div>
    
            <div class="addressSelection">
                <div class="row">
                    <div class="col-sm-12">

                        <!--countrySelectionBox-->
                        <div class="col-sm-3 col-xs-offset-1">
                            <div class="form-group">
                                <select name="country" class="countrySelectionBox form-control">
                                    <option value="nothing">Country</option>
                                </select>
                            </div>
                        </div>

                        <!--stateSelectionBox-->
                        <div class="col-sm-3">
                            <div class="form-group">
                                <select name="state" class="stateSelectionBox form-control">
                                    <option value="nothing">State</option>
                                </select>
                            </div>
                        </div>


                        <!--citySelectionBox-->
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select name="city" class="citySelectionBox form-control">
                                    <option value="nothing">City</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

	        <div class="clearfix"></div>


		    <button type="submit" class="btn btn-defualt btn-success btn-lg" style="float: right;">Update</button>
        </form>
      </div>

@endsection

@section('footer')
  <script type="text/javascript" src="/js/edit-client.js"></script>   
@endsection
