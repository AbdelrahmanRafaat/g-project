/**
 * @Author : Abdel-rahman Rafaat Ahmed.
 */

/*General Configurations :
	- Setting up the node Server 
	- Server side Socket.io 
	- node js Redis Client 
	- instance of ioredis 
*/
var server  =  require('http').Server();
var io      =  require('socket.io')(server);
var Redis   =  require('ioredis');
var messagingRedisClient     =  new Redis();
var notificationsRedisClient =  new Redis();

/*listen to a channel , and when a Message Arrives we send it to A specific user via socket.io*/
messagingRedisClient.subscribe('messagingChannel');
messagingRedisClient.on('message' , function(channel , message){
	var data = JSON.parse(message);
	data.dataType = 'message';
	io.sockets.emit('user:'+data.to , data );
});

notificationsRedisClient.subscribe('notificationsChannel');
notificationsRedisClient.on('message' , function(channel , notification){
	var data = JSON.parse(notification);
	data.dataType = 'notification';
	io.sockets.emit('user:'+data.to , data );
});


/*Booting Up the Server : port 3000 */
server.listen(3000 , '192.168.10.10' ,function(){
	console.log('The Server Is Running');
});