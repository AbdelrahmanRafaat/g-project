<?php

use App\Skill;
use App\Specialization;
use Illuminate\Database\Seeder;

class SkillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $specializations  = Specialization::all();
    	factory(Skill::class , 50)->create()->each(function($skill)
            use ($specializations)
        {
     		$skill->specialization_id = $specializations->random(1)->id;
            $skill->save();
     	});

    }
}
