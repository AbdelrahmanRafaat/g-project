<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    protected $tables = [
       'skills' , 'specializations'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::disableQueryLog();
        
        foreach($this->tables as $table){
            DB::table($table)->truncate();
        }

        /**
         * plant your seeds Here
         */
        $this->call(SpecializationsTableSeeder::class);
        $this->call(SkillsTableSeeder::class);

        
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Model::reguard();

    }
}
