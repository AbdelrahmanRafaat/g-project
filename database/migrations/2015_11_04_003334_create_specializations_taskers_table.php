<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecializationsTaskersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specialization_tasker', function (Blueprint $table) {
            $table->increments('id');

            
            $table->integer('specialization_id')->unsigned();
            $table->foreign('specialization_id')
                  ->references('id')
                  ->on('specializations')
                  ->onDelete('cascade');
            
            $table->integer('tasker_id')->unsigned();
            $table->foreign('tasker_id')
                  ->references('id')
                  ->on('taskers')
                  ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('specialization_tasker');
    }
}
