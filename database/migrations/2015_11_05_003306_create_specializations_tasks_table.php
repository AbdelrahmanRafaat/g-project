<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecializationsTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specialization_task', function (Blueprint $table) {
            $table->increments('id');

            /*Make sure that the table name is singular_singular in alphapitical order*/
            $table->integer('specialization_id')->unsigned();
            $table->foreign('specialization_id')
                  ->references('id')
                  ->on('specializations')
                  ->onDelete('cascade');
            
            $table->integer('task_id')->unsigned();
            $table->foreign('task_id')
                  ->references('id')
                  ->on('tasks')
                  ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('specialization_task');
    }
}
