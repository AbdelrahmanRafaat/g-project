<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title');
            $table->text('description');
            $table->integer('price');

            $table->integer('rating')->nullable();
            $table->text('review')->nullable();

            $table->tinyInteger('payment_method');

            $table->string('custom_location');
            $table->tinyInteger('status');
            // 0 -> pending  ,
            // 1 -> Assigned - Waiting for tasker response ,
            // 2 -> Assigned - Work in progress ,
            // 3 -> Assigned and completed ,

            $table->integer('location_id')->unsigned()->nullable();
            $table->foreign('location_id')
                  ->references('id')
                  ->on('locations')
                  ->onDelete('cascade');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');

            $table->integer('tasker_id')->unsigned()->nullable();
            $table->foreign('tasker_id')
                  ->references('id')
                  ->on('taskers')
                  ->onDelete('cascade');

            $table->integer('specialization_id')->unsigned();
            $table->foreign('specialization_id')
                  ->references('id')
                  ->on('specializations')
                  ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tasks');
    }
}
