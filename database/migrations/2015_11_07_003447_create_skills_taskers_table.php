<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkillsTaskersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skill_tasker', function (Blueprint $table) {
            $table->increments('id');

            /*Make sure that the table name is singular_singular in alphapitical order*/
            $table->integer('skill_id')->unsigned()->index();
            $table->foreign('skill_id')
                  ->references('id')
                  ->on('skills')
                  ->onDelete('cascade');
            
            $table->integer('tasker_id')->unsigned()->index();
            $table->foreign('tasker_id')
                  ->references('id')
                  ->on('taskers')
                  ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('skill_tasker');
    }
}
