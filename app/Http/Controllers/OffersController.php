<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Src\CustomClasses\SiteNotifier;
use App\Src\Interfaces\TasksRepositoryInterface;
use App\Src\Interfaces\OffersRepositoryInterface;
use App\Src\Interfaces\NotificationsRepositoryInterface;

class OffersController extends ApiController
{
    protected $tasksRepository;
    protected $offersRepository;
    protected $notifier;
    protected $notificationsRepsitory;

    public function __construct(
        SiteNotifier $notifier,
        TasksRepositoryInterface $tasksRepository,
        OffersRepositoryInterface $offersRepository,
        NotificationsRepositoryInterface $notificationsRepsitory
    ){
        $this->notifier = $notifier;
        $this->tasksRepository  = $tasksRepository;
        $this->offersRepository = $offersRepository;
        $this->notificationsRepsitory = $notificationsRepsitory;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store($id , Request $request){
        $task  = $this->tasksRepository->findOrFail($id);
        $offer = $this->offersRepository->create([
                'description' => $request->input('description'),
                'price'       => $request->input('price'),
                'task_id'     => $task->id,
                'tasker_id'   => \Auth::user()->tasker->id
            ]);

        $notificationContent = 'You got a new offer for your task from '.ucfirst($offer->tasker->user->name);

        $this->notificationsRepsitory->create([
            'task_id' => $task->id,
            'user_id' => $task->user->id,
            'content' => $notificationContent,
            'type'    => 6
        ]);

        $dataForBroadCasting = [
            'data' => json_encode([
                'content' => $notificationContent,
                'to'      => $task->user->id,
            ]),
            'channel' => 'notificationsChannel'
        ];
        
        $this->notifier->sendNotification($dataForBroadCasting);
        return $this->respondOk();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
