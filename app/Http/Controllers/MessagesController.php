<?php

namespace App\Http\Controllers;

use App\Message;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Src\CustomClasses\Messenger;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use App\Src\CustomClasses\SiteNotifier;
use App\Src\Formaters\MessagesFormater;

class MessagesController extends ApiController
{
    private $messenger; 
    private $messagesFormater;

    public function __construct(){
        $this->messenger = new Messenger;
        $this->messagesFormater = new MessagesFormater;
    }


   public function getChat(){
        $conversations = $this->messenger->getChatBoardConversations(Auth::user());
        return view('chat.chatBoard')->with('conversations' , $conversations);
    }


    public function postMessage(Request $request , SiteNotifier $notifier){
        $userId = Auth::user()->id;
        $senderName = Auth::user()->name;

        $this->messenger->saveMessage([
            'content'  => $request->message
            ,'user_id' => $userId
            ,'conversation_id' => $request->conversation_id
        ]);

        $this->messenger->updateRelatedConversation($request->conversation_id);

        /*BroadCast To The Other Side of Chat , user:$request->to*/
        $dataForBroadCasting = [
            'data' => json_encode([
                'content' => $request->message,
                'from'    => $userId,
                'to'      => $request->to,
                'senderName' => $senderName,
                'profilePicture' => $request->profilePicture
            ]),
            'channel' => 'messagingChannel'
        ];
        
        $notifier->sendNotification($dataForBroadCasting);
    }


    public function loadMoreMessages(Request $request){
        $olderMessages = $this->messenger->loadMoreMessages($request->conversationId , $request->startingMessage);
        $messages = $this->messagesFormater->format($olderMessages);
        return (count($olderMessages) == 0) ? 'false' : $messages;
    }

    public function handleProfileMessage($id , Request $request ,SiteNotifier $notifier){
        $user = \App\User::with('conversations.users')->findOrFail($id);
        $existingConversation = false;

        foreach($user->conversations as $conversation){
            $conversationUsers = $conversation->users->lists('id')->toArray();
            if(in_array(\Auth::user()->id , $conversationUsers)){
                $existingConversation = $conversation;
                break;
            }
        }
        
        if(! $existingConversation){
            $existingConversation = \App\Conversation::create();
            $existingConversation->users()->sync([
                \Auth::user()->id,
                $id
            ]);
        }

        $this->messenger->saveMessage([
            'content'  => $request->content
            ,'user_id' => Auth::user()->id
            ,'conversation_id' => $existingConversation->id
        ]);

        $this->messenger->updateRelatedConversation($existingConversation->id);

        $dataForBroadCasting = [
            'data' => json_encode([
                'content' => $request->content,
                'from'    => \Auth::user()->id,
                'to'      => $request->id,
                'senderName' => \Auth::user()->name,
                'profilePicture' => \Auth::user()->profile_picture_path
            ]),
            'channel' => 'messagingChannel'
        ];
        
        $notifier->sendNotification($dataForBroadCasting);

        return $this->respondOk('Message was sent');
    }
        
}
