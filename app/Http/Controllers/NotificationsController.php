<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Src\Formaters\NotificationsFormater;
use App\Src\Interfaces\NotificationsRepositoryInterface;

class NotificationsController extends Controller
{
	protected $notificationsRepository;

	public function __construct(NotificationsRepositoryInterface $notificationsRepository)
	{
		$this->notificationsRepository = $notificationsRepository;
	}


    public function show(Request $request , NotificationsFormater $notificationsFormater){
    	$notifications = $this->notificationsRepository->paginate(Auth::user());
    	return ($request->ajax()) ? [ $notificationsFormater->format($notifications) , $notifications->nextPageUrl() ] 
    								: view('/notification/notification')->with('notifications' , $notifications);
    }

    

}
