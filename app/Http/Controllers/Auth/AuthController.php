<?php

namespace App\Http\Controllers\Auth;

use  App\Src\Traits\AuthenticatesUsersBySocialMedia;
use App\Http\Controllers\Controller;
use App\Src\Interfaces\UsersRepositoryInterface;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends Controller
{
    protected $usersRepository;

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, AuthenticatesUsersBySocialMedia ,ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(UsersRepositoryInterface $usersRepository)
    {
        $this->middleware('guest', ['except' => 'getLogout']);
        $this->usersRepository = $usersRepository;
    }


    // redirect after login
    // protected $redirectPath = "user/".Auth::id();
    
    // protected $redirectAfterLogout   = "/landingPage";

    // redirect after registeration
    // protected $redirectTo = "/landingPage";



    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'  => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {   
        return $this->usersRepository->create($data);
    }
}
