<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Location;
use App\Profile;
use App\Specialization;
use App\Src\CustomClasses\StatisticsCalculator;
use App\Tasker;
use Illuminate\Http\Request;

class TaskersController extends ApiController{
	protected $statisticsCalculator;

	public function __construct(StatisticsCalculator $statisticsCalculator)
	{
		$this->statisticsCalculator = $statisticsCalculator;
	}

	/**
	 * Show all the taskers using vueJs to fit it with 
	 */
	public function showTaskersList() {
        $taskerDetails =  Tasker::with('user','locations','specializations')->get();
            return  $taskerDetails;
    }

    public function showTaskersView() {
        return view('tasker/listing/listing');
    }

	/**
	 * Shows all the taskers with the ability to search and filter through them
	 */
	public function index(Request $request){
		$locationTaskers       = Location::find($request->location)->taskers;
		$specializationTaskers = Specialization::find($request->specialization)->taskers;

		$matchingTaskers = collect();

		foreach ($locationTaskers as $tasker){
			if ( $specializationTaskers->contains($tasker->getKey())){
				$matchingTaskers->push($tasker);
			}
		}

		$responseData = [];
		foreach($matchingTaskers as $tasker) {
			$taskerStatistics = $this->statisticsCalculator->calculateTaskerStatistics($tasker);
			$taskerData = [];

			switch($tasker->price) {
				case 1:
					$taskerData['price']   = '1 : 5';
					break;
				case 2:
					$taskerData['price']   = '5 : 10';
					break;
				case 3:
					$taskerData['price']   = '10 : 30';
					break;
				case 4:
					$taskerData['price']   = 'Above 30';
					break;
			}

			$taskerData['id']      = $tasker->id;
			$taskerData['name']    = $tasker->user->name;
			$taskerData['bio']     = $tasker->user->profile->bio;
			$taskerData['picture'] = $tasker->user->profile_picture_path;
			$taskerData['specializations'] =join(' , ' , $tasker->specializations->lists('name')->toArray() );
			$taskerData['tasks_completed'] = $taskerStatistics['completedTasksCount'];
			$taskerData['rating']  = $taskerStatistics['taskerRating'];
			$taskerData['profile'] = 'user/'.$tasker->user->id.'/';
			
			$responseData[] = $taskerData; 
		}

		return $this->setData($responseData)->respondOk();
	}
    
}