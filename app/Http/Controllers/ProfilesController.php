<?php

namespace App\Http\Controllers;

use Auth;
use Input;
use App\Task;
use App\User;
use App\Tasker;
use App\Http\Requests;
use App\Http\Requests\UpdateProfileRequest;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SettingRequest;
use Intervention\Image\Facades\Image;
use App\Src\CustomClasses\UserProfileMapper;
use App\Src\CustomClasses\StatisticsCalculator;
use App\Src\Interfaces\SkillsRepositoryInterface;
use App\Src\Interfaces\TaskersRepositoryInterface;
use App\Src\Interfaces\ProfilesRepositoryInterface;
use App\Src\Interfaces\SpecializationsRepositoryInterface;
use App\Src\Interfaces\UsersRepositoryInterface;

class ProfilesController extends ApiController {

    protected $auth;
    protected $skillsRepository;
	protected $profileRepository;
    protected $specializationsRepository;
    protected $userProfileMapper;
    protected $StatisticsCalculator;
    protected $taskersRepository;
    protected $usersRepository;

	public function __construct(
        Guard $auth,
        UserProfileMapper $userProfileMapper,
        StatisticsCalculator $statisticsCalculator,
        SkillsRepositoryInterface $skillsRepository,
        TaskersRepositoryInterface $taskersRepository,
        ProfilesRepositoryInterface $profileRepository,
        SpecializationsRepositoryInterface $specializationsRepository,
        UsersRepositoryInterface $usersRepository
    ){
        $this->auth = $auth;
        $this->skillsRepository  = $skillsRepository;
        $this->userProfileMapper = $userProfileMapper;
		$this->profileRepository = $profileRepository;
        $this->statisticsCalculator = $statisticsCalculator;
        $this->specializationsRepository = $specializationsRepository;
        $this->taskersRepository = $taskersRepository;
        $this->usersRepository = $usersRepository;
	}
  

	public function getBuildProfile($id){
        if(\Gate::denies('buildProfile' , $id)){
            flash('You already did this once !');
            return redirect('/');  
        }
		$skills = $this->skillsRepository->getAll();
		$specializations = $this->specializationsRepository->getAll();
		return view('buildProfile/buildProfile')->with(['skills'=> $skills , 'specializations' => $specializations]);
	}

   	public function validateEmailUniqueness(Request $request){
   		return $this->validate($request , [
   			'email' => 'unique:users'
   		]);
   	}

   	public function validateUsernameUniqueness(Request $request){
   		return $this->validate($request , [
   			'username' => 'unique:users'
   		]);
   	}

   	public function uploadProfilePicture(Request $request){
   		$photo     = $request->file('profilePicture');
        $photoName = str_random(50).$photo->getClientOriginalName();
        $photo->move('images/users/profilePictures/' , $photoName);

        $interventionInstance = Image::make('images/users/profilePictures/'.$photoName);
        $interventionInstance->resize(200 , null , function ($constraint) {
                $constraint->aspectRatio();
            })->save('images/users/profilePictures/th-'.$photoName);

        return $photoName;
    }

   	public function createClientProfile(Request $request){
   	    $clientData = json_decode($request->clientData , false);
   	   	$this->profileRepository->createClientProfile($this->auth->user() , $clientData);
   	}

   	public function createTaskerProfile(Request $request){
   		$taskerData = json_decode($request->taskerData , false);
   		$this->profileRepository->createTaskerProfile($this->auth->user() , $taskerData);
   	}

    /**
     * Show Profile of Tasker || client.
     */
    public function show($id){
        $mappingResult = $this->userProfileMapper->mapUser($id);

        if($mappingResult['isTasker']){
            return view('tasker.profile.profile')->with([
                'user' => $mappingResult['userData'],
                'taskerStatistics' => $this->statisticsCalculator->calculateTaskerStatistics($mappingResult['userData']->tasker)
            ]);
        }
        
        return view('client.profile.profile')->with([
            'user' => $mappingResult['userData']
        ]);
    }

    public function edit($id){
        $mappingResult = $this->userProfileMapper->mapUser($id);

        if($mappingResult['isTasker']){
            $specializations = $this->specializationsRepository->getAll(); 
            $skills = $this->skillsRepository->getAll();
            return view('tasker.edit')->with([
                'user'    => $mappingResult['userData'],
                'skills'  => $skills,
                'specializations' => $specializations
            ]);
        }
        
        return view('client.edit')->with([
            'user' => $mappingResult['userData']
        ]);

    }

    public function update($id , UpdateProfileRequest $request){
        $user     = $this->usersRepository->findOrFail($id);
        $isTasker = $this->taskersRepository->isTasker($user);
        
        $userData = $request->only('name', 'username' , 'email' , 'gender' , 'picture' , 'phone');
        if($request->input('password') !== NULL ){
            $userData = bcrypt($request->input('password'));
        }
        $this->usersRepository->updateUserData($user , $userData);
        $profileData = $request->only('bio' , 'address');
        $this->profileRepository->updateProfileData($user->profile , $profileData);

        if($isTasker){
            $taskerData  = $request->only('price');
            $tasker      = $this->taskersRepository->getTasker($user);
            $this->taskersRepository->updateTaskerData($tasker , $taskerData);
            $this->taskersRepository->updateTaskerSkills($tasker , $request->only('skills') );
            $this->taskersRepository->updateTaskerSpecializations($tasker , $request->only('specilizations') );            
        }

        flash()->success('Good Job.' , 'Profile Updated Successfully.');
        return $this->setData(['url' => "/user/{$id}/"])->respondOk('Profile Was Updated .');        
    }

}
