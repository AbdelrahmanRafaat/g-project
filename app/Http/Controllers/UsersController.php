<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Src\CustomClasses\EmailConfirmer;

class UsersController extends Controller
{
    public function sendConfirmationEmail($id){
    	if(\Gate::allows('confirmEmail')){
    		$emailConfirmer = new EmailConfirmer;
    		$emailConfirmer->sendConfirmation(\Auth::user());
    		flash('Confirmation email was sent to your email .' ,'Make sure you check it out');
    		return redirect('/');
    	}
    }

	public function confirmEmail($id , $token){
		$profile = \App\Profile::where(['user_id' => $id , 'email_verified' => false , 'email_token'=> $token])->firstOrFail();
		$emailConfirmer = new EmailConfirmer;
    	$emailConfirmer->confirm($profile);
		flash()->success('Yupiiii' , 'You email is confirmed');
    	return redirect('/');
	}

}
