<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\Task;
use App\Tasker;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Src\CustomClasses\SiteNotifier;
use App\Src\Interfaces\TasksRepositoryInterface;
use App\Src\Interfaces\UsersRepositoryInterface;
use App\Src\Interfaces\SkillsRepositoryInterface;
use App\Src\Interfaces\TaskersRepositoryInterface;
use App\Src\Interfaces\LocationsRepositoryInterface;
use App\Src\Interfaces\NotificationsRepositoryInterface;
use App\Src\Interfaces\SpecializationsRepositoryInterface;

class TasksController extends ApiController{
    
    protected $locationsRepository;
    protected $specializationRepository;
    protected $skillsRepository;
    protected $tasksRepository;
    protected $usersRepository;
    protected $notifier;
    protected $taskersRepository;
    protected $notificationsRepsitory;

    public function __construct(
        TasksRepositoryInterface $tasksRepository,
        SkillsRepositoryInterface $skillsRepository,
        LocationsRepositoryInterface $locationsRepository,
        SpecializationsRepositoryInterface $specializationRepository,
        UsersRepositoryInterface $usersRepository,
        SiteNotifier $notifier,
        TaskersRepositoryInterface $taskersRepository,
        NotificationsRepositoryInterface $notificationsRepsitory
    ){
        $this->specializationRepository = $specializationRepository;
        $this->notificationsRepsitory   = $notificationsRepsitory;
        $this->taskersRepository = $taskersRepository;
        $this->skillsRepository  = $skillsRepository;
        $this->usersRepository   = $usersRepository;
        $this->tasksRepository   = $tasksRepository;
        $this->locationsRepository = $locationsRepository;
        $this->notifier = $notifier;
    }

    
    public function ana(){
        //View all pending tasks
        return $this->tasksRepository->getPendingTasks();
        // return Task::with('location','specialization')->where('status', '=',0)->get();
    }

    public function showPendingView(){
        return view('task.pending.listing.listing');
    }

    public function create($id){
        if(Gate::denies('createTask')){
            return redirect('/');
        }

        $user = $this->usersRepository->findOrFail($id);
        $skills = $this->skillsRepository->getAll(); 
        $locations = $this->locationsRepository->getAll();
        $specializations = $this->specializationRepository->getAll();

        return view('task.create.create' , compact('skills' , 'locations' , 'specializations' , 'user'));
    }

    public function store($id , Request $request){
        $taskData = $request->all();

        $taskData['status'] = 0;
        if($request->input('tasker_id')){
            $taskData['status'] = 1;
        }

        $taskData['user_id'] = Auth::user()->id;
        $task = $this->tasksRepository->create($taskData);
        $this->tasksRepository->syncSkills($task , $request->input('skills'));
        
        if($request->input('tasker_id')){
            $tasker = $this->taskersRepository->findOrFail($request->input('tasker_id'));
            $userAccount = $this->taskersRepository->getUser($tasker);
            $notificationContent = 'You were assigned to a new task.';
            
            $dataForBroadCasting = [
                'data' => json_encode([
                    'content' => $notificationContent,
                    'to'      => $userAccount->id,
                ]),
                'channel' => 'notificationsChannel'
            ];
            
            $this->notificationsRepsitory->create([
                'task_id' => $task->id,
                'user_id' => $userAccount->id,
                'content' => $notificationContent,
                'type'    => 1
            ]);

            $this->notifier->sendNotification($dataForBroadCasting); 
        }

        return $this->setData(['id' => $task->id])->respondOk();
    }


    public function show($id){
        $task = $this->tasksRepository->findOrFailWithRelations($id);
        return view('/task/show/show' , compact('task'));
    }

    public function edit($id , Request $request){
        
    }

    public function update($id , Request $request){
        $task = $this->tasksRepository->findOrFail($id);

        $newStatus = $request->input('status');
        if( isset($newStatus) ){
            if($newStatus == 0){
                $taskerName = ucfirst($task->tasker->user->name);
                $task->update([
                    'status'    => $newStatus ,
                    'tasker_id' => NULL
                ]);
                $notificationContent = $taskerName.' declined your task.';
                $notificationType    = 5; 

            }else if($newStatus == 2){
                if($request->input('tasker_id')){
                    //Client Accepted tasker`s offer
                    $tasker = $this->taskersRepository->findOrFail($request->input('tasker_id'));
                    $notificationContent = ucfirst($task->user->name).' accepted your offer.';

                    $task->update([
                        'status'    => $newStatus,
                        'tasker_id' => $tasker->id
                    ]);
                    
                    $this->notificationsRepsitory->create([
                        'task_id' => $task->id,
                        'user_id' => $tasker->user->id,
                        'content' => $notificationContent,
                        'type'    => 7
                    ]);

                    $dataForBroadCasting = [
                        'data' => json_encode([
                            'content' => $notificationContent,
                            'to'      => $tasker->user->id,
                        ]),
                        'channel' => 'notificationsChannel'
                    ];
                    
                    $this->notifier->sendNotification($dataForBroadCasting);
                    return $this->setData([
                                'tasker_name' => $tasker->user->name,
                                'profile_url' => '/user/'.$tasker->user->id.'/'
                            ])->respondOk();
                }else{
                    $task->update(['status' => $newStatus]);
                    $notificationContent = ucfirst($task->tasker->user->name).' accepted your task.'; 
                    $notificationType    = 2;
                }
            }else if($newStatus == 3){
                $task->update(['status' => $newStatus]);
                $notificationContent = ucfirst($task->tasker->user->name).' marked ('.$task->title.') task as completed.'; 
                $notificationType    = 9;
            }

            $this->notificationsRepsitory->create([
                'task_id' => $task->id,
                'user_id' => $task->user->id,
                'content' => $notificationContent,
                'type'    => $notificationType
            ]);

            $dataForBroadCasting = [
                'data' => json_encode([
                    'content' => $notificationContent,
                    'to'      => $task->user->id,
                ]),
                'channel' => 'notificationsChannel'
            ];
            
            $this->notifier->sendNotification($dataForBroadCasting); 
        }

        return $this->respondOk();
    }

    public function writeReview($id , Request $request){
        $task = $this->tasksRepository->findOrFail($id);
        $notificationContent = $task->user->name . ' wrote a review for your work.';
        
        $task->update([
            'review' => $request->input('review'),
            'rating' => $request->input('rating')
        ]);

        $this->notificationsRepsitory->create([
            'task_id' => $task->id,
            'user_id' => $task->tasker->user->id,
            'content' => $notificationContent,
            'type'    => 10
        ]);

        $dataForBroadCasting = [
            'data' => json_encode([
                'content' => $notificationContent,
                'to'      => $task->tasker->user->id,
            ]),
            'channel' => 'notificationsChannel'
        ];
        
        $this->notifier->sendNotification($dataForBroadCasting);
        
        return $this->respondOk();
    }

}