<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

/**
 * @author Abdelrahman Rafaat
 * small description
 *     Helper methods to provide a readable format of http responses 
 */

class ApiController extends Controller
{   

    /**
     * Http status code.
     * @var int
     */
    protected $statusCode = SymfonyResponse::HTTP_OK;
    
    /**
     * @var array
     */
    protected $data = [];
    
    /**
     * Array of headers will be sent with the http response.
     * @var array
     */
    protected $headers = [];

    /**
     * set http status code.
     * @param int $statusCode 
     */
    public function setStatusCode($statusCode){
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * set response data.
     * @param Array $data 
     */
    public function setData(Array $data){
        $this->data['data'] = $data;
        return $this;
    }

    /**
     * 
     * @param Array $headers [description]
     */
    public function setHeaders(Array $headers){
        $this->headers = $headers;
        return $this;
    }

    /**
     * Send json http response.
     * @param  Array $data      
     * @param  int   $statusCode
     * @param  Array $headers
     * @return Illuminate\Http\Response              
     */
    public function respond(Array $data = [] , $statusCode = null , Array $headers = []){
        return response()->json(
            isset($data) && !empty($data) ? $data : $this->data,
            isset($statusCode) ? $statusCode : $this->statusCode,
            isset($headers) && !empty($headers) ? $headers : $this->headers
        );
    }

    /**
     * creates an array of feedback(message , code) , this message can be a success|redirect|error.
     * code for further information on the API documentation.
     * @param  String $messageKey
     * @param  String $messageValue
     * @param  int $code
     * @return Array $feedback 
     */
    public function createFeedback($messageKey , $messageValue , $code = null){
         $feedback = [
            $messageKey => $messageValue,
        ];

        if(isset($code)){
            $feedback['code'] = $code;
        }

        return $feedback;
    }

    /**
     * Sends http success response.
     * @param  String $message 
     * @param  int    $code    
     * @return Illuminate\Http\Response
     */
    public function respondWithSuccess($message , $code = null){
        $this->data['message'] = $this->createFeedback('success' , $message , $code);
        return $this->respond();
    }

    /**
     * Sends http redirect response.
     * { @inheritdoc } 
     **/
    public function respondWithRedirection($message , $code = null){
        $this->data['message'] = $this->createFeedback('redirect' , $message , $code); 
        return $this->respond();
    }

    /**
     * Sends http error response.
     * { @inheritdoc }
     */
    public function respondWithError($message , $code = null){
        $this->data['message'] = $this->createFeedback('error' , $message , $code);
        return $this->respond();
    }

    #################
    #    Success    #
    #################

    /**
     * Sends Success Respond with Ok.
     * @param  string $message 
     * @return Illuminate\Http\Response 
     */
    public function respondOk($message = 'OK.'){
        return $this->setStatusCode(SymfonyResponse::HTTP_OK)->respondWithSuccess($message);
    }

    /**
     * Sends Success Respond with Resource created.
     * { @inheritdoc }
     */
    public function respondCreated($message = 'Resource was created.'){
        return $this->setStatusCode(SymfonyResponse::HTTP_CREATED)->respondWithSuccess($message);
    }

    /**
     * Sends Success Respond with Accepted and being proccessd.
     * { @inheritdoc }
     */
    public function respondAccepted($message = 'Request was accepted and being processed.'){
        return $this->setStatusCode(SymfonyResponse::HTTP_ACCEPTED)->respondWithSuccess($message);
    }

    /**
     * Sends Success Respond with no content(empty body).
     * @return Illuminate\Http\Response
     */
    public function respondSuccessNoContent(){
        $this->data = [];
        return $this->setStatusCode(SymfonyResponse::HTTP_NO_CONTENT)->respond();
    }

    #################
    # Redirections  #
    #################

    /**
     * Sends redirect Respond with Moved Permanently.
     * Moved Permanently link should be add to the links in the response data.
     * @param  string $message 
     * @return Illuminate\Http\Response 
     */
    public function respondMovedPermanently($message = 'This and all future requests should be directed to another URI.'){//links->MovedPermanently
        return $this->setStatusCode(SymfonyResponse::HTTP_MOVED_PERMANENTLY)->respondWithRedirection($message);
    }

    /**
     * Sends redirect Respond with See other.
     * See other link should be add to the links in the response data.
     * { @inheritdoc }
     */
    public function respondSeeOther($message = 'The response to the request can be found under another URI using a GET method.'){//links->seeOther
        return $this->setStatusCode(SymfonyResponse::HTTP_SEE_OTHER)->respondWithRedirection($message);
    }

    /**
     * Sends redirect Respond with Temporary Redirect.
     * Temporary Redirect link should be add to the links in the response data.
     * { @inheritdoc }
     */
    public function respondTemporaryRedirect($message = 'Request should be repeated with another URI.'){//links->TemporaryRedirect
        return $this->setStatusCode(SymfonyResponse::HTTP_TEMPORARY_REDIRECT)->respondWithRedirection($message);
    }

    /**
     * Sends redirect Respond(content was not modified) with no content(empty body). 
     * @return Illuminate\Http\Response
     */
    public function respondNotModified(){
        $this->data = [];
        return $this->setStatusCode(SymfonyResponse::HTTP_NOT_MODIFIED)->respond();
    }

    #################
    # Client Errors #
    #################

    /**
     * Sends error Respond with unautorized.
     * unautorized -> user is not autherized.
     * @param  string $message 
     * @return Illuminate\Http\Response 
     */
    public function respondUnautherized($message = 'Bad Authentication data.'){
        return $this->setStatusCode(SymfonyResponse::HTTP_UNAUTHORIZED)->respondWithError($message);
    }

    /**
     * Sends error Respond with forbidden.
     * unautorized -> user is forbidden.
     * { @inheritdoc }
     */
    public function respondForbidden($message = 'Forbidden Access'){
        return $this->setStatusCode(SymfonyResponse::HTTP_FORBIDDEN)->respondWithError($messages);
    }

    /**
     * Sends error Respond with unprocessable entity.
     * unprocessable entity -> mostly validation errors.
     * { @inheritdoc }
     */
    public function respondValidationFailed($message = 'Validation Failed.'){
        return $this->setStatusCode(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY)->respondWithError($message);
    }

    
    /**
     * Sends error Respond with not found.
     * not found -> Resource not found , The famous 404 respond.
     * { @inheritdoc }
     */
    public function respondNotFound($message = 'Resource not found.'){
        return $this->setStatusCode(SymfonyResponse::HTTP_NOT_FOUND)->respondWithError($message);
    }

    /**
     * methodNotAllowed -> method(GET|POST|PATCH|PUT...) is not allowed for the requested resource. 
     * { @inheritdoc }
     */
    public function respondMethodNotAllowed($message = 'Method not allowed for this resuorce.'){
        return $this->setStatusCode(SymfonyResponse::HTTP_METHOD_NOT_ALLOWED)->respondWithError($message);
    }

    /**
     * Sends error respond with not acceptable.
     * not acceptable -> when a client requests some data in a specific format but the server don`t support this format.
     * { @inheritdoc }
     */
    public function respondNotAcceptable($message = 'Requested data format is not supported.'){
        return $this->setStatusCode(SymfonyResponse::HTTP_NOT_ACCEPTABLE)->respondWithError($message);        
    }

    /**
     * Sends error respond with gone.
     * gone -> Resource deleted.
     * { @inheritdoc }
     */
    public function respondGone($message = 'Resource was deleted.'){
        return $this->setStatusCode(SymfonyResponse::HTTP_GONE)->respondWithError($message);
    }

    /**
     * Sends error respond with unsported media type.
     * unsported media type -> when the client sends a data to the server but the server don`t support this type of data
     * { @inheritdoc }
     */
    public function respondUnsupportedMediaType($message = 'Payload data format is not supported.'){
        return $this->setStatusCode(SymfonyResponse::HTTP_UNSUPPORTED_MEDIA_TYPE)->respondWithError($message);        
    }

    #################
    # Server Errors #
    #################


    /**
     * Sends error respond with internal Error.
     * Internal Error -> Error from the Api Provider(Server is down or in maintenance mode).
     * { @inheritdoc }
     */
    public function respondInternalError($message = 'Internal Error.'){
        return $this->setStatusCode(SymfonyResponse::HTTP_INTERNAL_SERVER_ERROR)->respondWithError($message);
    }

    /**
     * Sends error respond with service unavailable.
     * service unavailable -> API is not here right now.
     * { @inheritdoc }
     */
    public function respondServiceUnavailable($message = 'API is not here right now, please try again later.'){
        return $this->setStatusCode(SymfonyResponse::HTTP_SERVICE_UNAVAILABLE)->respondWithError($message);
    }

}

