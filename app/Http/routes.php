<?php

Route::get('/', function () {
	//Guest -> landing page
	//auth  -> profile page
	if(Auth::check()){
		return redirect('/user/'.Auth::user()->id.'/');		
	}
	//	
});

Route::get('/landingPage', function() {
	$allSpecializations = App\Specialization::all();
	return view('landingPage.landingPage',compact('allSpecializations'));
});




/**
 * Authentication routes... 
 * @see AuthenticatesUsers
 */
Route::get('auth/login'   , 'Auth\AuthController@getLogin');
Route::post('auth/login'  , 'Auth\AuthController@postLogin');
Route::get('auth/logout'  , 'Auth\AuthController@getLogout');

/**
 * Registration routes... 
 * @see RegistersUsers
 */
Route::get('auth/register'  , 'Auth\AuthController@getRegister');
Route::post('auth/register' , 'Auth\AuthController@postRegister');


/**
 * Reset Password Route... 
 * @see ResetsPasswords 
 * to Modify the email sent to the user : PasswordBroker.php -> emailResetLink 
 */
Route::controllers([
   'password' => 'Auth\PasswordController',
]);

/**
 * For Social Login
 * @see AutheticatesUsers
 */
Route::get('/social/login/{provider}' , ['as' => 'social' . 'login' , 'uses' => 'Auth\AuthController@getSocialLogin']);


/**
 * Build Profile Routes
 * @see ProfilesController
 */
Route::get('/user/{id}/buildProfile' , [
		'middleware' => ['auth' , 'matchingIds'],
		'uses' => 'ProfilesController@getBuildProfile'
	]
);

Route::post('/users/buildProfile/checkEmail' , [
		'middleware' => ['ajax' ],
		'uses' => 'ProfilesController@validateEmailUniqueness' 
 	]
 );

Route::post('/users/buildProfile/checkUsername' , [
		'middleware' => ['ajax' ],
		'uses' =>  'ProfilesController@validateUsernameUniqueness'
	]
);

Route::post('/user/{id}/buildProfile/createClientProfile' , [
 		'middleware' => ['auth' , 'ajax' , 'matchingIds'],
 		'uses' => 'ProfilesController@createClientProfile'
 	]
 );

Route::post('/user/{id}/buildProfile/createTaskerProfile' , [
		'middleware' => ['auth' , 'ajax' , 'matchingIds'],
		'uses' => 'ProfilesController@createTaskerProfile'
	]
);
Route::post('/user/{id}/profile/picture/' ,[
		'middleware' => ['auth' , 'ajax' , 'matchingIds'] ,
		'uses' => 'ProfilesController@uploadProfilePicture'
	]
);
#############################################################################################################################

/*Mail Confirmation*/
//Should be changed to post
Route::get('user/{id}/confirmEmail' , [
		'middleware' => ['auth' , 'matchingIds'],
		'uses' => 'UsersController@sendConfirmationEmail'
	]
);
Route::get('user/{id}/confirmEmail/{token}' , 'UsersController@confirmEmail');

/**
 * Messaging Routes.
 * @see MessagesController
 */
Route::get('/user/{id}/messages'  , [ 'middleware' => ['auth' , 'matchingIds' ] , 'uses' => 'MessagesController@getChat']);
Route::post('/user/{id}/messages' , [ ['auth' , 'matchingIds' , 'ajax'] , 'uses' => 'MessagesController@postMessage']);
Route::get('/user/{id}/messages/loadMore'   , [ ['auth' , 'matchingIds' , 'ajax'] , 'uses' => 'MessagesController@loadMoreMessages']);
Route::post('/user/{id}/profile-message/' , 'MessagesController@handleProfileMessage');


/**
 * Notifications Routes
 * @see NotificationsController
 */
Route::get('/user/{id}/notifications' , [ 'middleware' => ['auth' , 'matchingIds'] , 'uses' => 'NotificationsController@show']);


/**
 * Profiles Routes
 * @see ProfilesController
 */
Route::get('/user/{id}/'      , 'ProfilesController@show');
Route::get('/user/{id}/edit/' , 'ProfilesController@edit');
Route::put('/user/{id}/'      , 'ProfilesController@update');//offers -> tasker , client -> tasks

/**
 * Tasks Routes - Assigning && Offering Routes
 * @see TasksController
 */
// Route::get('/tasks/' , [ 'middleware' => ['auth'] , 'uses' => 'TasksController@index']);//Filtering and searching using ajax
Route::get('/user/{id}/tasks/create'    , [ 'middleware' => ['auth' , 'matchingIds'] , 'uses' => 'TasksController@create']);//assign a tasker and send him notification
Route::post('/user/{id}/tasks/'         , [ 'middleware' => ['auth' , 'ajax' , 'matchingIds'] , 'uses' => 'TasksController@store']);//assign a tasker and send him notification
Route::get('/tasks/{id}'      , [ 'middleware' => ['auth'] , 'uses' => 'TasksController@show']);//task with it`s offers
Route::get('/tasks/{id}/edit' , [ 'middleware' => ['auth'] , 'uses' => 'TasksController@edit']);
Route::put('/tasks/{id}/'     , [ 'middleware' => ['auth'] , 'uses' => 'TasksController@update']);//client -> accept an offer
Route::post('/tasks/{id}/reviews/' , [ 'middleware' => ['auth'] , 'uses' => 'TasksController@writeReview']);



/**
* Route for taskers Page
*/
Route::get('/taskers','TaskersController@showTaskersView');
Route::get('/api/taskers','TaskersController@showTaskersList');
// Route::get('/taskers/' , 'TaskersController@index');//Filtering and searching

/**
* Route for pending page 
*/
Route::get('pending','TasksController@showPendingView');
Route::get('/api/pending','TasksController@ana');


//client -> offers made by clients to taskers (when assigning) , tasker -> offer he made on pending tasks and offers he recived from clients
Route::get('/user/{id}/offers/'  , 'OffersController@index');

//taskers make an offer
Route::post('/tasks/{id}/offers/' , 'OffersController@store');


// Route::get('/tasks/' , 'TasksController@index');