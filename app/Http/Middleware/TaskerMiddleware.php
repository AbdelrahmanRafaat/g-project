<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use App\User;
use App\Tasker;

class TaskerMiddleware
{
     protected $user;

      public function __construct(User $user)
    {
        $this->user = $user;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $CheckTasker=Tasker::where('user_id','=',Auth::id())->get();
       if(count($CheckTasker))
    {
        return redirect('profile');
    }
    else{
    return $next($request);
}}
}
