<?php
namespace App\Http;
/**
 *Author : Abdelrahman Rafaat 
 *Idea   : Jeffery Way Tutorial
 **/
class Flash{

	/**
	 * general flash message creation using Sweet Alert , and other implementations , flashes a key and message information to the session
	 * @param   string $title      Flash Message Title
	 * @param   string $text       Flas Message Text(Body of the Message)
	 * @param   string $type       Flash Message Type(Success , Error , info , warning , Aside)
	 * @param   $buttonText 	   if the Message is overlay , this param will specifiy the button text 
	 * @param   string $key        what flash Message will be used(flashMessage , flashMessageOverlay ,flashMessageAside)
	 **/
	protected function create($title , $text , $type , $buttonText = null , $key = 'flashMessage' , $email = ''){
		session()->flash($key , [
			'title'   => $title,
			'text'    => $text,
			'type'    => $type,
			'buttonText' => $buttonText,
			'email'		 => $email,
		]);
	}

	/**
	 * informative Flash Message , i symbol , will be viewed for only 1.3 sec
	 * @param   string $title      Flash Message Title
	 * @param   string $text       Flas Message Text(Body of the Message)
	 **/
	public function info($title , $text){
		$this->create($title , $text , 'info');
	}

	/**
	 * Success Flash Message , Success symbol , will be viewed for only 1.3 sec
	 * @param   string $title      Flash Message Title
	 * @param   string $text       Flas Message Text(Body of the Message)
	 **/
	public function success($title , $text){		
		$this->create($title , $text , 'success');
	}

	/**
	 * Error Flash Message , Error symbol , will be viewed for only 1.3 sec
	 * @param   string $title      Flash Message Title
	 * @param   string $text       Flas Message Text(Body of the Message)
	 **/
	public function error($title , $text){		
		$this->create($title , $text , 'error');
	}

	/**
	 * Warining Flash Message , Warning symbol , will be viewed for only 1.3 sec
	 * @param   string $title      Flash Message Title
	 * @param   string $text       Flas Message Text(Body of the Message)
	 **/
	public function warning($title , $text){		
		$this->create($title , $text , 'warning');
	}

	/**
	 * overlay Flash Message , with a button to dismiss 
	 * @param   string $title      Flash Message Title
	 * @param   string $text       Flas Message Text(Body of the Message)
	 * @param   string $type       Flash Message Type(Success , Error , info , warning , Aside)
	 * @param   $buttonText 	   if the Message is overlay , this param will specifiy the button text 
	 **/
	public function overlay($title , $text , $type = 'success' , $buttonText){
		$this->create($title , $text , $type , $buttonText , 'flashMessageOverlay');
	}

	/**
	 * view an overlay to notify user he is registered but did not confirm his registeration using the email. 
	 * @param  string $email
	 */
	public function confirmationOverlay($email){
		$this->create('Please confirm your registeration !' ,'Make sure you visit your email , You will not login untill you confirm your registeration' 
						, 'warning' , 'Send another confirmation email ?' , 'flashMessageConfirmationOverlay' , $email);
	}

	/**
	 * Aside Flash Message , show a small flash message for 1.5 sec at the right bottom corner of the screen
	 * @param  string $title      Flash Message Title
	 * @param  string $type 	  To Specifiy the background-color 
	 */
	public function aside($text , $type = 'success'){
		$this->create('' , $text , $type , $buttonText = null , $key = 'flashMessageAside');
	}

}