<?php

namespace App;

use App\User;
use App\Offer;
use App\Skill;
use App\Tasker;
use App\Location;
use Illuminate\Database\Eloquent\Model;

class Task extends Model{
    
    protected $table    = 'tasks';
    protected $fillable = ['title' , 'rating' , 'price' , 'description' , 'review' , 'custom_location' , 'status' , 'location_id' ,'user_id' , 'tasker_id' , 'specialization_id'];

    /**
     * One To Many Relation
	 * A Task is Assigned To One and Only one Tasker.
     * Tasker is Assigned Many Tasks.
     */
    public function tasker(){
    	return $this->belongsTo(Tasker::class);
    }

    /**
     * One To Many Relation
	 * A Task is Created By One and Only one User.
     * User/Creator Creates Many Tasks.
     */
    public function user(){
    	return $this->belongsTo(User::class);
    }

    /**
     * One To Many Realtion
     * An Offer belongs To A Task.
     * A Task Has Many Offers.
     */
    public function offers(){
    	return $this->hasMany(Offer::class);
    }

     /**
     * Many To Many Relation.
     * A Skill Has Many Tasks.
     * A Task Has Many Skills.
     */
    public function skills(){
    	return $this->belongsToMany(Skill::class)->withTimestamps();
    }

    /**
     * Many To Many Relation.
     * A Specialization Has Many Tasks.
     * A Task Has Many Specializations.
     */
    public function specialization(){
    	return $this->belongsTo(Specialization::class);
    }

    /**
     * One To Many Relation
     * A Task Belongs To A Location
     * A Location Has Many Tasks
     */
    public function location(){
        return $this->belongsTo(Location::class);
    }

    /**
     * One to Many Relation.
     * A Notification belongs to a Task.
     * A Task has Many Notifications.
     */
    public function notifications(){
        return $this->hasMany(Task::class);
    }

}