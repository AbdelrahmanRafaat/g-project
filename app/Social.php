<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Social extends Model{

    protected $table     = 'social_logins';
    protected $fillable  = ['social_id' , 'provider'];

   /**
     * One To One Relation
     * Social Has Many Users.
     * A User Belongs to Social.
     */
    public function user(){
    	return $this->belongsTo(User::class);
    }

    
}