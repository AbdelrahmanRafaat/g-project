<?php

namespace App;

use App\User;
use App\Message;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model{
    
    protected $table = 'conversations';
    // protected $fillable = ['',''];

    /**
     * Many To Many Relation.
     * A User Has Many Conversations.
     * A Conversation Has Many Users(Two Or More).
     */
    public function users(){
    	return $this->belongsToMany(User::class)->withTimestamps();
    }

    /**
     * One To Many Relation.
     * A Conversation Has Many Messages.
     * A Message Belongs To A Conversation.
     */
    public function messages(){
    	return $this->hasMany(Message::class);
    }

}