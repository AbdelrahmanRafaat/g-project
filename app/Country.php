<?php

namespace App;

use App\State;
use Illuminate\Database\Eloquent\Model;

class Country extends Model{

   	protected $table = 'countries';
    protected $fillable = ['name' , 'iso'];

    /**
     * One To Many Relation. 
     * A Country Has Many States.
     * A State Belongs To A Country.
     */
    public function states(){
    	return $this->hasMany(State::class);
    }

}
