<?php

namespace App;

use App\Tasker;
use Illuminate\Database\Eloquent\Model;

class VerificationAttempts extends Model{
    
    protected $table    = 'verification_attempts';
    protected $fillable = ['data' , 'tasker_id'];

    /**
     * One To Many Relation
     * A Tasker Has Many VerificationAttempts.
     * VerificationAttempts belongs to a Tasker.
     */
    public function tasker(){
    	return $this->belongsTo(Tasker::class);
    }

}
