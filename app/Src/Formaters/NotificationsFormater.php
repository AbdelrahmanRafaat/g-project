<?php
namespace App\Src\Formaters;

use App\Src\Interfaces\FormaterInterface;

class NotificationsFormater implements FormaterInterface{
	
	public function format($notifications){

		$notificationsString = '';

		foreach ($notifications as $notification) {

			$notificationIcon = $this->formateNotificationIcon($notification->type);
		
			$notificationsString .= "<div class='row'><div class='col-xs-12'><div class='notification'><div class='row'><div class='notificationHeader col-xs-offset-6'>{$notificationIcon}</div><br><br></div><div class='row'><div class='notificationBody' >{$notification->content}</div><div class='notificationTime'>{$notification->created_at}</div></div></div></div></div><br><hr>";
		}

		return ( count($notifications) == 0 ) ? 'false' : $notificationsString;
	
	}

	private function formateNotificationIcon($notificationType){
		switch ($notificationType) {
			case '0':
				# Pending...
				return "<i class='icon-user'></i>";
				break;
			
			case '1':
				# Assiging...
				return "<i class='icon-user'></i>";
				break;
			
			case '2':
				# Offering...
				return "<i class='icon-user'></i>";
				break;

			case '3':
				# Completion...
				return "<i class='icon-user'></i>";
				break;

			case '4':
				# Review...
				return "<i class='icon-user'></i>";
				break;

			case '5':
				# Billing...
				return "<i class='icon-user'></i>";
				break;
		}
	}

}