<?php
namespace App\Src\Formaters;

use Illuminate\Support\Facades\Auth;
use App\Src\Interfaces\FormaterInterface;

class MessagesFormater implements FormaterInterface{

	public function format($messages){
		$messagesString = '';

        for($i = 0 ; $i < count($messages) ; $i++ ){
            $side = 'right';
            $content = $messages[$i]->content;
            $profilePicture = $messages[$i]->user->profile_picture_path;

            if( $messages[$i]->user_id == Auth::user()->id ){
                $side = 'left';
            }

            if($i == 0){
                $messagesString .= $this->formatFirstMessage($side , $content , $profilePicture ,$messages[$i]->id);
            }else{
            	$messagesString .= $this->formatMessage($side , $content , $profilePicture);
            }

        }

        return $messagesString;
	}

	private function formatFirstMessage($side , $content , $profilePicture , $id){
		return "<li class='message appeared {$side}'><input type='hidden' value='{$id}' name='firstMessage' /><div class='avatar'><img class='avatar' src='/images/users/profilePictures/".$profilePicture."'></div><div class='text_wrapper'><div class='text'>{$content}</div></div></li>";
	}

	private function formatMessage($side , $content , $profilePicture){
		return "<li class='message appeared {$side}'><div class='avatar'><img class='avatar' src='/images/users/profilePictures/".$profilePicture."'></div><div class='text_wrapper'><div class='text'>{$content}</div></div></li>";
	}


}