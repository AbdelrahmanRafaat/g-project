<?php
namespace App\Src\CustomClasses;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Src\Interfaces\SocialRepositoryInterface;
use App\Src\Interfaces\SocialAuthenticationInterface;
use Illuminate\Contracts\Config\Repository as Config;
use Laravel\Socialite\Contracts\Factory as Socialite;


Class SocialAuthenticator implements SocialAuthenticationInterface{

	protected $request;
	protected $auth;
	protected $config;
	protected $socialite;
	protected $socialRepository;

	public function __construct(Request $request ,  Guard $auth , Config $config , Socialite $socialite , SocialRepositoryInterface $socialRepository){
		$this->request   = $request;
		$this->auth      = $auth;
		$this->config    = $config;
		$this->socialite = $socialite;
		$this->socialRepository = $socialRepository;
	}


	/**
	 * Handles the Process of Autherization and Authentication of User using Social Media. 
	 * @param  string $provider          
	 */
	public function authenticate($provider){
		$provider = strtolower($provider);

		if( $this->isAjax() ){
			return $this->handleAjaxRequest();
		}
		
		if( !$this->providerExist($provider) ){
			return $this->handleProviderExistance($provider);
		}

		if( $this->providerPermissionDenied($provider) ){
			return $this->handlePermissionDenied();
		}

		if( !$this->isAutherized($provider) ){
			return $this->getAuthorizationFirst($provider);
		}
    	
    	$providerData = $this->getDataFromProvider($provider);
    	if( !$providerData ){
    		return $this->handleRetrievedDataErrors();
    	}

	    $socialUser = $this->socialRepository->findOrCreate($provider , $providerData);

    	if( !($socialUser instanceof User) ){
    		if($socialUser == false || $socialUser == 1062){
    			return $this->handleSavingNewUserExceptions($socialUser);
    		}
    	}

		$this->auth->login($socialUser);
        return redirect('/');
	}

	/**
	 * IF it was an Ajax Request , redirect to login Page.
	 * @return boolean 
	 */
	protected function isAjax(){
		if( $this->request->ajax() == true ){
    		return true;
    	}
    	return false;
	}

	/**
     * {@inheritdoc}
     * @return Illuminate\Http\RedirectResponse
     */
	protected function handleAjaxRequest(){
    	return \App::abort(403 , 'Unauthorized action.');
	}
	
	/**
	 * Check if the Given Provider Exist Or Not .
	 * @param  string $provider 
	 * @return Boolean 
	 */
	protected function providerExist($provider){
		return $this->config->has('services.' . $provider);
	}

	/**
	 * If the provider does not exist , We redirect to the login page with a flash message.
	 * @param  string $provider 
	 * @return Illuminate\Http\RedirectResponse
	 */
	protected function handleProviderExistance($provider){
		flash( ucfirst($provider)." Is not supported !");
		return redirect('/auth/login');
	}

	/**
	 * IF the user didn`t give premissions to our application , We redirect to the login page with a flash message.
	 * @param  string $provider 
	 */
	protected function providerPermissionDenied($provider){
		if($provider == "facebook" && $this->request->input('error_reason') !== null && $this->request->input('error_reason') == "user_denied"){
			return true;
		}

		if($provider == "twitter" && $this->request->input("denied") !== null){
			return true;
		}

		if($provider == "google" && $this->request->input("error") == "access_denied"){
			return true;
		}

		return false;
	}

	/**
     * {@inheritdoc}
     * @return Illuminate\Http\RedirectResponse
     */
	protected function handlePermissionDenied(){
	    flash('Permission Denied..','Make Sure You give us the permission !');
	    return redirect('/auth/login');
	}

	/**
     * Check For code || oauth_token || token ? if Non-of them was Found , getAuthorizationFirst 
	 * @param  string $provider 
	 */
	protected function isAutherized($provider){
		if( (!$this->request->input('oauth_token') && $provider == "twitter") 
			|| ( !$this->request->input('code') && ($provider == "facebook" || $provider == "github" || $provider == "google") && empty($this->request->input('errors')) ) ){
			return false;
		}
		return true;
	}


	/**
	 * Redirecting the user to the provider page to give premissions and return data.
	 * @param  string $provider 
	 */
	protected function getAuthorizationFirst($provider){
		return $this->socialite->driver($provider)->redirect();
	}

	/**
	 * Map the data for a given provider to an object and return it.
	 * @param  string $provider 
	 * @return MappedObject $providerData
	 */
	protected function getDataFromProvider($provider){
		/*Http Exceptions , Bad Request ..*/
		try{
			$providerData = $this->socialite->driver( $provider )->user();
		}catch(\Exception $e){
			return false;
		}
		return $providerData;
	}

	/**
	 * If there was Http Exceptions , Bad Request Exceptions .. Redirect and View A flash Message.
	 * @return Illuminate\Http\RedirectResponse
	 */
	protected function handleRetrievedDataErrors(){
		flash("Something Went Wrong" , "Please Try Again!");        			
        return redirect('/auth/login');
	}

	/**
	 * While saving the new user in the DB , if there was any exceptions , Redirect and view a message
	 * @param  int $exception
	 * @return Illuminate\Http\RedirectResponse
	 */
	protected function handleSavingNewUserExceptions($exception){
		if($exception == 1062){
			flash("Something Went Wrong" , "Email Already Exist !");
    		return redirect('/auth/login');
		}
		flash("Something Went Wrong" , "Please Try Again!");
    	return redirect('/auth/login');
	}

}