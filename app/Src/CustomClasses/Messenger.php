<?php
namespace App\Src\CustomClasses;;

use Carbon\Carbon;
use App\Src\Interfaces\MessagingInterface;
use App\Src\Interfaces\UsersRepositoryInterface;
use App\Src\Interfaces\MessagesRepositoryInterface;
use App\Src\Interfaces\ConversationsRepositoryInterface;

class Messenger implements MessagingInterface{

	protected $conversationsRepository;
	protected $messagesRepository;
	protected $usersRepository;

	public function __construct(){
		$this->conversationsRepository = app()->make(ConversationsRepositoryInterface::class);
		$this->messagesRepository = app()->make(MessagesRepositoryInterface::class);
		$this->usersRepository = app()->make(UsersRepositoryInterface::class);
	}

	public function getChatBoardConversations($user){
		$userConversations = $this->usersRepository->getConversations($user);
		$userConversations = $this->mapConversationToArray($userConversations);
		$userConversations = $this->deleteUserFromConversations($userConversations , $user);

		return $userConversations;
	}

	public function saveMessage(Array $messageArray){
		$this->messagesRepository->createMessage($messageArray);
	}

	public function updateRelatedConversation($conversationId){
		$this->conversationsRepository->updateConversation($conversationId , ['updated_at' => Carbon::now()]);
	}

	public function notifyReceiver(){
		//notifier
	}

	public function	loadMoreMessages($conversationId , $startingMessageId){
		return $this->conversationsRepository
					->getOlderMessages( $this->conversationsRepository->find($conversationId) , $startingMessageId);
	}

	//Transformer
	public function mapConversationToArray($userConversations){
		$conversations = [];
        foreach($userConversations as $conversation){
            $conversations[] = [
                  'conversationId'       => $conversation->id 
                , 'conversationUsers'    => $conversation->users 
                , 'conversationMessages' => $this->conversationsRepository->getLatestMessages($conversation)
            ];
        }
        return $conversations;
	}

	//Transformer
	public function deleteUserFromConversations($conversations , $user){
		foreach($conversations as $conversation){
            for($i = 0 ; $i <= count($conversation['conversationUsers']) ; ++$i){
                if($conversation['conversationUsers'][$i]->id == $user->id){
                    unset($conversation['conversationUsers'][$i]);
                }
            }
        }
        return $conversations;
	}
	
}