<?php
namespace App\Src\CustomClasses;

use App\Src\CustomClasses\LocationsOperator;

class TaskersLocationsOperator extends LocationsOperator{
	
	public function mergeIncomingTaskerLocationsNames($data){
		$dataLocations = [];
		if(isset($data->firstLocation)){
			$dataLocations[] = $data->firstLocation;
		}
		if(isset($data->secoundLocation)){
			$dataLocations[] = $data->secoundLocation;
		}
		if(isset($data->thirdLocation)){
			$dataLocations[] = $data->thirdLocation;
		}
		return ( !empty($dataLocations) ) ? array_unique($dataLocations) : false;
	}

}