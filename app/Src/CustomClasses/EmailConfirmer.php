<?php
namespace App\Src\CustomClasses;

use App\Src\CustomClasses\EmailNotifier;
use App\Src\Interfaces\ProfilesRepositoryInterface;
use App\Src\Interfaces\RegisterationConfirmationInterface;


class EmailConfirmer implements RegisterationConfirmationInterface{
	protected $emailNotifier;
	protected $profilesRepository;

	public function __construct(){
		$this->emailNotifier = new EmailNotifier;
		$this->profilesRepository = app()->make(ProfilesRepositoryInterface::class);
	}

	public function confirm($profile){
		$this->profilesRepository->confirmEmail($profile);
	}

	public function sendConfirmation($user){
		if(!isset($user->profile->email_token)){
			$this->profilesRepository->generateEmailConfirmationData($user->profile);
		}
		$this->emailNotifier->sendNotification(
			['to' => $user->email , 'view' => 'emails.confirm' , 'data' => $user->toArray()]
		);
	}
	
}