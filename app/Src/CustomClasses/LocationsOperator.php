<?php
namespace App\Src\CustomClasses;

abstract class LocationsOperator{

	public function mapLocationsCollectionsToNamesArray($locationsCollection){
		return $locationsCollection->map(function($location){
			return $location->name;
		})->toArray();
	}

	public function mapLocationsCollectionsToIdsArray($locationsCollection){
		return $locationsCollection->map(function($location){
			return $location->id;
		})->toArray();
	}

	public function findNewLocationsNames(Array $incomingLocationsNames , Array $existingLocationsNames){
		return array_diff( $incomingLocationsNames , $existingLocationsNames);
	}

	public function mapLocationsNamesToInsertFormat(Array $locationsNames){
		$mappedLocations = [];
		foreach($locationsNames as $locationName){
			$mappedLocations[] = ['name' => $locationName];
		}
		return $mappedLocations;
	}

}