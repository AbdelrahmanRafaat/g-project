<?php
namespace App\Src\CustomClasses;

use App\Src\Interfaces\UsersRepositoryInterface;
use App\Src\Interfaces\TaskersRepositoryInterface;

class UserProfileMapper{
	protected $usersRepository;
	protected $taskersRepository;


	public function __construct(
		UsersRepositoryInterface $usersRepository,
		TaskersRepositoryInterface $taskersRepository
	){

		$this->usersRepository   = $usersRepository;
		$this->taskersRepository = $taskersRepository;
	}

	public function mapUser($userId){
		$user     = $this->usersRepository->findOrFail($userId); 
		$isTasker = $this->taskersRepository->isTasker($user);

		if($isTasker){
			$userData = $this->usersRepository->findOrFailWithTaskerInfo($userId);
		}else{
			$userData = $this->usersRepository->findOrFailWithClientInfo($userId);
		}

		return [
			'userData' => $userData,
			'isTasker' => $isTasker
		];
	}

}