<?php
namespace App\Src\CustomClasses;

use App\Src\Interfaces\NotificationMethodInterface;
use Illuminate\Contracts\Redis\Database as Redis;

class SiteNotifier implements NotificationMethodInterface{
	
	protected $redisClient;

	public function __construct(Redis $redisClient){
		$this->redisClient = $redisClient;
	}

	public function sendNotification(Array $credentials){
        $this->redisClient->publish($credentials['channel'] , $credentials['data']);
	}

}