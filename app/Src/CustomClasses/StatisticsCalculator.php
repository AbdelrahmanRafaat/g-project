<?php
namespace App\Src\CustomClasses;

use App\Src\Interfaces\TaskersRepositoryInterface;

class StatisticsCalculator{
	protected $taskersRepository;

	public function __construct(
		TaskersRepositoryInterface $taskersRepository
	){
		$this->taskersRepository = $taskersRepository;
	}

	
	public function calculateTaskerStatistics($tasker){
		$completedTasks = $this->getCompletedTasks($tasker);
		$taskerRating   = $this->getTaskerRating($completedTasks);

		return [
			'completedTasksCount' => $completedTasks->count(),
			'taskerRating'        => $taskerRating['taskerRating'],
			'RatedTasksCount'     => $taskerRating['RatedTasksCount']
		];
	}

	public function getCompletedTasks($tasker){
		return $this->taskersRepository->getCompletedTasks($tasker);
	}

	public function getTaskerRating($completedTasks){
		$taskerRatings   = [];
		$taskerRating    = 0;
		foreach($completedTasks as $task){
			if($task->rating){
				$taskerRatings[] = $task->rating;
			}
		}

		foreach($taskerRatings as $rating){
			$taskerRating += $rating;
		}

		return [
			'taskerRating'     => (count($taskerRatings) == 0) ? 0 : ($taskerRating / ( 5 * count($taskerRatings) )) * 100,
			'RatedTasksCount'  => count($taskerRatings)
		];
	}
	
}