<?php
namespace App\Src\CustomClasses;

use App\Mailers\AppMailer;
use App\Src\Interfaces\NotificationMethodInterface;

class EmailNotifier implements NotificationMethodInterface{
	
	protected $mailer;

	public function __construct(){
		$this->mailer = new AppMailer(app()->make('mailer'));
	}

	public function sendNotification(Array $credentials){
		$this->setEmailCredentials($credentials);
		$this->mailer->deliver();
	}

	public function setEmailCredentials(Array $credentials){
		$this->mailer->to   = $credentials['to'];
		$this->mailer->view = $credentials['view'];
		$this->mailer->data = $credentials['data'];
	}

}