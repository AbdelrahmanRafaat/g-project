<?php
namespace App\Src\Interfaces;

Interface FilteringInterface{
	public function filter();
}