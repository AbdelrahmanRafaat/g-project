<?php
namespace App\Src\Interfaces;

Interface QuestionVerificationInterface{
	public function getSpecialization();
	public function chooseQuestions();
	public function viewExam();
}