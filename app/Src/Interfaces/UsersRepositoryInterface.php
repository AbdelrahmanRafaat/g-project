<?php
namespace App\Src\Interfaces;

Interface UsersRepositoryInterface{
	public function create(array $data);
	public function updateUserData($user , $data);
	public function getConversations($user);
}