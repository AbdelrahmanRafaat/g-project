<?php
namespace App\Src\Interfaces;

Interface DataCalculatorInterface{
	public function getData();
	public function performCalculations();
	public function sendCalculatedData();
}