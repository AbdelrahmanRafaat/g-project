<?php
namespace App\Src\Interfaces;

Interface MessagingInterface{
	public function saveMessage(Array $messageArray);
	public function notifyReceiver();
	public function	loadMoreMessages($conversationId , $startingMessageId);
	public function getChatBoardConversations($user);
}