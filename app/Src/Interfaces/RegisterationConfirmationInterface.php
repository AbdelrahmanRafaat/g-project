<?php
namespace App\Src\Interfaces;

Interface RegisterationConfirmationInterface{
	public function confirm($profile);
	public function sendConfirmation($user);
}