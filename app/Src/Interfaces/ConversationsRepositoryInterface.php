<?php
namespace App\Src\Interfaces;

interface ConversationsRepositoryInterface{
	public function getLatestMessages($conversation);
	public function getOlderMessages($conversation , $startingMessage);
	public function find($id);
	public function getNavbarMessages($user);
}