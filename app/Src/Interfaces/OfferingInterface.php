<?php
namespace App\Src\Interfaces;

Interface OfferingInterface{
	public function makeOffer();
	public function clientAcceptOffer();
}