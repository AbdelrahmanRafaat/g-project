<?php
namespace App\Src\Interfaces;

Interface ProfilesRepositoryInterface{
	public function createClientProfile($user , $clientData);
	public function createTaskerProfile($user , $taskerData);
}