<?php
namespace App\Src\Interfaces;

Interface BillingInterface{
	public function pay();
	public function NotifyAfterPayingCompletion();
	public function saveBill();
}