<?php
namespace App\Src\Interfaces;

Interface MessagesRepositoryInterface{
	public function createMessage(Array $messageArray);
}