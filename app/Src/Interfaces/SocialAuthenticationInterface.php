<?php
namespace App\Src\Interfaces;

Interface SocialAuthenticationInterface{
	public function authenticate($provider);
}