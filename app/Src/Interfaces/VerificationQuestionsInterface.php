<?php
namespace App\Src\Interfaces;

Interface VerificationQuestionsInterface{
	public function chooseQuestions();
	public function viewExam();
	public function calculateResult();
}