<?php
namespace App\Src\Interfaces;

Interface SocialRepositoryInterface{
	public function findOrCreate($provider , $providerData);
	public function checkUserExistance($provider , $socialId);
	public function createSocialUser($provider , $providerData);
}