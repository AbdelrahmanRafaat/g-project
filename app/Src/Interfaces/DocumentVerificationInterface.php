<?php
namespace App\Src\Interfaces;

Interface DocumentVerificationInterface{
	public function filterUploadedDocuments();
	public function checkFilteredDocuments();
}