<?php
namespace App\Src\Interfaces;

Interface NotificationsRepositoryInterface{
	public function paginate($user);
}