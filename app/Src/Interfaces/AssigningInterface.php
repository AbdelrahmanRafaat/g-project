<?php
namespace App\Src\Interfaces;

Interface AssigningInterface{
	public function assignTasker();
	public function taskerAcceptedAssigning();
}