<?php
namespace App\Src\Interfaces;

Interface NotificationMethodInterface{
	public function sendNotification(Array $credentials);
}