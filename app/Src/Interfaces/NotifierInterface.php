<?php
namespace App\Src\Interfaces;

Interface NotifierInterface{
	public function notifyAll(/*NotificationsMethodInterface*/);
	public function notifyGroup();
	public function notify();
}