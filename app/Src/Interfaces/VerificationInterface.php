<?php
namespace App\Src\Interfaces;

Interface VerificationInterface{
	public function calculateResult();
	public function sendResult();
}