<?php
namespace App\Src\Repositories;

use App\Notification;
use App\Src\Interfaces\NotificationsRepositoryInterface;

class NotificationsRepository implements NotificationsRepositoryInterface{
	protected $notification;

	public function __construct(){
		$this->notification = new Notification;
	}
	
	public function paginate($user){
		return $user->notifications()->orderBy('id' , 'DESC')->simplePaginate(5);
	}

	public function getNavbarNotifications($user){
		return $user->notifications()->orderBy('id','DESC')->limit(4)->get();
	}

	public function create(Array $data){
		return $this->notification->create($data);
	}

	
}