<?php
namespace App\Src\Repositories;

use App\Task;
use App\Src\Interfaces\TasksRepositoryInterface;

class TasksRepository implements TasksRepositoryInterface{
	protected $task;

	public function __construct(){
		$this->task = new Task;
	}

	public function getAll(){
		return $this->task->all();
	}

	public function findOrFail($id){
		return $this->task->findOrFail($id);
	}

	public function create(Array $data){
		return $this->task->create($data);
	}

	public function findOrFailWithRelations($id){
		return $this->task->with('tasker' , 'user' , 'offers' , 'skills' , 'specialization' , 'location')->findOrFail($id);
	}

	public function syncSkills($task ,Array $skills){
		return $task->skills()->sync($skills);
	}

	public function update(Array $data){
		return $task->update($data);
	}

	public function getPendingTasks(){
		return $this->task->with('location','specialization')->where('status', '=',0)->get();
	}

}