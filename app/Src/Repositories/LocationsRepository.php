<?php
namespace App\Src\Repositories;

use App\Location;
use App\Src\CustomClasses\TaskersLocationsOperator;
use App\Src\Interfaces\LocationsRepositoryInterface;


class LocationsRepository implements LocationsRepositoryInterface{
	
	protected $location;
	protected $taskersLocationsOperator;


	public function __construct(){
		$this->location = new Location;
		$this->taskersLocationsOperator = new TaskersLocationsOperator;
	}

	public function findLocationsByNames(Array $locationsNamesArray){
		return $this->location->where(function($query) use ($locationsNamesArray){
			for($i = 0 ; $i < count($locationsNamesArray) ; $i++) { 
				if($i == 0){
					$query->where('name' , '=' , $locationsNamesArray[$i]);
				}else{
					$query->orWhere('name' , '=' , $locationsNamesArray[$i]);
				}					      	
			}
		})->get();
	}

	public function insertMultipleLocations(Array $locations){
		return \DB::transaction(function () use($locations){
				$this->location->insert($locations);
				return \DB::getPdo()->lastInsertId();
			});
	}

	public function getLatestInsertedLocationsIds($firstInsertedId , $newInsertionsCount){//generate
		$insertedLocationsIds = [$firstInsertedId];
		for($i = 1 ; $i < $newInsertionsCount ; $i++){
			$insertedLocationsIds[] = $firstInsertedId+1;
		}
		return $insertedLocationsIds;
	}

	public function findOrInsert(Array $locationsNames){
		$existingLocations = $this->findLocationsByNames($locationsNames);
		$existingLocationsNames    = $this->taskersLocationsOperator->mapLocationsCollectionsToNamesArray($existingLocations);
		$newIncomingLocationsNames = $this->taskersLocationsOperator->findNewLocationsNames($locationsNames , $existingLocationsNames);
		$overAllLocations = $this->taskersLocationsOperator->mapLocationsCollectionsToIdsArray($existingLocations);
		
		$newInsertionsCount = count($newIncomingLocationsNames);
		if($newInsertionsCount > 0){
			$firstInsertedId = $this->insertMultipleLocations(
				$this->taskersLocationsOperator->mapLocationsNamesToInsertFormat($newIncomingLocationsNames)
			);

			$newIncomingLocationsIds = $this->getLatestInsertedLocationsIds($firstInsertedId , $newInsertionsCount);
			$overAllLocations =  array_merge($newIncomingLocationsIds , $overAllLocations);
		}
		return $overAllLocations;
	}


	public function getAll(){
		return $this->location->all();
	}

}