<?php
namespace App\Src\Repositories;

use App\Src\Interfaces\OffersRepositoryInterface;
use App\Offer;

class OffersRepository implements OffersRepositoryInterface{
	
	protected $offer;

	public function __construct(){
		$this->offer = new Offer;
	}

	public function create(Array $data){
		return $this->offer->create($data);
	}

}