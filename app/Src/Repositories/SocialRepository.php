<?php
namespace App\Src\Repositories;

use App\User;
use App\Social;
use App\Profile;
use App\Src\Interfaces\SocialRepositoryInterface;

/**
 * Author : Abdelrahman Rafaat
 */
class SocialRepository implements SocialRepositoryInterface{
	private $social;

	public function __construct(){
		$this->social = new Social;
	}

    /**
     * IF the user Exist -> return A User object , if it doesn`t -> create a new User from the providerData and return it.
     * @param string $provider
     * @param object $providerData
     * @return User
     */
	public function findOrCreate($provider , $providerData){
		$userExsit = $this->checkUserExistance($provider , $providerData->id);
		if( empty($userExsit) && !empty($providerData) ){
    		$newUser = $this->createSocialUser($provider , $providerData);
    		return $newUser;
    	}
    	if( !empty($userExsit) ){
			return $userExsit->user;
        }
	}

    /**
     * Check if Social Account Exist or not in the DB.
     * @param  string $provider 
     * @param  int $socialId 
     * @return Social
     */
	public function checkUserExistance($provider , $socialId){
		return $this->social->where('social_id', '=' , $socialId)->where('provider', '=', $provider)->first();
	}

    /**
     * @param  string $provider     
     * @param  Object $providerData 
     * @return User
     */
	public function createSocialUser($provider , $providerData){
		$newUser = new User;
    	$newUser->email    = ($providerData->email == null)  ? NULL :  $providerData->email;
        $newUser->name     = $providerData->name;
       	
        try{
    		$newUser->save();
    	}catch(\Exception $e){
    		if($e->errorInfo[1] == 1062){
				return 1062;
    		}
    		return false;
    	}

    	/*User who registers using social media is considered email verified*/
        $profile = new Profile;
        if($providerData->email != null){
            $profile->email_verified = true;
            $profile->email_token    = NULL;
        }
        $newUser->profile()->save($profile);

        $socialData = new Social;
        $socialData->social_id = $providerData->id;
        $socialData->provider  = $provider;
        $newUser->social()->save($socialData);

        return $newUser;
	}



}