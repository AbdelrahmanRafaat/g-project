<?php
namespace App\Src\Repositories;

use App\Conversation;
use App\Src\Interfaces\ConversationsRepositoryInterface;


class ConversationsRepository implements ConversationsRepositoryInterface{
	
	protected $conversation;


	public function __construct(){
		$this->conversation = new Conversation;
	}


	public function getLatestMessages($conversation){
		return $conversation->messages()->orderBy('id','DESC')->limit(8)->get();
	}


	public function getOlderMessages($conversation , $startingMessageId){
		return	$conversation->messages()
		            ->where('id' , '<' , $startingMessageId)
		            ->where('id' , '>' , $startingMessageId - 9)
		            ->limit(8)
		            ->get(['id','user_id','content']);
	}


	public function find($id){
		return $this->conversation->find($id);
	}


	public function updateConversation($id , Array $data){
		$this->conversation->where('id' , '=' , $id)->update($data);
	}


	public function getNavbarMessages($user){
		$latestConversationsIds = $user->conversations()
											->orderBy('updated_at','DESC')
											->limit(4)
											->get()
											->lists('id');

		$latestMessagesWithUser = [];

		foreach ($latestConversationsIds as $id) {
			$latestMessagesWithUser[] = $this->conversation->with([
				'users' => function($query) use ($user){
					$query->select(['users.id' , 'name' , 'profile_picture_path'])->where( 'users.id' , '!=' , $user->id );
				},
				'messages' => function($query) use ($user){
					$query->latest()->limit(1);
				}
			])->find($id);
		}

		return $latestMessagesWithUser;
	}

	// public function startConversation(){
		
	// }

}