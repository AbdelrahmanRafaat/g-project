<?php
namespace App\Src\Repositories;

use App\Tasker;
use App\Src\Interfaces\TaskersRepositoryInterface;

class TaskersRepository implements TaskersRepositoryInterface{
		
	protected $tasker;

	public function __construct(){
		$this->tasker = new Tasker;
	}

	public function createTasker($price , $userId){
		return $this->tasker->create([ 'price' => $price , 'user_id' => $userId]);
	}

	public function updateTaskerSkills($tasker , $skills){
		$tasker->skills()->sync($skills);
		return $tasker;
	}

	public function updateTaskerSpecializations($tasker , $specilizations){
		$tasker->specializations()->sync($specilizations);
		return $tasker;
	}

	public function updateTaskerLocations($tasker , $locations){
		$tasker->locations()->sync($locations);
		return $tasker;
	}

	public function isTasker($user){
		if($user->tasker){
			return true;
		}
		
		return false;
	}

	public function getCompletedTasks($tasker){
		return $tasker->tasks()->where('status' , '=' , 3)->get();
	}


	public function getTasker($user){
		return $user->tasker;
	}

	public function updateTaskerData($tasker , $taskerData){
		return $tasker->update($taskerData);
	}

	public function getUser($tasker){
		return $tasker->user;
	}

	public function findOrFail($id){
		return $this->tasker->findOrFail($id);
	}

}