<?php
namespace App\Src\Repositories;

use App\Skill;
use App\Src\Interfaces\SkillsRepositoryInterface;

class SkillsRepository implements SkillsRepositoryInterface{
	protected $skill;

	
	public function __construct(){
		$this->skill = new Skill;
	}

	public function getAll(){
		return $this->skill->all();
	}

}