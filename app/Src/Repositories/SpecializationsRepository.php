<?php
namespace App\Src\Repositories;

use App\Specialization;
use App\Src\Interfaces\SpecializationsRepositoryInterface;

class SpecializationsRepository implements SpecializationsRepositoryInterface{
	protected $specialization;

	public function __construct()
	{
		$this->specialization = new Specialization;
	}

	
	public function getAll(){
		return $this->specialization->all();
	}
}