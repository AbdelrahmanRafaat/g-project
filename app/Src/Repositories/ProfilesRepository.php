<?php
namespace App\Src\Repositories;

use App\Src\Interfaces\UsersRepositoryInterface;
use App\Src\Interfaces\TaskersRepositoryInterface;
use App\Src\CustomClasses\TaskersLocationsOperator;
use App\Src\Interfaces\ProfilesRepositoryInterface;
use App\Src\Interfaces\LocationsRepositoryInterface;

class ProfilesRepository implements ProfilesRepositoryInterface{
	protected $taskersLocationsOperator;
	protected $locationsRepository;
	protected $usersRepository;
	protected $taskersRepository;

	public function __construct(
		LocationsRepositoryInterface $locationsRepository ,
		UsersRepositoryInterface $usersRepository ,
		TaskersRepositoryInterface $taskersRepository
	){
		$this->taskersLocationsOperator = new TaskersLocationsOperator;
		$this->locationsRepository = $locationsRepository;
		$this->taskersRepository   = $taskersRepository; 
		$this->usersRepository     = $usersRepository;
	}

	public function createClientProfile($user , $clientData){
		if($clientData->type[0] != 'client'){
			throw new \Exception('Type mismatch Exception'); 
		}
		$user = $this->saveProfileData($user , $clientData);
		$user->push();
	}

	public function createTaskerProfile($user , $taskerData){//Job ?!
		if($taskerData->type[0] != 'tasker'){
			throw new \Exception('Type Mismatch Exception'); 
		}
		
		$user = $this->saveProfileData($user , $taskerData);
		$user->push();
		$user = $this->saveTaskerData($user , $taskerData);

		$incomingLocations = $this->taskersLocationsOperator->mergeIncomingTaskerLocationsNames($taskerData);
		if($incomingLocations != false){
			$locations = $this->locationsRepository->findOrInsert($incomingLocations);
			$this->taskersRepository->updateTaskerLocations( $user->tasker ,$locations);
		}
	}

	//Job
	public function saveProfileData($user , $data){
		$this->usersRepository->updateUserData($user , $data);		
		$this->updateProfileData($user->profile , $data);
		return $user;
	}

	public function updateProfileData($profile , $data){
		$profile->address = isset($data->address) ? $data->address : $profile->address ;
		$profile->bio     = isset($data->bio) ? $data->bio : $profile->bio ;
		$profile->email_verified = isset($data->emailVerified) ? $data->emailVerified : $profile->email_verified ;
		$profile->email_token    = isset($data->emailToken) ? $data->emailVerified : $profile->email_token ;
		$profile->phone_verified = isset($data->phoneVerified) ? $data->phoneVerified : $profile->phone_verified ;
		$profile->phone_code = isset($data->phoneCode) ? $data->phoneCode : $profile->phone_code ;
		return $profile;
	}
	
	//Job
	public function saveTaskerData($user , $data){
		$tasker = $this->taskersRepository->createTasker($data->salary[0] , $user->id);
		$this->taskersRepository->updateTaskerSkills( $tasker , $data->skills);
		$this->taskersRepository->updateTaskerSpecializations( $tasker , $data->specilizations);
		return $user;
	}

	public function confirmEmail($profile){
		$profile->email_verified = true;
		$profile->email_token   = NULL;
		$profile->save();
		return $profile;
	}

	public function generateEmailConfirmationData($profile){
		$profile->email_token   = str_random(60);
		$profile->save();
		return $profile;
	}

}