<?php
namespace App\Src\Repositories;

use App\Message;
use App\Conversation;
use App\Src\Interfaces\MessagesRepositoryInterface;

class MessagesRepository implements MessagesRepositoryInterface{
	
	protected $message;

	public function __construct(){
		$this->message = new Message;
	}

	public function createMessage(Array $messageArray){
		return $this->message->create([
		           'content'  => $messageArray['content']
		           ,'user_id' => $messageArray['user_id']
		           ,'conversation_id' => $messageArray['conversation_id']
		       ]);
	}




}