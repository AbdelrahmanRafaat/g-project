<?php
namespace App\Src\Repositories;

use App\User;
use App\Profile;
use App\Src\Interfaces\UsersRepositoryInterface;

class UsersRepository implements UsersRepositoryInterface{
	protected $user;

	public function __construct(){
		$this->user = new User;
	}

	public function create(array $data){
		$user = $this->user->create([
            'name'  => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

		$profile = new Profile;
		$user->profile()->save($profile);
        return $user;
	}

	public function updateUserData($user , $data){
		$user->profile_picture_path = (isset($data->profilePicture)) ? $data->profilePicture : $user->profile_picture_path;
		$user->email    = (isset($data->email)) ? $data->email : $user->email;
		if(isset($data->password)){
			$user->password = $data->password;
		}
		$user->name     = (isset($data->name))  ? $data->name : $user->name;
		$user->phone    = (isset($data->phone)) ? $data->phone : $user->phone; 
		$user->gender   = (isset($data->gender[0])) ? $data->gender[0] : $user->gender;
		$user->username = (isset($data->username))  ? $data->username : $user->username;
		return $user;
	}

	public function getConversations($user){
		return $user->conversations()->get();
	}

	public function findOrFail($id){
		return $this->user->findOrFail($id);
	}

	public function findOrFailWithTaskerInfo($id){
		return $this->user->with('profile' , 'tasks' , 'tasker.tasks','tasker.locations','tasker.offers','tasker.skills','tasker.specializations')->findOrFail($id);
	}

	public function findOrFailWithClientInfo($id){
		return $this->user->with('profile' , 'tasks.tasker')->findOrFail($id);
	}


}