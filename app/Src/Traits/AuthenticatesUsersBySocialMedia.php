<?php
namespace App\Src\Traits;

use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Src\CustomClasses\SocialAuthenticator;
use Laravel\Socialite\Contracts\Factory as Socialite;

/**
 * @author : Abdel-rahman Rafaat Ahmed.
 */
trait AuthenticatesUsersBySocialMedia{
	
    public function getSocialLogin($provider , SocialAuthenticator $socialAuthenticator){
    	return $socialAuthenticator->authenticate($provider);
	}

}