<?php

namespace App;

use App\Task;
use App\Tasker;
use App\Specialization;
use Illuminate\Database\Eloquent\Model;

class Skill extends Model{
    
    protected $table = 'skills';
    protected $fillable = ['name' , 'specialization_id'];

    /**
     * Many To Many Relation.
     * A Skill Has Many Tasks.
     * A Task Has Many Skills.
     */
    public function tasks(){
		return $this->belongsToMany(Task::class)->withTimestamps();
    }

    /**
     * Many To Many Relation.
     * A Skill Has Many Taskers.
     * A Tasker Has Many Skills.
     */
    public function taskers(){
    	return $this->belongsToMany(Tasker::class)->withTimestamps();	
    }

    /**
     * One To Many Relation. 
     * A Specialization Has Many Skills.
     * A Skill Belongs To A specialization.
     */
    public function specialization(){
    	return $this->belongsTo(Specialization::class);
    }

}
