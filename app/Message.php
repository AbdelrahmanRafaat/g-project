<?php

namespace App;

use App\User;
use App\Conversation;
use Illuminate\Database\Eloquent\Model;

class Message extends Model{

    protected $table = 'messages';
    protected $fillable = ['content' , 'conversation_id' , 'user_id'];

   /**
     * One to Many Relation
     * User Has Many Messages
     * Message belongs to A User. 
     */
    public function user(){
    	return $this->belongsTo(User::class);
    }

    /**
     * One to Many Relation
     * A Conversation Has Many Messages.
     * A Message belongs to A Conversation.
     */
    public function conversation(){
    	return $this->belongsTo(Conversation::class);	
    }

}