<?php

namespace App;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model{

    protected $table = 'notifications';
    protected $fillable = ['content' , 'type' , 'user_id' , 'task_id'];

     /**
     * One To Many Relation.
     * User Has Many notifications.
     * A Notification belongs to User. 
     */
    public function user(){
    	return $this->belongsTo(User::class);
    }

     public function getCreatedAtAttribute($date){
        return ( new Carbon($date) )->diffForHumans();
    }

    public function task(){
        return $this->belongsTo(Task::class);
    }

}