<?php

namespace App;

use App\Task;
use App\Tasker;
use Illuminate\Database\Eloquent\Model;

class Location extends Model{
    
    protected $table    = 'locations';
    protected $fillable = ['name'];


	/**
	* Many to Many Relation.
	* Tasker Has Many Locations He is avaiable at.
	* And Location Has Many Taskers.
	*/
    public function taskers(){
    	return $this->belongsToMany(Tasker::class);
    }

    /**
     * One To Many Relation
     * A Task Belongs To A Location
     * A Location Has Many Tasks
     */
    public function tasks(){
    	return $this->hasMany(Task::class);
    }

}