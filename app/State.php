<?php

namespace App;

use App\City;
use App\Country;
use Illuminate\Database\Eloquent\Model;

class State extends Model{

	protected $table    = 'states';
    protected $fillable = ['name'];

    /**
     * One To Many Relation
     * A City Belongs To A State.
     * A State Has Many Cities.
     */
    public function cities(){
    	return $this->hasMany(City::class);
    }

    /**
     * One To Many Relation. 
     * A Country Has Many States.
     * A State Belongs To A Country.
     */
    public function country(){
    	return $this->belongsTo(Country::class);
    }
    
}
