<?php

namespace App;

use App\Offer;
use App\Skill;
use App\Specialization;
use App\Location;
use App\VerificationAttempts;
use Illuminate\Database\Eloquent\Model;

class Tasker extends Model{
    
    protected $table    = 'taskers';
    protected $fillable = ['price' , 'verified' , 'user_id'];

    /**
     * One To One Relation
     * A Tasker is A User.
     */
    public function user(){
    	return $this->belongsTo(User::class);
    }

    /**
     * One To Many Relation
	 * A Task is Assigned To One and Only one Tasker.
     * Tasker is Assigned Many Tasks.
     */
    public function tasks(){
    	return $this->hasMany(Task::class);
    }

    /**
     * Locations Tasker Available at.
     * Many to Many Relation
     * Tasker Has Many Locations He is avaiable at.
     * And Location Has Many Taskers.
     */
    public function locations(){
    	return $this->belongsToMany(Location::class);
    }

    /**
     * One To Many Realtion
     * An Offer belongs To A Tasker.
     * A Tasker Has Many Offers.
     */
    public function offers(){
        return $this->hasMany(Offer::class);
    }
    
    /**
     * Many To Many Relation.
     * A Skill Has Many Taskers.
     * A Tasker Has Many Skills.
     */
    public function skills(){
        return $this->belongsToMany(Skill::class)->withTimestamps();
    }

    /**
     * Many To Many Relation.
     * A Specialization Has Many Taskers.
     * A Tasker Has Many Specializations.
     */
    public function specializations(){
        return $this->belongsToMany(Specialization::class);
    }

    /**
     * One To Many Relation
     * A Tasker Has Many VerificationAttempts.
     * VerificationAttempts belongs to a Tasker.
     */
    public function verificationAttempts(){
        return $this->hasMany(VerificationAttempts::class);
    }

}