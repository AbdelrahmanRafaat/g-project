<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {   
        $this->registerPolicies($gate);

        $gate->define('buildProfile' , function($user , $id){
            return (!isset($user->email) || !isset($user->username)) && $user->id == $id;
        });

        $gate->define('confirmEmail' , function($user){
            return $user->profile->email_verified == 0;
        });

        $gate->define('createTask' , function($user){
                        //Not a tasker
            return ( $user->tasker == NULL ) ? true : false; 
        });

        $gate->define('writeReview' , function($user , $task){
                        //assigned          //review is empty           the task creator is the auth user
            return ( $task->status == 3 && $task->review == NULL && $task->user->id == $user->id ) ? true : false; 
        });

        $gate->define('acceptOffer' , function($user , $task){
                        //the task creator is the auth user .. task is pending
            return ($task->user->id == $user->id && $task->status == 0) ? true : false;
        });

        $gate->define('makeOffer' , function($user , $task){
                        //tasker                pending task            no tasker assigned
            return ($user->tasker !== NULL && $task->status == 0 && $task->tasker == NULL) ? true: false;
        });

        $gate->define('markCompleted' , function($user , $task){
                        //tasker                //Status -> In Progress          //assigned to him
            return ($user->tasker !== NULL && $task->status == 2 && $task->tasker->id == $user->tasker->id) ? true: false;
        });

        $gate->define('acceptAssignment' , function($user , $task){
                        //tasker                //Status -> Assigned            //assigned to him
            return ($user->tasker !== NULL && $task->status == 1 && $task->tasker->id == $user->tasker->id) ? true: false;
        });

    }
}