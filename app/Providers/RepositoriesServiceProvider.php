<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Src\Repositories\UsersRepository;
use App\Src\Repositories\SkillsRepository;
use App\Src\Repositories\TasksRepository;
use App\Src\Repositories\SocialRepository;
use App\Src\Repositories\TaskersRepository;
use App\Src\Repositories\MessagesRepository;
use App\Src\Repositories\ProfilesRepository;
use App\Src\Repositories\OffersRepository;
use App\Src\Repositories\LocationsRepository;
use App\Src\Interfaces\UsersRepositoryInterface;
use App\Src\Interfaces\TasksRepositoryInterface;
use App\Src\Interfaces\SkillsRepositoryInterface;
use App\Src\Interfaces\SocialRepositoryInterface;
use App\Src\Repositories\ConversationsRepository;
use App\Src\Repositories\NotificationsRepository;
use App\Src\Interfaces\TaskersRepositoryInterface;
use App\Src\Interfaces\MessagesRepositoryInterface;
use App\Src\Interfaces\ProfilesRepositoryInterface;
use App\Src\Repositories\SpecializationsRepository;
use App\Src\Interfaces\LocationsRepositoryInterface;
use App\Src\Interfaces\ConversationsRepositoryInterface;
use App\Src\Interfaces\NotificationsRepositoryInterface;
use App\Src\Interfaces\SpecializationsRepositoryInterface;
use App\Src\Interfaces\OffersRepositoryInterface;


class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(){
        $this->app->bind(UsersRepositoryInterface::class    , UsersRepository::class);
        $this->app->bind(SocialRepositoryInterface::class   , SocialRepository::class);
        $this->app->bind(ProfilesRepositoryInterface::class , ProfilesRepository::class);
        $this->app->bind(SpecializationsRepositoryInterface::class , SpecializationsRepository::class);
        $this->app->bind(SkillsRepositoryInterface::class , SkillsRepository::class);
        $this->app->bind(LocationsRepositoryInterface::class , LocationsRepository::class);
        $this->app->bind(TaskersRepositoryInterface::class   , TaskersRepository::class);
        $this->app->bind(MessagesRepositoryInterface::class  , MessagesRepository::class);
        $this->app->bind(ConversationsRepositoryInterface::class   , ConversationsRepository::class);
        $this->app->bind(NotificationsRepositoryInterface::class   , NotificationsRepository::class);
        $this->app->bind(TasksRepositoryInterface::class  , TasksRepository::class);
        $this->app->bind(OffersRepositoryInterface::class , OffersRepository::class);


    }
    
}
