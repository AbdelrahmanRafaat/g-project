<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use App\Src\Interfaces\ConversationsRepositoryInterface;
use App\Src\Interfaces\NotificationsRepositoryInterface;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(NotificationsRepositoryInterface $notificationsRepository , ConversationsRepositoryInterface $conversationsRepository)
    {   
        //MessageDateMapper
        //MessageDateFormater
        
                                //5-The time of the messages. ->the same day.
                                                              // ->the same week.
                                                              // ->the same month.
                                                              // ->the same year.
                                                              // ->full date(Y-M-D)
        view()->composer('/partials/authNavBar' , function($view) use ($notificationsRepository , $conversationsRepository){
            $user = Auth::user();
            $navBarNotifications = $notificationsRepository->getNavbarNotifications( $user );
            $navBarMessages      = $conversationsRepository->getNavbarMessages( $user );
            $view->with(['navBarNotifications' => $navBarNotifications , 'navBarMessages' => $navBarMessages]);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
