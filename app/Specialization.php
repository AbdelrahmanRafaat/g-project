<?php

namespace App;

use App\Skill;
use Illuminate\Database\Eloquent\Model;

class Specialization extends Model
{
    protected $table = 'specializations';
    protected $fillable = ['name' , 'image_path'];

   /**
     * Many To Many Relation.
     * A Specialization Has Many Tasks.
     * A Task Has Many Specializations.
     */
    public function tasks(){
		return $this->hasMany(Task::class);
    }

    /**
     * Many To Many Relation.
     * A Skill Has Many Taskers.
     * A Tasker Has Many Skills.
     */
    public function taskers(){
    	return $this->belongsToMany(Tasker::class);	
    }

    /**
     * One To Many Relation. 
     * A Specialization Has Many Skills.
     * A Skill Belongs To A specialization.
     */
    public function skills(){
    	return $this->hasMany(Skill::class);
    }

}
