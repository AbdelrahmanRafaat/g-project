<?php

namespace App;

use App\Task;
use App\Client;
use App\Social;
use App\Tasker;
use App\Message;
use App\Profile;
use App\Conversation;
use App\Notification;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract{

    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'username' , 'email' , 'gender' , 'profile_picture_path' , 'phone' , 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    /**
     * One To One Relation
     * A Tasker is A User.
     */
    public function tasker(){
        return $this->hasOne(Tasker::class);
    }

    /**
     * One to Many Relation
     * User Has Many Messages
     * Message belongs to A User 
     */
    public function messages(){
        return $this->hasMany(Message::class);
    }

    /**
     * Many To Many Relation
     * User Has Many Conversations
     * Conversation Has Many Messages
     */
    public function conversations(){
        return $this->belongsToMany(Conversation::class)->withTimestamps();
    }

    /**
     * One to One Relation
     * User Has A Profile
     * Profile Belongs To A User
     */
    public function profile(){
        return $this->hasOne(Profile::class);
    }

    /**
     * One To One Relation
     * Users Has social Account.
     * A User Belongs to Social.
     */
    public function social(){
        return $this->hasOne(Social::class);
    }

    /**
     * One To Many Relation.
     * User Creates Many Tasks.
     * A Task belongs to User. 
     */
    public function tasks(){
        return $this->hasMany(Task::class);
    }

     /**
     * One To Many Relation.
     * User Has Many notifications.
     * A Notification belongs to User. 
     */
    public function notifications(){
        return $this->hasMany(Notification::class);
    }


    public function owns($relatedModel){
        return $this->id == $relatedModel->user_id;
    }

}