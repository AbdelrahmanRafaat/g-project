<?php

namespace App;

use App\Task;
use App\Tasker;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model{
    
    protected $table = 'offers';
    protected $fillable = ['description' , 'price' , 'task_id' , 'tasker_id'];

    /**
     * One To Many Realtion
     * An Offer belongs To A Tasker.
     * A Tasker Has Many Offers.
     */
    public function tasker(){
    	return $this->belongsTo(Tasker::class);
    }

    /**
     * One To Many Realtion
     * An Offer belongs To A Task.
     * A Task Has Many Offers.
     */
    public function task(){
    	return $this->belongsTo(Task::class);
    }

}
