<?php

namespace App;

use App\State;
use Illuminate\Database\Eloquent\Model;

class City extends Model{

    protected $table = 'cities';
    protected $fillable = ['name'];

    /**
     * One To Many Relation
     * A City Belongs To A State.
     * A State Has Many Cities.
     */
    public function state(){
    	return $this->belongsTo(State::class);
    }
    
}
