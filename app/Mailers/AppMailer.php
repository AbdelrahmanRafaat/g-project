<?php

namespace App\Mailers;

use App\User;
use Illuminate\Contracts\Mail\Mailer; 

class AppMailer{

	private $from = 'admins@taskHelper.com';
	public $to;
	public $view;
	public $data;

	protected $mailer;

	public function __construct(Mailer $mailer){
		$this->mailer = $mailer;
	}

	public function deliver(){
		$this->mailer->send($this->view , ['data' => $this->data] , function($message){
			$message->from($this->from  , 'TaskHelper');
			$message->to($this->to);
		});
	}

}