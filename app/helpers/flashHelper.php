<?php
//This File is autoLoaded in composer.json , autoload , files array
use App\Http\Flash;

function flash($title = null, $text = null){
	$flash = app('App\Http\Flash');

	if(func_num_args() == 0){
		//Return an object to chain it.
		return $flash;
	}

	$flash->info($title , $text);
}