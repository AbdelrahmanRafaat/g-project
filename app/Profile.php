<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model{
    
    protected $table = 'profiles';
    protected $fillable = ['address' , 'bio' , 'user_id' , 'email_verified' , 'email_token' , 'phone_verified' , 'phone_code'];

    /**
     * One To One.
     * User Has A Profile.
     * A Profile Belongs to A User.
     */
    public function user(){
    	return $this->belongsTo(User::class);
    }

    /**
     * To Be Implemented After Searching about Laravel cashier API.
     * Payment Information
     * Profile Has One Billing
     * Billing belongs to Profile
     *
     *public function payment(){
     *	Code Goes Here
     *}
	*/


}